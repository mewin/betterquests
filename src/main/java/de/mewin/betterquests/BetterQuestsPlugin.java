/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import de.mewin.betterquests.features.TrackQuestListener;
import de.mewin.betterquests.input.UserInputHandler;
import de.mewin.betterquests.addons.PlaceholderAPIAddon;
import de.mewin.betterquests.chat.DefaultMessageBuilder;
import de.mewin.betterquests.input.BQCommand;
import de.mewin.betterquests.input.CommandBQCallback;
import de.mewin.betterquests.input.CommandBetterQuests;
import de.mewin.betterquests.input.CommandQuest;
import de.mewin.betterquests.io.ProgressManagerBackend;
import de.mewin.betterquests.objectives.factories.ItemObjectiveFactory;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.io.QuestRegistryBackend;
import de.mewin.betterquests.io.YamlProgressManagerBackend;
import de.mewin.betterquests.io.YamlQuestRegistryBackend;
import de.mewin.betterquests.net.NetActionsListener;
import de.mewin.betterquests.net.NetworkHandler;
import de.mewin.betterquests.net.NetworkHelper;
import de.mewin.betterquests.objectives.factories.*;
import de.mewin.betterquests.objectives.listeners.ItemObjectiveListener;
import de.mewin.betterquests.objectives.listeners.*;
import de.mewin.betterquests.addons.ProtocolLibAddon;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.requirements.factories.*;
import de.mewin.betterquests.rewards.factories.*;
import de.mewin.betterquests.triggers.factories.*;
import de.mewin.betterquests.triggers.listeners.*;
import de.mewin.betterquests.util.ConfigKeys;
import de.mewin.mewinbukkitlib.util.Items;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.PluginNameConversationPrefix;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;
import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CONVERSATION_MANAGER;

/**
 * Main plugin class.
 *
 * Central access point for managing quests and progress.
 *
 * Use the {@link QuestRegistry} instance returned by {@link #getQuestRegistry() getQuestRegistry()}
 * to manage quests.
 *
 * Use the {@link ProgressManager} instance returned by {@link #getProgressManager() getProgressManager()}
 * to manage quest progress.
 *
 * @author mewin
 */
public class BetterQuestsPlugin extends JavaPlugin
{
    private final UserInputHandler userInputHandler;
    private final FactoryRegistry factoryRegistry;
    private final ConversationFactory conversationFactory;
    private final ObjectsByClassRegistry<Trigger> triggerRegistry;
    private final ObjectsByClassRegistry<Objective> objectiveRegistry;
    private final ArrayList<BetterQuestsAddon> addons;
    private NetworkHandler networkHandler;
    private NetworkHelper networkHelper;
    private ProgressManager progressManager;
    private QuestRegistry questRegistry;

    /**
     * Plugin constructor.
     *
     * Construct the plugin.
     */
    public BetterQuestsPlugin()
    {
        userInputHandler = new UserInputHandler(this);
        factoryRegistry = new FactoryRegistry();
        conversationFactory = new ConversationFactory(this)
                .withModality(true)
                .withPrefix(new PluginNameConversationPrefix(this))
                .withEscapeSequence(".");
        triggerRegistry = new ObjectsByClassRegistry<>();
        objectiveRegistry = new ObjectsByClassRegistry<>();
        addons = new ArrayList<>();

        if (instance == null) {
            instance = this;
        }
    }

    /**
     * Retrieves the used factory registry.
     *
     * The factory registry is used to manage factories for customizables which
     * define how these are created and loaded/saved.
     *
     * @return the factory registry used by the plugin
     */
    public FactoryRegistry getFactoryRegistry()
    {
        return factoryRegistry;
    }


    /**
     * Retrieves the used quest registry.
     *
     * The quest registry is used to manage the quest data (e.g. quests, their
     * stages, requirements, etc.). You can add, edit and remove quests here.
     *
     * @return the quest registry used by the plugin
     */
    public QuestRegistry getQuestRegistry()
    {
        return questRegistry;
    }

    /**
     * Used internally to parse user input.
     * @return
     */
    public UserInputHandler getUserInputHandler()
    {
        return userInputHandler;
    }


    /**
     * Retrieves the used progress manager.
     *
     * The progress manager is used to track each players active and finished
     * quests. Here you can access and modify that data.
     *
     * @return the progress manager used by the plugin
     */
    public ProgressManager getProgressManager()
    {
        return progressManager;
    }

    /**
     * Retrieves the used trigger registry.
     *
     * This registry is used to store all triggers of active quests, sorted by
     * their type. This can be used to quickly retrieve all triggers of a specific
     * type when needed by a listener.
     *
     * @return the trigger registry used by the plugin
     */
    public ObjectsByClassRegistry<Trigger> getTriggerRegistry()
    {
        return triggerRegistry;
    }

    /**
     * Retrieves the used objective registry.
     *
     * This registry is used to store all objectives of active quests, sorted by
     * their type. This can be used to quickly retrieve all objectives of a specific
     * type when needed by a listener.
     *
     * @return the objective registry used by the plugin
     */
    public ObjectsByClassRegistry<Objective> getObjectiveRegistry()
    {
        return objectiveRegistry;
    }

    /**
     * Retrieves the network handler.
     *
     * It is used internally to communicate with the client side UI mod. This
     * may return null if networking has been disabled in the plugin configuration.
     *
     * @return the network handler, or null if disabled
     */
    public NetworkHandler getNetworkHandler()
    {
        return networkHandler;
    }

    /**
     * Retrieves the network helper.
     *
     * This is used internally to ease the use and build messages that are sent
     * to clients using the UI mod. May be null if networking is disabled.
     *
     * @return thje network helper, or null if disabled
     */
    public NetworkHelper getNetworkHelper()
    {
        return networkHelper;
    }

    @Override
    public void onEnable()
    {
        getDataFolder().mkdirs(); // lets hope this works

        File configFile = new File(getDataFolder(), "config.yml");
        if (configFile.exists())
        {
            try
            {
                getConfig().load(configFile);
            }
            catch(IOException | InvalidConfigurationException ex)
            {
                getLogger().log(Level.WARNING, "Could not load configuration.", ex);
            }
        }
        else
        {
            InputStream in = BetterQuestsPlugin.class.getResourceAsStream("/config.yml");
            if (in != null) // just in case
            {
                try
                {
                    getLogger().log(Level.INFO, "No configuration file found, creating a default one.");
                    Files.copy(in, configFile.toPath());
                }
                catch (IOException ex)
                {
                    getLogger().log(Level.SEVERE, "Could not create default configuration.", ex);
                }
            }
        }

        if (getConfig().getBoolean(ConfigKeys.LOG_FULL))
        {
            getLogger().setLevel(Level.ALL);
        }

        PluginManager pluginManager = getServer().getPluginManager();

        CHAT_HELPER.loadMessages(this);

        if (getConfig().getBoolean(ConfigKeys.ENABLE_NETWORK, true))
        {
            networkHandler = new NetworkHandler(this);
            networkHandler.init();

            networkHelper = new NetworkHelper(this);
        }

        registerListeners();
        registerDefaults();

        QuestRegistryBackend questRegistryBackend
                = new YamlQuestRegistryBackend(new File(getDataFolder(), "quests.yml"));

        // TODO: different backends?
        questRegistry = new QuestRegistry(questRegistryBackend);

        try
        {
            questRegistry.loadQuests();
        }
        catch(Exception ex)
        {
            getLogger().log(Level.SEVERE, "Could not load quests.", ex);
            getLogger().log(Level.SEVERE, "Plugin will be disabled to prevent data loss.");
            questRegistry = null;
            pluginManager.disablePlugin(this);
            return;
        }

        ProgressManagerBackend progressManagerBackend
                = new YamlProgressManagerBackend(new File(getDataFolder(), "progress.yml"),
                                                 factoryRegistry, questRegistry);

        progressManager = new ProgressManager(this, progressManagerBackend);

        try
        {
            progressManager.loadProgress();
        }
        catch(Exception ex)
        {
            getLogger().log(Level.SEVERE, "Could not load progress.", ex);
            getLogger().log(Level.SEVERE, "Plugin will be disabled to prevent data loss.");
            questRegistry = null;
            progressManager = null;
            pluginManager.disablePlugin(this);
            return;
        }

        loadAddons();
    }

    private void switchToMaintenance()
    {
        // TODO
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        BQCommand cmd = getBQCmd(sender, command, label, args);

        if (cmd != null) {
            return cmd.run();
        }

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
    {
        BQCommand cmd = getBQCmd(sender, command, alias, args);

        if (cmd != null) {
            return cmd.tabComplete();
        }

        return null;
    }

    /**
     * start a conversation
     *
     * used internally to start a conversation that is managed by the plugin
     *
     * @param firstPrompt the prompt to start with
     * @param conversable the person to converse with
     */
    public void startConversation(Prompt firstPrompt, Conversable conversable)
    {
        Conversation conversation = conversationFactory.withFirstPrompt(firstPrompt).buildConversation(conversable);
        CONVERSATION_MANAGER.beginConversation(conversation);
    }

    private void loadAddons()
    {
        PluginManager pluginManager = getServer().getPluginManager();

        if (pluginManager.isPluginEnabled("ProtocolLib"))
        {
            getLogger().log(Level.INFO, "ProtocolLib is installed, hooking into it.");
            addons.add(new ProtocolLibAddon(this));
        }

        if (pluginManager.isPluginEnabled("PlaceholderAPI"))
        {
            getLogger().log(Level.INFO, "PlaceholderAPI is installed, hooking into it.");
            addons.add(new PlaceholderAPIAddon(this));
        }

        for (BetterQuestsAddon addon : addons) {
            addon.onEnable();
        }
    }

    private void registerDefaults()
    {
        PluginManager pluginManager = getServer().getPluginManager();
        // requirements
        factoryRegistry.tryRegisterRequirementFactory(ItemRequirementFactory.MY_ID, new ItemRequirementFactory());
        factoryRegistry.tryRegisterRequirementFactory(QuestRequirementFactory.MY_ID, new QuestRequirementFactory());
        // triggers
        factoryRegistry.tryRegisterTriggerFactory(ClickBlockTriggerFactory.MY_ID, new ClickBlockTriggerFactory());
        factoryRegistry.tryRegisterTriggerFactory(ItemTriggerFactory.MY_ID, new ItemTriggerFactory());
        factoryRegistry.tryRegisterTriggerFactory(PasswordTriggerFactory.MY_ID, new PasswordTriggerFactory());
        // objectives
        factoryRegistry.tryRegisterObjectiveFactory(ItemObjectiveFactory.MY_ID, new ItemObjectiveFactory());
        factoryRegistry.tryRegisterObjectiveFactory(CombinedObjectiveFactory.MY_ID, new CombinedObjectiveFactory());
        factoryRegistry.tryRegisterObjectiveFactory(SurvivalObjectiveFactory.MY_ID, new SurvivalObjectiveFactory());
        factoryRegistry.tryRegisterObjectiveFactory(LocationObjectiveFactory.MY_ID, new LocationObjectiveFactory());
        factoryRegistry.tryRegisterObjectiveFactory(PasswordObjectiveFactory.MY_ID, new PasswordObjectiveFactory());
        factoryRegistry.tryRegisterObjectiveFactory(KillMobsObjectiveFactory.MY_ID, new KillMobsObjectiveFactory());
        // rewards
        factoryRegistry.tryRegisterRewardFactory(ItemRewardFactory.MY_ID, new ItemRewardFactory());
        factoryRegistry.tryRegisterRewardFactory(CommandRewardFactory.MY_ID, new CommandRewardFactory());
        factoryRegistry.tryRegisterRewardFactory(ExperienceRewardFactory.MY_ID, new ExperienceRewardFactory());
        factoryRegistry.tryRegisterRewardFactory(TitleRewardFactory.MY_ID, new TitleRewardFactory());

        // listeners (triggers)
        pluginManager.registerEvents(new ItemTriggerListener(this), this);
        pluginManager.registerEvents(new ClickBlockTriggerListener(this), this);
        pluginManager.registerEvents(new PasswordTriggerListener(this), this);
        // listeners (objectives)
        pluginManager.registerEvents(new ItemObjectiveListener(this), this);
        pluginManager.registerEvents(new SurvivalObjectiveListener(this), this);
        pluginManager.registerEvents(new LocationObjectiveListener(this), this);
        pluginManager.registerEvents(new PasswordObjectiveListener(this), this);
        pluginManager.registerEvents(new KillMobsObjectiveListener(this), this);
    }

    private void registerListeners()
    {
        PluginManager pluginManager = getServer().getPluginManager();

        pluginManager.registerEvents(new RegistryListener(this), this);
        pluginManager.registerEvents(new PromptListener(this), this);
        pluginManager.registerEvents(new QuestListener(this), this);

        if (networkHandler != null) {
            pluginManager.registerEvents(new NetActionsListener(this), this);
        }

        BukkitScheduler scheduler = Bukkit.getScheduler();

        scheduler.runTaskTimer(this, new ProgressTimer(this), 20L, 5L); // todo: make configurable
        scheduler.runTaskTimer(this, new QuestUpdateTimer(this), 20L, 20L); // todo: this too

        // tracked quests
        TrackQuestListener trackQuestListener = new TrackQuestListener(this);
        pluginManager.registerEvents(trackQuestListener, this);
        scheduler.runTaskTimer(this, trackQuestListener, 20L, 20L); // todo: even this
    }

    @Override
    public void onDisable()
    {
        try
        {
            if (questRegistry != null) {
                questRegistry.saveQuests();
                progressManager.saveProgress();
            }
        }
        catch(IOException ex)
        {
            getLogger().log(Level.SEVERE, "Could not save quests.", ex);
        }

        unloadAddons();
    }

    private void unloadAddons()
    {
        for (BetterQuestsAddon addon : addons) {
            addon.onDisable();
        }
    }

    private BQCommand getBQCmd(CommandSender sender, Command command, String label, String[] args)
    {
        switch(command.getName())
        {
            case "betterquests":
                return new CommandBetterQuests(this, sender, command, label, args);
            case "bq_callback":
                return new CommandBQCallback(this, sender, command, label, args);
            case "quest":
                return new CommandQuest(this, sender, command, label, args);
            case "itemnbt":
                sender.sendMessage(Items.toNBTString(((Player) sender).getInventory().getItemInMainHand()));
                break;
        }

        return null;
    }

    private static BetterQuestsPlugin instance = null;

    /**
     * Retrieves the plugin instance.
     *
     * @return the BetterQuestsPlugin singleton
     */
    public static BetterQuestsPlugin getInstance()
    {
        return instance;
    }
}
