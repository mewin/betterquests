/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.requirements.conversation.QuestRequirementPrompt;
import de.mewin.betterquests.requirements.factories.QuestRequirementFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class QuestRequirement extends Requirement implements StringAcceptor
{
    private String questId;

    public QuestRequirement(RequirementsHolder holder)
    {
        super(holder);
    }

    public QuestRequirement(RequirementsHolder holder, String questId)
    {
        super(holder);
        this.questId = questId;
    }

    public String getQuestId()
    {
        return questId;
    }

    public void setQuestId(String questId)
    {
        this.questId = questId;
    }

    @Override
    public boolean testRequirement(Player player)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        ProgressManager progressManager = plugin.getProgressManager();
        Quest finishedQuest = questRegistry.findQuest(questId);

        if (finishedQuest == null) {
            return false;
        }

        return progressManager.getFinishedQuest(player, finishedQuest) != null;
    }

    @Override
    public String getFailMessage(Player player)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest finishedQuest = questRegistry.findQuest(questId);

        String title = finishedQuest == null ? questId : CHAT_HELPER.formatPlayerString(finishedQuest.getTitle(), player);

        return CHAT_HELPER.getMessage(player, "requirement-quest-missing", title);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "requirement-quest", questId);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RequirementsHolder base, CommandSender sender)
    {
        return new QuestRequirementPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return QuestRequirementFactory.MY_ID;
    }

    @Override
    public String getString()
    {
        return questId;
    }

    @Override
    public boolean setString(String value)
    {
        if (!Quest.isValidQuestId(value)) {
            return false;
        }

        questId = value;

        return true;
    }
}
