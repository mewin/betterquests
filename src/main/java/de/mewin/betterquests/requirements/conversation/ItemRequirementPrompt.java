/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements.conversation;

import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.requirements.ItemRequirement;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemAmountAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemDamageAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemMaterialAcceptorPrompt;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemRequirementPrompt extends RequirementPrompt
{
    private final ItemRequirement itemRequirement;

    public ItemRequirementPrompt(ItemRequirement requirement, RequirementsHolder holder, MenuPrompt parentPrompt, CommandSender sender)
    {
        super(requirement, holder, parentPrompt);

        this.itemRequirement = requirement;
    }

    @Override
    protected void fillOptions(final CommandSender sender)
    {
        ItemStack items = itemRequirement.getItemStack();

        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-material", items.getType().toString()),
                        new ItemMaterialAcceptorPrompt(itemRequirement, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-amount", items.getAmount()),
                        new ItemAmountAcceptorPrompt(itemRequirement, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-meta", items.getDurability()),
                        new ItemDamageAcceptorPrompt(itemRequirement, this));

        if (sender instanceof Player)
        {
            addComplexOption(CHAT_HELPER.getMessage(sender, "menu-item-fromhand"),
            new PromptSupplier()
            {
                @Override
                public Prompt get()
                {
                    Player player = (Player) sender;
                    ItemStack stack = player.getInventory().getItemInMainHand();
                    if (stack == null) { // just in case
                        stack = new ItemStack(Material.AIR, 0);
                    }
                    itemRequirement.setItemStack(stack);
                    ItemRequirementPrompt.this.reloadMenu();
                    return ItemRequirementPrompt.this;
                }
            });
        }

        super.fillOptions(sender);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());
        settings.add(new KnownSetting(ItemRequirement.IGNORE_META, DataType.BOOLEAN));
        settings.add(new KnownSetting(ItemRequirement.TAKE_ITEMS, DataType.BOOLEAN));
        return settings;
    }
}
