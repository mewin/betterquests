/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements.conversation;

import de.mewin.betterquests.conversation.acceptors.QuestAcceptorPrompt;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.requirements.QuestRequirement;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class QuestRequirementPrompt extends RequirementPrompt
{
    private final QuestRequirement questRequirement;

    public QuestRequirementPrompt(QuestRequirement requirement, RequirementsHolder holder, MenuPrompt parentPrompt)
    {
        super(requirement, holder, parentPrompt);
        this.questRequirement = requirement;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-quest", questRequirement.getQuestId()),
                new QuestAcceptorPrompt(this, "prompt-quest", questRequirement));

        super.fillOptions(receiver);
    }
}
