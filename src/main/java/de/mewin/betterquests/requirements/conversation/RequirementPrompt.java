/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements.conversation;

import de.mewin.betterquests.conversation.editors.CustomizableEditorPrompt;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.quest.extra.RequirementSettings;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class RequirementPrompt extends CustomizableEditorPrompt<RequirementsHolder, Requirement>
{
    public RequirementPrompt(Requirement requirement, RequirementsHolder holder, MenuPrompt parentPrompt)
    {
        super(requirement, holder, parentPrompt, "advanced-requirement");
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-edit-requirement");
    }

    @Override
    protected void doDelete()
    {
        base.removeRequirement(object);
    }

    @Override
    protected String getString(Strings id)
    {
        switch(id)
        {
            case MENU_DELETE:
                return "menu-delete-requirement";
            default:
                return "<missing string>";
        }
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        return RequirementSettings.getKnownSettingsList();
    }
}
