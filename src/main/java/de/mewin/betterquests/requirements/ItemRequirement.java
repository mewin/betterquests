/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.requirements.conversation.ItemRequirementPrompt;
import de.mewin.betterquests.requirements.factories.ItemRequirementFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemStackAcceptor;
import de.mewin.mewinbukkitlib.util.Items;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemRequirement extends Requirement implements ItemStackAcceptor
{
    private ItemStack items;

    public ItemRequirement(RequirementsHolder holder)
    {
        super(holder);
        items = new ItemStack(Material.AIR, 0);
    }

    public ItemRequirement(RequirementsHolder holder, ItemStack items)
    {
        super(holder);
        this.items = items;
    }

    @Override
    public ItemStack getItemStack()
    {
        return items;
    }

    @Override
    public void setItemStack(ItemStack itemStack)
    {
        items = new ItemStack(itemStack);
    }

    @Override
    public boolean testRequirement(Player player)
    {
        if (items == null) {
            return true;
        }

        int amount = 0;
        boolean ignoreDamage = getSettings().getBoolean(IGNORE_META, false);

        for (ItemStack stack : player.getInventory().getContents()) {
            if (Items.compareStacks(stack, items, ignoreDamage)) {
                amount += stack.getAmount();
                if (amount >= items.getAmount()) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "requirement-item");
    }

    @Override
    public String getFactoryId()
    {
        return ItemRequirementFactory.MY_ID;
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RequirementsHolder holder, CommandSender sender)
    {
        return new ItemRequirementPrompt(this, holder, parentPrompt, sender);
    }

    @Override
    public String getFailMessage(Player player)
    {
        return CHAT_HELPER.getMessage(player, "requirement-item-missing", items.getAmount(), items.getType().toString());
    }

    @Override
    public void onApply(Player player)
    {
        if (getSettings().getBoolean(TAKE_ITEMS, false)) {
            boolean ignoreMeta = getSettings().getBoolean(IGNORE_META, false);
            Items.removeFromInventory(player.getInventory(), items, ignoreMeta);
        }
    }

    public static final String IGNORE_META = "ignore-meta";
    public static final String TAKE_ITEMS = "take-items";
}
