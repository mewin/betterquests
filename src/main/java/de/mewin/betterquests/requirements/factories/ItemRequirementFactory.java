/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements.factories;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.factory.RequirementFactory;
import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.requirements.ItemRequirement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemRequirementFactory extends RequirementFactory
{
    public static final String MY_ID = "item-stack";

    @Override
    public Requirement createNew(RequirementsHolder holder)
    {
        ItemRequirement requirement = new ItemRequirement(holder);
        holder.addRequirement(requirement);
        return requirement;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "requirement-item-factory");
    }

    @Override
    public Requirement fromYaml(RequirementsHolder holder, Map<String, Object> yaml) throws IOException
    {
        Object itemsObj = yaml.get("items");
        if (!(itemsObj instanceof Map)) {
            return new ItemRequirement(holder);
        }
        Map<String, Object> items = (Map<String, Object>) itemsObj;

        return new ItemRequirement(holder, Serializer.deserializeItemStack(items));
    }

    @Override
    public Map<String, Object> toYaml(Requirement object) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();
        ItemRequirement requirement = (ItemRequirement) object;

        ItemStack items = requirement.getItemStack();

        if (items != null) {
            map.put("items", Serializer.serializeItemStack(items));
        }

        return map;
    }
}
