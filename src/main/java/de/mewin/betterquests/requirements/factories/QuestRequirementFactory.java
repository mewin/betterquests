/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.requirements.factories;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;
import de.mewin.betterquests.quest.factory.RequirementFactory;
import de.mewin.betterquests.requirements.QuestRequirement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class QuestRequirementFactory extends RequirementFactory
{
    @Override
    public Requirement createNew(RequirementsHolder holder)
    {
        QuestRequirement requirement = new QuestRequirement(holder);
        holder.addRequirement(requirement);
        return requirement;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "requirement-quest-factory");
    }

    @Override
    public Requirement fromYaml(RequirementsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String questId = "";
        Object tmp;

        if ((tmp = yaml.get("quest")) != null) {
            questId = tmp.toString();
        }

        return new QuestRequirement(holder, questId);
    }

    @Override
    public Map<String, Object> toYaml(Requirement requirement) throws IOException
    {
        QuestRequirement questRequirement = (QuestRequirement) requirement;

        HashMap<String, Object> map = new HashMap<>();

        map.put("quest", questRequirement.getQuestId());

        return map;
    }

    public static final String MY_ID = "quest";
}
