/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conditions;

import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public interface AdvertisementCondition
{
    public boolean test(Player player);
    public String getMessage(Player player);
}
