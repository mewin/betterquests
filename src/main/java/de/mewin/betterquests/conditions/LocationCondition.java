/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conditions;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class LocationCondition implements AdvertisementCondition
{
    private final Location location;
    private final double distSq;

    public LocationCondition(Location location, double distance)
    {
        this.location = location;
        this.distSq = distance * distance;
    }

    @Override
    public boolean test(Player player)
    {
        Location playerLoc = player.getLocation();

        if (!playerLoc.getWorld().equals(location.getWorld())) {
            return false;
        }

        return playerLoc.distanceSquared(location) <= distSq;
    }

    @Override
    public String getMessage(Player player)
    {
        return CHAT_HELPER.getMessage(player, "condition-location");
    }

}
