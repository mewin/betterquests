/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conditions;

import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TimeoutCondition implements AdvertisementCondition
{
    private final long timeoutTime;

    public TimeoutCondition(long timeoutTime)
    {
        this.timeoutTime = timeoutTime;
    }

    @Override
    public boolean test(Player player)
    {
        return System.currentTimeMillis() <= timeoutTime;
    }

    @Override
    public String getMessage(Player player)
    {
        return CHAT_HELPER.getMessage(player, "condition-timeout");
    }

}
