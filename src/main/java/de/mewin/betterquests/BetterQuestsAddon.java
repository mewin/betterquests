/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

/**
 * An addon to BetterQuests.
 *
 * Used internally to bundle functionality that can be en- and disabled together.
 * Addons are saved inside the plugin class and are en-/disabled together with
 * the actual plugin.
 *
 * @author mewin
 */
public abstract class BetterQuestsAddon
{
    /**
     * Called when the plugin is enabled.
     */
    public void onEnable() {}
    /**
     * Called when the plugin is disabled.
     */
    public void onDisable() {}
}
