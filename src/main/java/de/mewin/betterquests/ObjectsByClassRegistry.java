/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class to store objects sorted by their type.
 *
 * This can be used to store similar objects and quickly retrieve all objects
 * that have exactly the given type.
 *
 * @param <T> The base type for all objects to store
 * @author mewin
 */
public class ObjectsByClassRegistry<T>
{
    private final HashMap<Class<?>, ArrayList<T>> registeredObjects;

    public ObjectsByClassRegistry()
    {
        this.registeredObjects = new HashMap<>();
    }

    private ArrayList<T> getObjects(Class<?>clazz)
    {
        ArrayList<T> list = registeredObjects.get(clazz);
        if (list == null)
        {
            list = new ArrayList<>();
            registeredObjects.put(clazz, list);
        }
        return list;
    }

    public void register(Class<?> clazz, T object)
    {
        ArrayList<T> objects = getObjects(clazz);

        objects.add(object);
    }

    public void register(T object)
    {
        register(object.getClass(), object);
    }

    public void unregister(Class<?> clazz, T object)
    {
        ArrayList<T> objects = getObjects(clazz);

        objects.remove(object);
    }

    public void unregister(T object)
    {
        unregister(object.getClass(), object);
    }

    public List<T> getRegistered(Class<?> clazz)
    {
        return Collections.unmodifiableList(getObjects(clazz));
    }

    public Map<Class<?>, ArrayList<T>> getRegisteredObjects()
    {
        return Collections.unmodifiableMap(registeredObjects);
    }
}
