/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.io;

import de.mewin.betterquests.progress.ProgressManager;
import java.io.IOException;

/**
 *
 * @author mewin
 */
public class DummyProgressManagerBackend extends ProgressManagerBackend
{

    @Override
    public void loadProgress(ProgressManager progressTracker) throws IOException
    {
        // meh
    }

    @Override
    public void saveProgress(ProgressManager progressTracker) throws IOException
    {
        // meh
    }

}
