/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.io;

import com.google.common.io.Files;
import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.Stage;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;
import de.mewin.betterquests.quest.factory.RequirementFactory;
import de.mewin.betterquests.quest.factory.RewardFactory;
import de.mewin.betterquests.quest.factory.TriggerFactory;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author mewin
 */
public class YamlQuestRegistryBackend extends QuestRegistryBackend
{
    private final File file;

    public YamlQuestRegistryBackend(File file)
    {
        this.file = file;
    }

    @Override
    public void loadQuests(QuestRegistry registry) throws IOException
    {
        if (!file.exists())
        {
            File newFile = new File(file.getPath() + ".new");
            if (newFile.exists()) {
                throw new IOException("apparently there was an error while saving the quests");
            }
            return; // nothing to load
        }

        Yaml yaml = new Yaml();
        try(FileReader reader = new FileReader(file))
        {
            Object data = yaml.load(reader);

            if (!(data instanceof Map)) {
                throw new IOException("invalid quests.yml file");
            }

            questsFromYaml(registry, (Map<String, Object>) data);
        }
    }

    @Override
    public void saveQuests(QuestRegistry registry) throws IOException
    {
        // write quests to <file>.new
        File newFile = new File(file.getPath() + ".new");
        Yaml yaml = new Yaml();
        try(FileWriter writer = new FileWriter(newFile))
        {
            Map<String, Object> data = questsToYaml(registry);

            yaml.dump(data, writer);
        }

        // backup old file
        if (file.exists())
        {
            File bakFile = new File(file.getPath() + ".bak");
            Files.move(file, bakFile);
        }
        // move new file
        Files.move(newFile, file);
    }

    public static void questsFromYaml(QuestRegistry registry, Map<String, Object> map) throws IOException
    {
        for(Map.Entry<String, Object> entry : map.entrySet())
        {
            String id = entry.getKey();
            Object value = entry.getValue();
            if (!(value instanceof Map)) {
                continue;
            }
            Quest quest = questFromYaml(id, (Map<String, Object>) entry.getValue());

            try {
                registry.registerQuest(quest);
            }
            catch(QuestRegistry.DuplicateQuestException ex) {

            }
        }
    }

    public static Quest questFromYaml(String id, Map<String, Object> map) throws IOException
    {
        Quest quest = new Quest(id);

        Object descriptionObj = map.get("description");
        if (descriptionObj != null) {
            quest.setDescription(descriptionObj.toString());
        }

        Object titleObj = map.get("title");
        if (titleObj != null) {
            quest.setTitle(titleObj.toString());
        }

        Object uuidObj = map.get("uuid");
        if (uuidObj == null || !(uuidObj instanceof String)) {
            throw new IOException("invalid quest, missing or invalid uuid");
        }
        try {
            quest.setUuid(UUID.fromString((String) uuidObj));
        } catch(IllegalArgumentException ex) {
            throw new IOException("invalid quest, invalid uuid", ex);
        }

        Object settingsObj = map.get("settings");
        if (settingsObj == null || !(settingsObj instanceof Map)) {
            throw new IOException("invalid quest, missing or invalid settings");
        }

        quest.getSettings().setValues((Map<String, Object>) settingsObj);

        Object triggersObj = map.get("triggers");
        if (triggersObj == null || !(triggersObj instanceof List)) {
            throw new IOException("invalid quest, missing or invalid triggers");
        }

        List<Object> triggers = (List<Object>) triggersObj;

        for (Object triggerObj : triggers)
        {
            if (!(triggerObj instanceof Map)) {
                throw new IOException("invalid requirement, invalid type");
            }
            Map<String, Object> triggerMap = (Map<String, Object>) triggerObj;
            Trigger trigger = triggerFromYaml(quest, triggerMap);

            quest.addTrigger(trigger);
        }

        Object requirementsObj = map.get("requirements");
        if (requirementsObj == null || !(requirementsObj instanceof List)) {
            throw new IOException("invalid quest, missing or invalid requirements");
        }

        List<Object> requirements = (List<Object>) requirementsObj;

        for (Object requirementObj : requirements)
        {
            if (!(requirementObj instanceof Map)) {
                throw new IOException("invalid requirement, invalid type");
            }
            Map<String, Object> requirementMap = (Map<String, Object>) requirementObj;
            Requirement requirement = requirementFromYaml(quest, requirementMap);

            quest.addRequirement(requirement);
        }

        Object stagesObj = map.get("stages");
        if (stagesObj == null || !(stagesObj instanceof List)) {
            throw new IOException("invalid quest, missing or invalid stages");
        }

        List<Object> stages = (List<Object>) stagesObj;

        for (Object stageObj : stages)
        {
            if (!(stageObj instanceof Map)) {
                throw new IOException("invalid stage, invalid type");
            }
            Map<String, Object> stageMap = (Map<String, Object>) stageObj;
            Stage stage = stageFromYaml(quest, stageMap);

            quest.appendStage(stage);
        }

        return quest;
    }

    public static Trigger triggerFromYaml(Quest quest, Map<String, Object> map) throws IOException
    {
        Object typeObj = map.get("type");
        if (typeObj == null || !(typeObj instanceof String)) {
            throw new IOException("invalid trigger, invalid of missing type parameter");
        }

        Object settingsObj = map.get("settings");
        if (settingsObj == null || !(settingsObj instanceof Map)) {
            throw new IOException("invalid trigger, invalid or missing settings parameter");
        }

        Map<String, Object> settings = (Map<String, Object>) settingsObj;

        String type = (String) typeObj;
        TriggerFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findTriggerFactory(type);
        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing trigger factory \"" + type + "\", TODO: dummy factory");
        }

        Trigger trigger = factory.fromYaml(quest, map);
        trigger.getSettings().setValues(settings);

        return trigger;
    }

    public static Requirement requirementFromYaml(Quest quest, Map<String, Object> map) throws IOException
    {
        Object typeObj = map.get("type");
        if (typeObj == null || !(typeObj instanceof String)) {
            throw new IOException("invalid requirement, invalid of missing type parameter");
        }

        String type = (String) typeObj;
        RequirementFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findRequirementFactory(type);
        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing requirement factory \"" + type + "\", TODO: dummy factory");
        }

        Object settingsObj = map.get("settings");
        if (settingsObj == null || !(settingsObj instanceof Map)) {
            throw new IOException("invalid requirement, invalid or missing settings parameter");
        }

        Map<String, Object> settings = (Map<String, Object>) settingsObj;

        Requirement requirement = factory.fromYaml(quest, map);
        requirement.getSettings().setValues(settings);
        return requirement;
    }

    public static Stage stageFromYaml(Quest quest, Map<String, Object> map) throws IOException
    {
        Stage stage = new Stage(quest);

        Object objectivesObj = map.get("objectives");
        if (objectivesObj == null || !(objectivesObj instanceof List)) {
            throw new IOException("invalid stage, missing or invalid objectives");
        }

        List<Object> objectives = (List<Object>) objectivesObj;

        for (Object objectiveObj : objectives)
        {
            if (!(objectiveObj instanceof Map)) {
                throw new IOException("invalid objective, invalid type");
            }

            Map<String, Object> objectiveMap = (Map<String, Object>) objectiveObj;
            Objective objective = objectiveFromYaml(stage, objectiveMap);

            stage.addObjective(objective);
        }

        Object rewardsObj = map.get("rewards");
        if (rewardsObj == null || !(rewardsObj instanceof List)) {
            throw new IOException("invalid stage, missing or invalid rewards");
        }

        List<Object> rewards = (List<Object>) rewardsObj;

        for (Object rewardObj : rewards)
        {
            if (!(rewardObj instanceof Map)) {
                throw new IOException("invalid reward, invalid type");
            }

            Map<String, Object> rewardMap = (Map<String, Object>) rewardObj;
            Reward reward = rewardFromYaml(stage, rewardMap);

            stage.addReward(reward);
        }

        Object uuidObj = map.get("uuid");
        if (uuidObj == null || !(uuidObj instanceof String)) {
            throw new IOException("invalid stage, missing or invalid uuid");
        }
        try {
            stage.setUuid(UUID.fromString((String) uuidObj));
        }
        catch(IllegalArgumentException ex) {
            throw new IOException("invalid stage, invalid uuid", ex);
        }

        Object settingsObj = map.get("settings");
        if (settingsObj == null || !(settingsObj instanceof Map)) {
            throw new IOException("invalid stage, invalid or missing settings parameter");
        }

        Map<String, Object> settings = (Map<String, Object>) settingsObj;

        stage.getSettings().setValues(settings);

        return stage;
    }

    public static Objective objectiveFromYaml(ObjectivesHolder holder, Map<String, Object> map) throws IOException
    {
        Object typeObj = map.get("type");
        if (typeObj == null || !(typeObj instanceof String)) {
            throw new IOException("invalid objective, missing or invalid type parameter");
        }
        String type = (String) typeObj;
        ObjectiveFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findObjectiveFactory(type);
        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing objective factory \"" + type + "\", TODO: dummy factory");
        }

        Objective objective = factory.fromYaml(holder, map);

        Object uuidObj = map.get("uuid");
        if (uuidObj == null || !(uuidObj instanceof String)) {
            throw new IOException("invalid objective, missing or invalid uuid parameter");
        }

        try {
            objective.setUuid(UUID.fromString((String) uuidObj));
        }
        catch(IllegalArgumentException ex) {
            throw new IOException("invalid objective, invalid uuid", ex);
        }

        Object settingsObj = map.get("settings");
        if (settingsObj == null || !(settingsObj instanceof Map)) {
            throw new IOException("invalid objective, invalid or missing settings parameter");
        }

        Map<String, Object> settings = (Map<String, Object>) settingsObj;

        objective.getSettings().setValues(settings);

        return objective;
    }

    public static Reward rewardFromYaml(Stage stage, Map<String, Object> map) throws IOException
    {
        Object typeObj = map.get("type");
        if (typeObj == null || !(typeObj instanceof String)) {
            throw new IOException("invalid reward, missing or invalid type parameter");
        }
        String type = (String) typeObj;
        RewardFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findRewardFactory(type);
        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing reward factory \"" + type + "\", TODO: dummy factory");
        }


        Object settingsObj = map.get("settings");
        if (settingsObj == null || !(settingsObj instanceof Map)) {
            throw new IOException("invalid reward, invalid or missing settings parameter");
        }

        Map<String, Object> settings = (Map<String, Object>) settingsObj;

        Reward reward = factory.fromYaml(stage, map);
        reward.getSettings().setValues(settings);
        return reward;
    }

    public static Map<String, Object> questsToYaml(QuestRegistry registry) throws IOException
    {
        HashMap<String, Object> data = new HashMap<>();

        for (Map.Entry<String, Quest> entry : registry.getQuests().entrySet())
        {
            String id = entry.getKey();
            Quest quest = entry.getValue();

            data.put(id, questToYaml(quest));
        }

        return data;
    }

    public static Map<String, Object> questToYaml(Quest quest) throws IOException
    {
        HashMap<String, Object> data = new HashMap<>();

        data.put("description", quest.getDescription());
        data.put("title", quest.getTitle());
        data.put("uuid", quest.getUuid().toString());
        data.put("settings", quest.getSettings().getValues());

        List<Object> triggers = new ArrayList<>();

        for (Trigger trigger : quest.getTriggers())
        {
            Map<String, Object> triggerMap = triggerToYaml(trigger);
            triggers.add(triggerMap);
        }

        data.put("triggers", triggers);


        List<Object> requirements = new ArrayList<>();

        for (Requirement requirement : quest.getRequirements())
        {
            Map<String, Object> requirementMap = requirementToYaml(requirement);
            requirements.add(requirementMap);
        }

        data.put("requirements", requirements);


        List<Object> stages = new ArrayList<>();

        for (Stage stage : quest.getStages())
        {
            Map<String, Object> stageMap = stageToYaml(stage);

            stages.add(stageMap);
        }

        data.put("stages", stages);

        return data;
    }

    public static Map<String, Object> triggerToYaml(Trigger trigger) throws IOException
    {
        String type = trigger.getFactoryId();
        TriggerFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findTriggerFactory(type);

        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing trigger factory \"" + type + "\", TODO: dummy factory");
        }
        Map<String, Object> triggerMap = factory.toYaml(trigger);
        triggerMap.put("type", type);
        triggerMap.put("settings", trigger.getSettings().getValues());
        return triggerMap;
    }

    public static Map<String, Object> requirementToYaml(Requirement requirement) throws IOException
    {
        String type = requirement.getFactoryId();
        RequirementFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findRequirementFactory(type);

        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing requirement factory \"" + type + "\", TODO: dummy factory");
        }
        Map<String, Object> requirementMap = factory.toYaml(requirement);
        requirementMap.put("type", type);
        requirementMap.put("settings", requirement.getSettings().getValues());
        return requirementMap;
    }

    public static Map<String, Object> stageToYaml(Stage stage) throws IOException
    {
        Map<String, Object> data = new HashMap<>();

        List<Object> objectives = new ArrayList<>();

        for (Objective objective : stage.getObjectives())
        {
            Map<String, Object> objectiveMap = objectiveToYaml(objective);

            objectives.add(objectiveMap);
        }

        data.put("objectives", objectives);


        List<Object> rewards = new ArrayList<>();

        for (Reward reward : stage.getRewards())
        {
            Map<String, Object> rewardMap = rewardToYaml(reward);

            rewards.add(rewardMap);
        }

        data.put("rewards", rewards);

        data.put("uuid", stage.getUuid().toString());
        data.put("settings", stage.getSettings().getValues());

        return data;
    }

    public static Map<String, Object> objectiveToYaml(Objective objective) throws IOException
    {

        String type = objective.getFactoryId();
        ObjectiveFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findObjectiveFactory(type);

        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing objective factory \"" + type + "\", TODO: dummy factory");
        }
        Map<String, Object> objectiveMap = factory.toYaml(objective);
        objectiveMap.put("type", type);
        objectiveMap.put("uuid", objective.getUuid().toString());
        objectiveMap.put("settings", objective.getSettings().getValues());
        return objectiveMap;
    }

    public static Map<String, Object> rewardToYaml(Reward reward) throws IOException
    {

        String type = reward.getFactoryId();
        RewardFactory factory = BetterQuestsPlugin.
                getInstance()
                .getFactoryRegistry()
                .findRewardFactory(type);

        if (factory == null) {
            // TODO: use dummy factory
            throw new IOException("missing reward factory \"" + type + "\", TODO: dummy factory");
        }
        Map<String, Object> rewardMap = factory.toYaml(reward);
        rewardMap.put("type", type);
        rewardMap.put("settings", reward.getSettings().getValues());
        return rewardMap;
    }
}
