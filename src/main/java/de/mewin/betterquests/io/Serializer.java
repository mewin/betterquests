/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.io;

import de.mewin.mewinbukkitlib.world.LocationProxy;
import de.mewin.mewinbukkitlib.util.Pair;
import de.mewin.betterquests.util.Util;
import de.mewin.mewinbukkitlib.util.NbtFactory;
import de.mewin.mewinbukkitlib.util.NbtSerializer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

/**
 *
 * @author mewin
 */
public class Serializer
{
    // TODO: (de)serialization of item meta (bukkit serialize() doesnt work :/)
    public static boolean isYamlType(Object obj)
    {
        return (obj instanceof Number)
                || (obj instanceof String)
                || (obj instanceof Boolean)
                || (obj instanceof Map)
                || (obj instanceof List);
    }

    public static void stripNonYaml(List<?> list)
    {
        for (Iterator<?> it = list.iterator(); it.hasNext();)
        {
            Object obj = it.next();
            if (!isYamlType(obj)) {
                it.remove();
            }
            if (obj instanceof Map) {
                stripNonYaml((Map<?, ?>) obj);
            }
            else if (obj instanceof List) {
                stripNonYaml((List<?>) obj);
            }
        }
    }

    public static void stripNonYaml(Map<?, ?> map)
    {
        HashSet<Object> keysToRemove = new HashSet<>();
        for (Object key : map.keySet())
        {
            if (!(key instanceof String))
            {
                keysToRemove.add(key);
                continue;
            }
            Object value = map.get(key);
            if (!isYamlType(value))
            {
                keysToRemove.add(key);
                continue;
            }
            if (value instanceof Map) {
                stripNonYaml((Map<?, ?>) value);
            }
            else if (value instanceof List) {
                stripNonYaml((List<?>) value);
            }
        }

        for (Object key : keysToRemove)
        {
            map.remove(key);
        }
    }

    public static Object makeYaml(Object raw)
    {
        if (raw instanceof Map)
        {
            HashMap copy = new HashMap((Map) raw);
            stripNonYaml(copy);
            return copy;
        }
        if (raw instanceof List)
        {
            ArrayList copy = new ArrayList((List) raw);
            stripNonYaml(copy);
            return copy;
        }
        if (isYamlType(raw))
        {
            return raw;
        }
        return null;
    }

    public static Map<String, Object> serializeEnchantment(Enchantment enchantment, int level)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("name", enchantment.getName());
        map.put("level", level);

        return map;
    }

    public static List<Object> serializeEnchantments(Map<Enchantment, Integer> enchantments)
    {
        ArrayList<Object> list = new ArrayList<>();
        for (Enchantment enchantment : enchantments.keySet())
        {
            int level = enchantments.get(enchantment);
            list.add(serializeEnchantment(enchantment, level));
        }
        return list;
    }

    public static List<Object> serializeItemFlags(Set<ItemFlag> flags)
    {
        ArrayList<Object> list = new ArrayList<>();

        for (ItemFlag flag : flags) {
            list.add(flag.name());
        }

        return list;
    }

    public static Map<String, Object> serializeBannerPattern(Pattern pattern)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("color", pattern.getColor().name());
        map.put("type", pattern.getPattern().name());

        return map;
    }

    public static List<Object> serializeBannerPatterns(List<Pattern> patterns)
    {
        ArrayList<Object> list = new ArrayList<>();

        for (Pattern pattern : patterns)
        {
            list.add(serializeBannerPattern(pattern));
        }

        return list;
    }

    public static HashMap<String, Object> serializeColor(Color color)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("red", color.getRed());
        map.put("green", color.getGreen());
        map.put("blue", color.getBlue());

        return map;
    }

    public static List<Object> serializeColors(List<Color> colors)
    {
        ArrayList<Object> list = new ArrayList<>();

        for (Color color : colors) {
            list.add(serializeColor(color));
        }

        return list;
    }

    public static HashMap<String, Object> serializeFireworkEffect(FireworkEffect effect)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("colors", serializeColors(effect.getColors()));
        map.put("fade-colors", serializeColors(effect.getFadeColors()));
        map.put("type", effect.getType().name());
        if (effect.hasFlicker()) {
            map.put("flicker", true);
        }
        if (effect.hasTrail()) {
            map.put("trail", true);
        }

        return map;
    }

    public static HashMap<String, Object> serializePotionData(PotionData potionData)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("type", potionData.getType().name());
        if (potionData.isExtended()) {
            map.put("extended", true);
        }
        if (potionData.isUpgraded()) {
            map.put("upgraded", true);
        }

        return map;
    }

    public static HashMap<String, Object> serializePotionEffect(PotionEffect effect)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("amplifier", effect.getAmplifier());
        map.put("color", serializeColor(effect.getColor()));
        map.put("duration", effect.getDuration());
        map.put("type", effect.getType().getName());
        if (effect.hasParticles()) {
            map.put("particles", true);
        }
        if (effect.isAmbient()) {
            map.put("ambient", true);
        }

        return map;
    }

    public static List<Object> serializePotionEffects(List<PotionEffect> effects)
    {
        ArrayList<Object> list = new ArrayList<>();

        for (PotionEffect effect : effects) {
            list.add(serializePotionEffect(effect));
        }

        return list;
    }

    public static Map<String, Object> serializeBannerMeta(BannerMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("base-color", meta.getBaseColor().name()); // TODO: deprecated in 1.12
        map.put("patterns", serializeBannerPatterns(meta.getPatterns()));

        return map;
    }

    public static Map<String, Object> serializeBookMeta(BookMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        if (meta.hasAuthor()) {
            map.put("author", meta.getAuthor());
        }
        // TODO: enable when we have this method
//        if (meta.hasGeneration()) {
//            map.put("generation", meta.getGeneration().name());
//        }
        if (meta.hasPages()) {
            map.put("pages", meta.getPages());
        }
        if (meta.hasTitle()) {
            map.put("title", meta.getTitle());
        }

        return map;
    }

    public static Map<String, Object> serializeEnchantmentStorageMeta(EnchantmentStorageMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("enchantments", serializeEnchantments(meta.getStoredEnchants()));

        return map;
    }

    public static Map<String, Object> serializeFireworkEffectMeta(FireworkEffectMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        if (meta.hasEffect()) {
            map.put("effect", serializeFireworkEffect(meta.getEffect()));
        }

        return map;
    }

    public static Map<String, Object> serializeLeatherArmorMeta(LeatherArmorMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("color", serializeColor(meta.getColor()));

        return map;
    }

    public static Map<String, Object> serializeMapMeta(MapMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        // TODO: more stuff in 1.12
        if (meta.isScaling()) {
            map.put("scaling", true);
        }

        return map;
    }

    public static Map<String, Object> serializePotionMeta(PotionMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("base-data", serializePotionData(meta.getBasePotionData()));
        // if (meta.hasColor()) {
        // map.put("color", serializeColor(meta.getColor())); // TODO: 1.12
        // }
        if (meta.hasCustomEffects()) {
            map.put("custom-effects", serializePotionEffects(meta.getCustomEffects()));
        }

        return map;
    }

    public static Map<String, Object> serializeSkullMeta(SkullMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        if (meta.hasOwner()) {
            map.put("owner", meta.getOwner());
        }

        return map;
    }

    public static Map<String, Object> serializeItemMeta(ItemMeta meta)
    {
        HashMap<String, Object> map = new HashMap<>();

        if (meta.hasDisplayName()) {
            map.put("display-name", meta.getDisplayName());
        }
        if (meta.hasLore()) {
            map.put("lore", meta.getLore());
        }
        map.put("flags", serializeItemFlags(meta.getItemFlags()));
        // TODO: getLocalizedName() (in 1.12)
        // and other new stuff

        if (meta instanceof BannerMeta)
        {
            BannerMeta bannerMeta = (BannerMeta) meta;
            map.put("banner", serializeBannerMeta(bannerMeta));
        }

        // not yet
//        if (meta instanceof BlockStateMeta)
//        {
//            BlockStateMeta blockStateMeta = (BlockStateMeta) meta;
//        }
        if (meta instanceof BookMeta)
        {
            BookMeta bookMeta = (BookMeta) meta;
            map.put("book", serializeBookMeta(bookMeta));
        }

        if (meta instanceof EnchantmentStorageMeta)
        {
            EnchantmentStorageMeta enchantmentStorageMeta = (EnchantmentStorageMeta) meta;
            map.put("enchantment-storage", serializeEnchantmentStorageMeta(enchantmentStorageMeta));
        }

        if (meta instanceof FireworkEffectMeta)
        {
            FireworkEffectMeta fireworkEffectMeta = (FireworkEffectMeta) meta;
            map.put("firework-effect", serializeFireworkEffectMeta(fireworkEffectMeta));
        }

        // Coming soon (1.12)
        // if (meta instanceof KnowledgeBookMeta)

        if (meta instanceof LeatherArmorMeta)
        {
            LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) meta;
            map.put("leather-armor", serializeLeatherArmorMeta(leatherArmorMeta));
        }

        if (meta instanceof MapMeta)
        {
            MapMeta mapMeta = (MapMeta) meta;
            map.put("map", serializeMapMeta(mapMeta));
        }

        if (meta instanceof PotionMeta)
        {
            PotionMeta potionMeta = (PotionMeta) meta;
            map.put("potion", serializePotionMeta(potionMeta));
        }

        if (meta instanceof SkullMeta)
        {
            SkullMeta skullMeta = (SkullMeta) meta;
            map.put("skull", serializeSkullMeta(skullMeta));
        }

        // SpawnEggMeta (also coming soon)

        return map;
    }

    public static Map<String, Object> serializeItemNbt(ItemStack stack_)
    {
        if (stack_.getType() == Material.AIR) {
            return new HashMap<>(); // cannot get air nbt
        }
        ItemStack stack = NbtFactory.getCraftItemStack(stack_);
        if (!stack_.isSimilar(stack)) {
            return new HashMap<>(); // wtf is CraftItemStack doing?
        }
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(stack);
        return NbtSerializer.serializeCompound(compound);
    }

    public static Map<String, Object> serializeItemStack(ItemStack stack)
    {
        HashMap<String, Object> map = new HashMap<>();
        map.put("type", stack.getType().toString());
        map.put("amount", stack.getAmount());
        map.put("durability", stack.getDurability());

//        Map<Enchantment, Integer> enchantments = stack.getEnchantments();
//        if (!enchantments.isEmpty()) {
//            map.put("enchantments", serializeEnchantments(enchantments));
//        }
//
//        if (stack.hasItemMeta())
//        {
//            ItemMeta itemMeta = stack.getItemMeta();
//            map.put("meta", serializeItemMeta(itemMeta));
//        }
        map.put("nbt", serializeItemNbt(stack));

        return map;
    }

    public static List<String> deserializeStringList(List<?> list)
    {
        ArrayList<String> strings = new ArrayList<>();

        for (Object ele : list)
        {
            if (ele instanceof String) {
                strings.add((String) ele);
            }
        }

        return strings;
    }

    public static Pair<Enchantment, Integer> deserializeEnchantment(Map<?, ?> map)
    {
        Enchantment enchantment = null;
        int level = 0;
        Object tmp;

        if (map.containsKey("name") && ((tmp = map.get("name")) instanceof String))
        {
            enchantment = Enchantment.getByName((String) tmp);
        }

        if (map.containsKey("level") && ((tmp = map.get("level")) instanceof Number))
        {
            level = ((Number) tmp).intValue();
        }

        if (enchantment == null) {
            return null;
        }

        return new Pair<>(enchantment, level);
    }

    public static Map<Enchantment, Integer> deserializeEnchantments(List<?> list)
    {
        HashMap<Enchantment, Integer> map = new HashMap<>();

        for (Object ele : list)
        {
            if (ele instanceof Map)
            {
                Pair<Enchantment, Integer> enchantment = deserializeEnchantment((Map<?, ?>) ele);
                if (enchantment != null) {
                    map.put(enchantment.getFirst(), enchantment.getSecond());
                }
            }
        }

        return map;
    }

    public static ItemFlag[] deserializeItemFlags(List<?> list)
    {
        ArrayList<ItemFlag> flags = new ArrayList<>();

        for (Object ele : list)
        {
            if (ele instanceof String) {
                try
                {
                    flags.add(ItemFlag.valueOf((String) ele));
                }
                catch(IllegalArgumentException ex) {}
            }
        }

        return flags.toArray(new ItemFlag[0]);
    }

    public static Color deserializeColor(Map<?, ?> map)
    {
        int red = 0;
        int green = 0;
        int blue = 0;

        Object tmp;

        if (map.containsKey("red") && ((tmp = map.get("red")) instanceof Number)) {
            red = Util.limit(((Number) tmp).intValue(), 0, 255);
        }

        if (map.containsKey("green") && ((tmp = map.get("green")) instanceof Number)) {
            green = Util.limit(((Number) tmp).intValue(), 0, 255);
        }

        if (map.containsKey("blue") && ((tmp = map.get("blue")) instanceof Number)) {
            blue = Util.limit(((Number) tmp).intValue(), 0, 255);
        }

        return Color.fromRGB(red, green, blue);
    }

    public static List<Color> deserializeColors(List<?> list)
    {
        ArrayList<Color> colors = new ArrayList<>();

        for (Object ele : list)
        {
            if (ele instanceof Map) {
                colors.add(deserializeColor((Map<?, ?>) ele));
            }
        }

        return colors;
    }

    public static Pattern deserializeBannerPattern(Map<?, ?> map)
    {
        DyeColor color = DyeColor.BLACK;
        PatternType type = PatternType.BASE;
        Object tmp;

        if (map.containsKey("color") && ((tmp = map.get("color")) instanceof String))
        {
            try {
                color = DyeColor.valueOf((String) tmp);
            }
            catch(IllegalArgumentException ex) {}
        }

        if (map.containsKey("type") && ((tmp = map.get("type")) instanceof String))
        {
            try {
                type = PatternType.valueOf((String) tmp);
            }
            catch(IllegalArgumentException ex) {}
        }

        return new Pattern(color, type);
    }

    public static List<Pattern> deserializeBannerPatterns(List<?> list)
    {
        ArrayList<Pattern> patterns = new ArrayList<>();

        for (Object ele : list)
        {
            if (ele instanceof Map) {
                patterns.add(deserializeBannerPattern((Map<?, ?>) ele));
            }
        }

        return patterns;
    }

    public static FireworkEffect deserializeFireworkEffect(Map<?, ?> map)
    {
        Object tmp;

        FireworkEffect.Builder builder = FireworkEffect.builder();

        if (map.containsKey("colors") && ((tmp = map.get("colors")) instanceof List)) {
            builder.withColor(deserializeColors((List<?>) tmp));
        }

        if (map.containsKey("fade-colors") && ((tmp = map.get("fade-colors")) instanceof List)) {
            builder.withFade(deserializeColors((List<?>) tmp));
        }

        if (map.containsKey("type") && ((tmp = map.get("type")) instanceof String)) {
            try {
                builder.with(FireworkEffect.Type.valueOf((String) tmp));
            }
            catch(IllegalArgumentException ex) {}
        }

        if (map.containsKey("flicker") && ((tmp = map.get("flicker")) instanceof Boolean)
                && ((Boolean) tmp))
        {
            builder.withFlicker();
        }

        if (map.containsKey("trail") && ((tmp = map.get("trail")) instanceof Boolean)
                && ((Boolean) tmp))
        {
            builder.withTrail();
        }

        try {
            return builder.build();
        }
        catch(IllegalStateException ex) {
            return null;
        }
    }

    public static PotionData deserializePotionData(Map<?, ?> map)
    {
        PotionType type = PotionType.WATER;
        Object tmp;

        if (map.containsKey("type") && ((tmp = map.get("type")) instanceof String))
        {
            try {
                type = PotionType.valueOf((String) tmp);
            }
            catch(IllegalArgumentException ex) {}
        }

        boolean extended =
                (map.containsKey("extended")
                && ((tmp = map.get("extended")) instanceof Boolean)
                && (Boolean) tmp);

        boolean upgraded =
                (map.containsKey("upgraded")
                && ((tmp = map.get("upgraded")) instanceof Boolean)
                && (Boolean) tmp);

        return new PotionData(type, extended, upgraded);
    }

    public static PotionEffect deserializePotionEffect(Map<?, ?> map)
    {
        PotionEffectType type = PotionEffectType.ABSORPTION;
        int amplifier = 0;
        int duration = 0;
        boolean ambient = false;
        boolean particles = false;
        Color color = null;
        Object tmp;

        if (map.containsKey("amplifier") && ((tmp = map.get("amplifier")) instanceof Number)) {
            amplifier = ((Number) tmp).intValue();
        }

        if (map.containsKey("color") && ((tmp = map.get("color")) instanceof Map)) {
            color = deserializeColor((Map<?, ?>) tmp);
        }

        if (map.containsKey("duration") && ((tmp = map.get("duration")) instanceof Number)) {
            duration = ((Number) tmp).intValue();
        }

        if (map.containsKey("type") && ((tmp = map.get("type")) instanceof String))
        {
            PotionEffectType type_ = PotionEffectType.getByName((String) tmp);
            if (type_ != null) {
                type = type_;
            }
        }

        ambient = (map.containsKey("ambient")
                && ((tmp = map.get("ambient")) instanceof Boolean)
                && (Boolean) tmp);
        particles = (map.containsKey("particles")
                && ((tmp = map.get("particles")) instanceof Boolean)
                && (Boolean) tmp);

        if (color != null) {
            return new PotionEffect(type, duration, amplifier, ambient, particles, color);
        } else {
            return new PotionEffect(type, duration, amplifier, ambient, particles);
        }
    }

    public static List<PotionEffect> deserializePotionEffects(List<?> list)
    {
        ArrayList<PotionEffect> effects = new ArrayList<>();

        for (Object ele : list)
        {
            if (ele instanceof Map) {
                effects.add(deserializePotionEffect((Map<?, ?>) ele));
            }
        }

        return effects;
    }

    public static void deserializeBannerMeta(BannerMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("base-color") && ((tmp = map.get("base-color")) instanceof String)) {
            try {
                meta.setBaseColor(DyeColor.valueOf((String) tmp));
            }
            catch(IllegalArgumentException ex) {}
        }

        if (map.containsKey("patterns") && ((tmp = map.get("patterns")) instanceof List)) {
            meta.setPatterns(deserializeBannerPatterns((List<?>) tmp));
        }
    }

    public static void deserializeBookMeta(BookMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("author") && ((tmp = map.get("author")) instanceof String)) {
            meta.setAuthor((String) tmp);
        }

        if (map.containsKey("generation") && ((tmp = map.get("generation")) instanceof String)) {
            try {
                meta.setGeneration(BookMeta.Generation.valueOf((String) tmp));
            }
            catch(IllegalArgumentException ex) {}
        }

        if (map.containsKey("pages") && ((tmp = map.get("pages")) instanceof List)) {
            meta.setPages(deserializeStringList((List<?>) tmp));
        }

        if (map.containsKey("title") && ((tmp = map.get("title")) instanceof String)) {
            meta.setTitle((String) tmp);
        }
    }

    public static void deserializeEnchantmentStorageMeta(EnchantmentStorageMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("enchantments") && ((tmp = map.get("enchantments")) instanceof List))
        {
            Map<Enchantment, Integer> enchantments = deserializeEnchantments((List<?>) tmp);

            for (Map.Entry<Enchantment, Integer> enchantment : enchantments.entrySet())
            {
                meta.addStoredEnchant(enchantment.getKey(), enchantment.getValue(), true);
            }
        }
    }

    public static void deserializeFireworkEffectMeta(FireworkEffectMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("effect") && ((tmp = map.get("effect")) instanceof Map))
        {
            FireworkEffect effect = deserializeFireworkEffect((Map<?, ?>) tmp);
            if (effect != null) {
                meta.setEffect(effect);
            }
        }
    }

    public static void deserializeLeatherArmorMeta(LeatherArmorMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("color") && ((tmp = map.get("color")) instanceof Map)) {
            meta.setColor(deserializeColor((Map<?, ?>) tmp));
        }
    }

    public static void deserializeMapMeta(MapMeta meta, Map<?, ?> map)
    {
        Object tmp;

        meta.setScaling(map.containsKey("scaling")
                && ((tmp = map.get("scaling")) instanceof Boolean)
                && (Boolean) tmp);
    }

    public static void deserializePotionMeta(PotionMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("base-data") && ((tmp = map.get("base-data")) instanceof Map))
        {
            meta.setBasePotionData(deserializePotionData((Map<?, ?>) tmp));
        }

        if (map.containsKey("custom-effects") && ((tmp = map.get("custom-effects")) instanceof List))
        {
            List<PotionEffect> effects = deserializePotionEffects((List<?>) tmp);
            for (PotionEffect effect : effects) {
                meta.addCustomEffect(effect, true);
            }
        }
    }

    public static void deserializeSkullMeta(SkullMeta meta, Map<?, ?> map)
    {
        Object tmp;

        if (map.containsKey("owner") && ((tmp = map.get("owner")) instanceof String)) {
            meta.setOwner((String) tmp);
        }
    }

    public static ItemMeta deserializeItemMeta(Map<?, ?> map, Material material)
    {
        ItemMeta meta = Bukkit.getServer().getItemFactory().getItemMeta(material);
        Object tmp;

        if (map.containsKey("display-name") && ((tmp = map.get("display-name")) instanceof String)) {
            meta.setDisplayName((String) tmp);
        }

        if (map.containsKey("lore") && ((tmp = map.get("lore")) instanceof List)) {
            meta.setLore(deserializeStringList((List<?>) tmp));
        }

        if (map.containsKey("flags") && ((tmp = map.get("flags")) instanceof List)) {
            meta.addItemFlags(deserializeItemFlags((List<?>) tmp));
        }

        if (meta instanceof BannerMeta && map.containsKey("banner") && ((tmp = map.get("banner"))) instanceof Map) {
            deserializeBannerMeta((BannerMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof BookMeta && map.containsKey("book") && ((tmp = map.get("book"))) instanceof Map) {
            deserializeBookMeta((BookMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof EnchantmentStorageMeta && map.containsKey("enchantment-storage") && ((tmp = map.get("enchantment-storage"))) instanceof Map) {
            deserializeEnchantmentStorageMeta((EnchantmentStorageMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof FireworkEffectMeta && map.containsKey("firework-effect") && ((tmp = map.get("firework-effect"))) instanceof Map) {
            deserializeFireworkEffectMeta((FireworkEffectMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof LeatherArmorMeta && map.containsKey("leather-armor") && ((tmp = map.get("leather-armor"))) instanceof Map) {
            deserializeLeatherArmorMeta((LeatherArmorMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof MapMeta && map.containsKey("map") && ((tmp = map.get("map"))) instanceof Map) {
            deserializeMapMeta((MapMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof PotionMeta && map.containsKey("potion") && ((tmp = map.get("potion"))) instanceof Map) {
            deserializePotionMeta((PotionMeta) meta, (Map<?, ?>) tmp);
        }

        if (meta instanceof SkullMeta && map.containsKey("skull") && ((tmp = map.get("skull"))) instanceof Map) {
            deserializeSkullMeta((SkullMeta) meta, (Map<?, ?>) tmp);
        }

        return meta;
    }

    public static ItemStack deserializeItemNbt(ItemStack stack_, Map<String, Object> map)
    {
        if (stack_.getType() == Material.AIR) {
            return stack_; // empty stack
        }
        ItemStack stack = NbtFactory.getCraftItemStack(stack_);
        if (!stack.isSimilar(stack_)) {
            return stack_; // wtf is CraftItemStack doing?!
        }
        NbtFactory.NbtCompound nbt = NbtSerializer.deserializeCompound(map);
        NbtFactory.setItemTag(stack, nbt);
        return stack;
    }

    public static ItemStack deserializeItemStack(Map<String, Object> map)
    {
        ItemStack stack = new ItemStack(Material.AIR);
        Object tmp;

        if (map.containsKey("type") && ((tmp = map.get("type")) instanceof String)) {
            Material mat;
            if ((mat = Material.getMaterial((String) tmp)) != null) {
                stack.setType(mat);
            }
        }

        if (map.containsKey("amount") && ((tmp = map.get("amount")) instanceof Number)) {
            stack.setAmount(((Number) tmp).intValue());
        }

        if (map.containsKey("durability") && ((tmp = map.get("durability")) instanceof Number)) {
            stack.setDurability(((Number) tmp).shortValue());
        }

        // old save format
        if (map.containsKey("meta") && ((tmp = map.get("meta")) instanceof Map)) {
            stack.setItemMeta(deserializeItemMeta((Map<?, ?>) tmp, stack.getType()));
        }

        if (map.containsKey("enchantments") && ((tmp = map.get("enchantments")) instanceof List)) {
            stack.addUnsafeEnchantments(deserializeEnchantments((List<?>) tmp));
        }

        if (map.containsKey("nbt") && ((tmp = map.get("nbt")) instanceof Map)) {
            stack = deserializeItemNbt(stack, (Map<String, Object>) tmp);
        }

        return stack;
    }

    public static Map<String, Object> serializeLocation(Location location)
    {
        HashMap<String, Object> map = new HashMap<>();
        map.put("world", location.getWorld().getUID().toString());
        map.put("x", location.getX());
        map.put("y", location.getY());
        map.put("z", location.getZ());
        return map;
    }

    public static Map<String, Object> serializeLocationProxy(LocationProxy proxy)
    {
        return serializeLocation(proxy.getLocation());
    }

    public static LocationProxy deserializeLocation(Map<String, Object> map)
    {
        String world = "";
        Object tmp;

        if (map.containsKey("world") && ((tmp = map.get("world")) instanceof String)) {
            world = tmp.toString();
        }

        double x = 0;
        double y = 0;
        double z = 0;

        if (map.containsKey("x") && ((tmp = map.get("x")) instanceof Number)) {
            x = ((Number) tmp).doubleValue();
        }

        if (map.containsKey("y") && ((tmp = map.get("y")) instanceof Number)) {
            y = ((Number) tmp).doubleValue();
        }

        if (map.containsKey("z") && ((tmp = map.get("z")) instanceof Number)) {
            z = ((Number) tmp).doubleValue();
        }

        return new LocationProxy(world, x, y, z);
    }
}
