/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.io;

import de.mewin.betterquests.quest.QuestRegistry;
import java.io.IOException;

/**
 *
 * @author mewin
 */
public abstract class QuestRegistryBackend
{
    public abstract void loadQuests(QuestRegistry registry) throws IOException;
    public abstract void saveQuests(QuestRegistry registry) throws IOException;
}
