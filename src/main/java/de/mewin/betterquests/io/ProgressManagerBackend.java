/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.io;

import de.mewin.betterquests.progress.ProgressManager;
import java.io.IOException;

/**
 *
 * @author mewin
 */
public abstract class ProgressManagerBackend
{
    public abstract void loadProgress(ProgressManager progressTracker) throws IOException;
    public abstract void saveProgress(ProgressManager progressTracker) throws IOException;
}
