/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.io;

import com.google.common.io.Files;
import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.FinishedQuestInfo;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.quest.Stage;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author mewin
 */
public class YamlProgressManagerBackend extends ProgressManagerBackend
{
    private final File file;
    private final FactoryRegistry factoryRegistry;
    private final QuestRegistry questRegistry;

    public YamlProgressManagerBackend(File file, FactoryRegistry factoryRegistry, QuestRegistry questRegistry)
    {
        this.file = file;
        this.factoryRegistry = factoryRegistry;
        this.questRegistry = questRegistry;
    }

    @Override
    public void loadProgress(ProgressManager progressTracker) throws IOException
    {
        if (!file.exists())
        {
            File newFile = new File(file.getPath() + ".new");
            if (newFile.exists()) {
                throw new IOException("apparently there was an error while saving the progress");
            }
            return;
        }

        Yaml yaml = new Yaml();
        try(FileReader reader = new FileReader(file))
        {
            Object data = yaml.load(reader);

            if (!(data instanceof Map)) {
                throw new IOException("invalid progress.yml file");
            }

            progressesFromYaml(progressTracker, (Map<String, ?>) data);
        }
    }

    @Override
    public void saveProgress(ProgressManager progressTracker) throws IOException
    {
        // write progress to <file>.new
        File newFile = new File(file.getPath() + ".new");
        Yaml yaml = new Yaml();
        try(FileWriter writer = new FileWriter(newFile))
        {
            Map<String, ?> data = progressesToYaml(progressTracker);

            yaml.dump(data, writer);
        }

        // backup old file
        if (file.exists())
        {
            File bakFile = new File(file.getPath() + ".bak");
            Files.move(file, bakFile);
        }
        // move new file
        Files.move(newFile, file);
    }

    private void progressesFromYaml(ProgressManager progressManager, Map<String, ?> map) throws IOException
    {
        Object tmp;

        if ((tmp = map.get("current")) != null && tmp instanceof Map) {
            currentProgressesFromYaml(progressManager, (Map<String, ?>) tmp);
        }

        if ((tmp = map.get("finished")) != null && tmp instanceof Map) {
            finishedQuestsFromYaml(progressManager, (Map<String, ?>) tmp);
        }

        if ((tmp = map.get("tracked")) != null && tmp instanceof Map) {
            trackedQuestsFromYaml(progressManager, (Map<String, ?>) tmp);
        }
    }

    private void currentProgressesFromYaml(ProgressManager progressManager, Map<String, ?> map) throws IOException
    {
        for (Map.Entry<String, ?> entry : map.entrySet())
        {
            UUID playerid = null;
            Object val = entry.getValue();
            try {
                playerid = UUID.fromString(entry.getKey());
            } catch(IllegalArgumentException ex) {
                throw new IOException("could not load progress, invalid player uuid");
            }
            if (!(val instanceof Map)) {
                throw new IOException("could not load progress, invalid player progress");
            }

            Map<String, ?> playerMap = (Map<String, ?>) val;

            for (Map.Entry<String, ?> questEntry : playerMap.entrySet())
            {
                UUID questid = null;
                Object questVal = questEntry.getValue();
                try {
                    questid = UUID.fromString(questEntry.getKey());
                } catch(IllegalArgumentException ex) {
                    throw new IOException("could not load progress, invalid quest uuid");
                }
                if (!(questVal instanceof Map)) {
                    throw new IOException("could not load progress, invalid quest progress");
                }
                QuestProgress progress = progressFromYaml(questid, (Map<String, ?>) questVal);

                if (progress == null) {
                    continue;
                }

                progressManager.setQuestProgress(playerid, progress);
            }
        }
    }

    private QuestProgress progressFromYaml(UUID uuid, Map<String, ?> map) throws IOException
    {
        Quest quest = questRegistry.findQuest(uuid);
        if (quest == null) {
            return null;
        }

        QuestProgress questProgress = null;
        Object tmp;
        long time;

        if ((tmp = map.get("time")) != null && tmp instanceof Number) {
            time = ((Number) tmp).longValue();
        } else {
            time = System.currentTimeMillis();
        }

        if (map.containsKey("current-stage") && ((tmp = map.get("current-stage")) instanceof String))
        {
            try {
                UUID stageid = UUID.fromString((String) tmp);
                Stage stage = quest.findStage(stageid);

                if (stage != null)
                {
                    questProgress = new QuestProgress(quest, stage, new Date(time));

                    if (map.containsKey("objectives") && ((tmp = map.get("objectives")) instanceof Map))
                    {
                        objectivesFromYaml(questProgress, stage, (Map<String, ?>) tmp);
                    }
                }
            } catch(IllegalArgumentException ex) {
                throw new IOException("could not load stage, invalid uuid", ex);
            }
        }

        return questProgress;
    }

    private void objectivesFromYaml(QuestProgress questProgress, Stage stage, Map<String, ?> map) throws IOException
    {
        for (Map.Entry<String, ?> entry : map.entrySet())
        {
            UUID uuid;
            try {
                uuid = UUID.fromString(entry.getKey());
            } catch(IllegalArgumentException ex) {
                throw new IOException("could not load objectives progress, invalid uuid", ex);
            }
            Objective objective = stage.findObjective(uuid);

            if (objective == null) {
                continue;
            }

            ObjectiveFactory factory = factoryRegistry.findObjectiveFactory(objective.getFactoryId());
            if (factory == null) {
                continue; // TODO: error?
            }
            Object fromYaml = factory.progressFromYaml(entry.getValue());
            if (fromYaml != null) {
                questProgress.setObjectiveProgress(objective, fromYaml);
            }
        }
    }

    private void finishedQuestsFromYaml(ProgressManager progressTracker, Map<String, ?> map) throws IOException
    {

        for (Map.Entry<String, ?> entry : map.entrySet())
        {
            UUID playerid = null;
            Object val = entry.getValue();
            try {
                playerid = UUID.fromString(entry.getKey());
            } catch(IllegalArgumentException ex) {
                throw new IOException("could not load finished quest, invalid player uuid");
            }
            if (!(val instanceof Map)) {
                throw new IOException("could not load finished quest, invalid quests");
            }

            Map<String, ?> playerMap = (Map<String, ?>) val;

            for (Map.Entry<String, ?> questEntry : playerMap.entrySet())
            {
                UUID questid = null;
                Object finishedVal = questEntry.getValue();
                try {
                    questid = UUID.fromString(questEntry.getKey());
                } catch(IllegalArgumentException ex) {
                    throw new IOException("could not load finished quest, invalid quest uuid");
                }
                if (!(finishedVal instanceof Map)) {
                    throw new IOException("could not load finished quest, invalid finished quest info");
                }
                FinishedQuestInfo info = finishedQuestFromYaml(questid, (Map<String, ?>) finishedVal);

                if (info == null) {
                    continue;
                }

                progressTracker.setFinishedQuestInfo(playerid, info);
            }
        }
    }

    private FinishedQuestInfo finishedQuestFromYaml(UUID questid, Map<String, ?> map)
    {
        Quest quest = questRegistry.findQuest(questid);
        if (quest == null) {
            return null;
        }

        Object tmp;

        Date time = new Date();
        int numSuccess = 0;
        int numFailed = 0;
        boolean lastFailed = false;

        if ((tmp = map.get("time")) != null && tmp instanceof Number) {
            time = new Date(((Number) tmp).intValue());
        }

        if ((tmp = map.get("num-success")) != null && tmp instanceof Number) {
            numSuccess = ((Number) tmp).intValue();
        }

        if ((tmp = map.get("num-failed")) != null && tmp instanceof Number) {
            numFailed = ((Number) tmp).intValue();
        }

        if ((tmp = map.get("last-failed")) != null && tmp instanceof Boolean
                && (Boolean) tmp) {
            lastFailed = true;
        }

        return new FinishedQuestInfo(quest, lastFailed, time, numFailed, numSuccess);
    }

    private void trackedQuestsFromYaml(ProgressManager progressManager, Map<String, ?> map) throws IOException
    {
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            UUID playerid = null;
            Object val = entry.getValue();
            try {
                playerid = UUID.fromString(entry.getKey());
            } catch (IllegalArgumentException ex) {
                throw new IOException("could not load tracked quest, invalid player uuid");
            }

            UUID questid = null;
            try {
                questid = UUID.fromString(String.valueOf(val));
            } catch(IllegalArgumentException ex) {
                throw new IOException("could not load tracked quest, invalid quest uuid");
            }
            Quest trackedQuest = questRegistry.findQuest(questid);
            if (trackedQuest == null) {
                BetterQuestsPlugin.getInstance().getLogger().log(Level.WARNING, "could not set tracked quest of player {0}: quest with uuid {1} not found",
                        new Object[] {});
            }
            progressManager.getPlayerProgress(playerid).setTrackedQuest(trackedQuest);
        }
    }

    private Map<String, ?> progressesToYaml(ProgressManager progressManager) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("current", currentProgressesToYaml(progressManager));
        map.put("finished", finishedProgressesToYaml(progressManager));
        map.put("tracked", trackedQuestsToYaml(progressManager));

        return map;
    }

    private Map<String, ?> currentProgressesToYaml(ProgressManager progressManager) throws IOException
    {
        HashMap<String, HashMap<String, Object>> map = new HashMap<>();

        Map<UUID, PlayerProgress> playerProgresses = progressManager.getPlayerProgresses();

        for (Map.Entry<UUID, PlayerProgress> entry : playerProgresses.entrySet())
        {
            UUID playerid = entry.getKey();
            for (Map.Entry<UUID, QuestProgress> entry2 : entry.getValue().getQuestProgresses().entrySet())
            {
                UUID questid = entry2.getKey();

                HashMap<String, Object> playerProgress = map.get(playerid.toString());
                if (playerProgress == null)
                {
                    playerProgress = new HashMap<>();
                    map.put(playerid.toString(), playerProgress);
                }

                Map<String, Object> progressYaml = progressToYaml(entry2.getValue());
                if (progressYaml != null) {
                    playerProgress.put(questid.toString(), progressYaml);
                }
            }
        }

        return map;
    }

    private Map<String, Object> progressToYaml(QuestProgress progress)
    {
        HashMap<String, Object> map = new HashMap<>();

        Stage currentStage = progress.getCurrentStage();
        if (currentStage == null) {
            return null;
        }

        map.put("current-stage", currentStage.getUuid().toString());
        map.put("objectives", objectivesToYaml(currentStage, progress.getObjectivesProgress()));
        map.put("time", progress.getTimeStarted().getTime());

        return map;
    }

    private Map<String, Object> objectivesToYaml(Stage stage, Map<UUID, Object> progresses)
    {
        HashMap<String, Object> map = new HashMap<>();

        for (Map.Entry<UUID, Object> progress : progresses.entrySet())
        {
            UUID uuid = progress.getKey();
            Objective objective = stage.findObjective(uuid);
            if (objective == null) {
                continue;
            }
            ObjectiveFactory factory = factoryRegistry.findObjectiveFactory(objective.getFactoryId());
            if (factory == null) {
                continue; // TODO: error?
            }
            Object toYaml = factory.progressToYaml(progress.getValue());
            if (toYaml != null) {
                map.put(uuid.toString(), toYaml);
            }
        }

        return map;
    }

    private Map<String, ?> finishedProgressesToYaml(ProgressManager progressManager) throws IOException
    {
        HashMap<String, HashMap<String, Object>> map = new HashMap<>();

        Map<UUID, PlayerProgress> playerProgresses = progressManager.getPlayerProgresses();

        for (Map.Entry<UUID, PlayerProgress> entry : playerProgresses.entrySet())
        {
            UUID playerid = entry.getKey();
            for (Map.Entry<UUID, FinishedQuestInfo> entry2 : entry.getValue().getFinishedQuests().entrySet())
            {
                UUID questid = entry2.getKey();

                HashMap<String, Object> playerFinishedQuests = map.get(playerid.toString());
                if (playerFinishedQuests == null)
                {
                    playerFinishedQuests = new HashMap<>();
                    map.put(playerid.toString(), playerFinishedQuests);
                }

                Map<String, Object> finishedQuestYaml = finishedQuestToYaml(entry2.getValue());
                if (finishedQuestYaml != null) {
                    playerFinishedQuests.put(questid.toString(), finishedQuestYaml);
                }
            }
        }

        return map;
    }

    private Map<String, Object> finishedQuestToYaml(FinishedQuestInfo value)
    {
        HashMap<String, Object> map = new HashMap<>();

        map.put("time", value.getTime().getTime());
        map.put("num-success", value.getNumSuccess());
        map.put("num-failed", value.getNumFailed());
        map.put("last-failed", value.isLastFailed());

        return map;
    }

    private Map<String, Object> trackedQuestsToYaml(ProgressManager progressManager)
    {
        HashMap<String, Object> map = new HashMap<>();

        Map<UUID, PlayerProgress> playerProgresses = progressManager.getPlayerProgresses();

        for (Map.Entry<UUID, PlayerProgress> entry : playerProgresses.entrySet())
        {
            UUID playerid = entry.getKey();
            Quest trackedQuest = entry.getValue().getTrackedQuest();

            if (trackedQuest != null) {
                map.put(playerid.toString(), trackedQuest.getUuid().toString());
            }
        }

        return map;
    }
}
