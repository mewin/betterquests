/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import java.util.List;

/**
 *
 * @author mewin
 */
public interface RewardsHolder
{
    public void addReward(Reward reward);
    public void removeReward(Reward reward);
    public List<Reward> getRewards();
    public Stage getStage();
}
