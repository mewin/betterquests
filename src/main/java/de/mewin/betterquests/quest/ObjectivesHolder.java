/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import java.util.List;

/**
 *
 * @author mewin
 */
public interface ObjectivesHolder
{
    public void addObjective(Objective objective);
    public void removeObjective(Objective objective);
    public List<Objective> getObjectives();
    public Stage getStage();
}
