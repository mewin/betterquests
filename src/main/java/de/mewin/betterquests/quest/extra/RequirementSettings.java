/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.extra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mewin
 */
public class RequirementSettings
{
    public static final String SILENT = "silent";

    private static final ArrayList<KnownSetting> KNOWN_SETTINGS = new ArrayList<>();

    public static KnownSetting[] getKnownSettings()
    {
        return KNOWN_SETTINGS.toArray(new KnownSetting[0]);
    }

    public static List<KnownSetting> getKnownSettingsList()
    {
        return Collections.unmodifiableList(KNOWN_SETTINGS);
    }

    public static void addKnownSetting(KnownSetting setting)
    {
        KNOWN_SETTINGS.add(setting);
    }

    static
    {
        KNOWN_SETTINGS.add(new KnownSetting(SILENT, DataType.BOOLEAN));
    }
}
