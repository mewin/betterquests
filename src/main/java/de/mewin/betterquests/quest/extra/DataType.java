/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.extra;

/**
 *
 * @author mewin
 */
public enum DataType
{
    STRING,
    INTEGER,
    DOUBLE,
    BOOLEAN,
    LONG_STRING
}
