/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.extra;

/**
 *
 * @author mewin
 */
public class KnownSetting
{
    public String name;
    public DataType type;

    public KnownSetting(String name, DataType type)
    {
        this.name = name;
        this.type = type;
    }
}
