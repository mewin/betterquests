/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.extra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mewin
 */
public class StageSettings
{
    public static final String START_MESSAGE = "start-message";
    public static final String FINISH_MESSAGE = "finish-message";
    public static final String FAIL_MESSAGE = "fail-message";
    public static final String OBJECTIVES_TEXT = "objectives-text";

    private static final ArrayList<KnownSetting> KNOWN_SETTINGS = new ArrayList<>();

    public static KnownSetting[] getKnownSettings()
    {
        return KNOWN_SETTINGS.toArray(new KnownSetting[0]);
    }

    public static List<KnownSetting> getKnownSettingsList()
    {
        return Collections.unmodifiableList(KNOWN_SETTINGS);
    }

    public static void addKnownSetting(KnownSetting setting)
    {
        KNOWN_SETTINGS.add(setting);
    }

    static
    {
        KNOWN_SETTINGS.add(new KnownSetting(START_MESSAGE, DataType.LONG_STRING));
        KNOWN_SETTINGS.add(new KnownSetting(FINISH_MESSAGE, DataType.LONG_STRING));
        KNOWN_SETTINGS.add(new KnownSetting(FAIL_MESSAGE, DataType.LONG_STRING));
        KNOWN_SETTINGS.add(new KnownSetting(OBJECTIVES_TEXT, DataType.LONG_STRING));
    }
}
