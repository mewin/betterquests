/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.extra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mewin
 */
public class QuestSettings
{
    public static final String PRINT_DESCRIPTION = "print-description";
    public static final String START_MESSAGE = "start-message";
    public static final String FAIL_MESSAGE = "fail-message";
    public static final String REQUIREMENT_MISSING_MESSAGE = "requirement-missing-message";
    public static final String REDO_DELAY = "redo-delay";
    public static final String RETRY_DELAY = "retry-delay";
    public static final String FINISH_MESSAGE = "finish-message";

    private static final ArrayList<KnownSetting> KNOWN_SETTINGS = new ArrayList<>();

    public static KnownSetting[] getKnownSettings()
    {
        return KNOWN_SETTINGS.toArray(new KnownSetting[0]);
    }

    public static List<KnownSetting> getKnownSettingsList()
    {
        return Collections.unmodifiableList(KNOWN_SETTINGS);
    }

    public static void addKnownSetting(KnownSetting setting)
    {
        KNOWN_SETTINGS.add(setting);
    }

    static
    {
        KNOWN_SETTINGS.add(new KnownSetting(PRINT_DESCRIPTION, DataType.BOOLEAN));
        KNOWN_SETTINGS.add(new KnownSetting(START_MESSAGE, DataType.LONG_STRING));
        KNOWN_SETTINGS.add(new KnownSetting(FAIL_MESSAGE, DataType.LONG_STRING));
        KNOWN_SETTINGS.add(new KnownSetting(REQUIREMENT_MISSING_MESSAGE, DataType.LONG_STRING));
        KNOWN_SETTINGS.add(new KnownSetting(REDO_DELAY, DataType.INTEGER));
        KNOWN_SETTINGS.add(new KnownSetting(RETRY_DELAY, DataType.INTEGER));
        KNOWN_SETTINGS.add(new KnownSetting(FINISH_MESSAGE, DataType.LONG_STRING));
    }
}
