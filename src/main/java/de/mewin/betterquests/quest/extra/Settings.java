/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.extra;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mewin
 */
public class Settings
{
    private final HashMap<String, Object> values;

    public Settings()
    {
        values = new HashMap<>();
    }

    public Map<String, Object> getValues()
    {
        return Collections.unmodifiableMap(values);
    }

    public void setValues(Map<String, Object> values)
    {
        this.values.clear();
        this.values.putAll(values);
    }

    public String getString(String key, String def)
    {
        if (values.containsKey(key)) {
            return values.get(key).toString();
        }
        return def;
    }

    public String getString(String key)
    {
        return getString(key, "");
    }

    public void setString(String key, String value)
    {
        values.put(key, value);
    }

    private Number getNumber(String key, Number def)
    {
        Object tmp;
        if (values.containsKey(key) && ((tmp = values.get(key)) instanceof Number)) {
            return (Number) tmp;
        }
        return def;
    }

    public int getInt(String key, int def)
    {
        return getNumber(key, def).intValue();
    }

    public int getInt(String key)
    {
        return getInt(key, 0);
    }

    public void setInt(String key, int value)
    {
        values.put(key, value);
    }

    public double getDouble(String key, double def)
    {
        return getNumber(key, def).doubleValue();
    }

    public double getDouble(String key)
    {
        return getDouble(key, Double.NaN);
    }

    public void setDouble(String key, double value)
    {
        values.put(key, value);
    }

    public boolean getBoolean(String key, boolean def)
    {
        Object tmp;
        if (values.containsKey(key) && ((tmp = values.get(key)) instanceof Boolean)) {
            return ((Boolean) tmp);
        }
        return def;
    }

    public boolean getBoolean(String key)
    {
        return getBoolean(key, false);
    }

    public void setBoolean(String key, boolean value)
    {
        values.put(key, value);
    }

    public void remove(String key)
    {
        values.remove(key);
    }

    public boolean contains(String key)
    {
        return values.containsKey(key);
    }
}
