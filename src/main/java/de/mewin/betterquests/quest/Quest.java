/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.quest.extra.Settings;
import de.mewin.betterquests.quest.event.TriggerAddedEvent;
import de.mewin.betterquests.quest.event.TriggerRemovedEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;

/**
 *
 * @author mewin
 */
public class Quest implements RequirementsHolder, TriggersHolder
{
    private final ArrayList<Requirement> requirements;
    private final ArrayList<Stage> stages;
    private final ArrayList<Trigger> triggers;
    private String id;
    private String title;
    private String description;
    private UUID uuid;
    private final Settings settings;

    public Quest(String id)
    {
        this.id = id;
        title = id;
        description = "";
        requirements = new ArrayList<>();
        stages = new ArrayList<>();
        triggers = new ArrayList<>();
        uuid = UUID.randomUUID();
        settings = new Settings();
    }

    public Quest(String id, UUID uuid)
    {
        this.uuid = uuid;
        this.id = id;
        title = id;
        description = "";
        requirements = new ArrayList<>();
        stages = new ArrayList<>();
        triggers = new ArrayList<>();
        settings = new Settings();
    }

    public void setUuid(UUID uuid)
    {
        this.uuid = uuid;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Settings getSettings()
    {
        return settings;
    }

    @Override
    public void addRequirement(Requirement requirement)
    {
        requirements.add(requirement);
    }

    public void removeRequirement(int index)
    {
        requirements.get(index).onRemove();
        requirements.remove(index);
    }

    @Override
    public void removeRequirement(Requirement requirement)
    {
        if (requirements.remove(requirement))
        {
            requirement.onRemove();
        }
    }

    @Override
    public List<Requirement> getRequirements()
    {
        return Collections.unmodifiableList(requirements);
    }

    public void appendStage(Stage stage)
    {
        stages.add(stage);
    }

    public void insertStage(int index, Stage stage)
    {
        stages.add(index, stage);
    }

    public Stage findStage(UUID uuid)
    {
        for (Stage stage : stages)
        {
            if (stage.getUuid().equals(uuid)) {
                return stage;
            }
        }

        return null;
    }

    public void removeStage(int index)
    {
        stages.get(index).onRemove();
        stages.remove(index);
    }

    public void removeStage(Stage stage)
    {
        if (stages.remove(stage))
        {
            stage.onRemove();
        }
    }

    public List<Stage> getStages()
    {
        return Collections.unmodifiableList(stages);
    }

    public Stage getNextStage(Stage stage)
    {
        if (stage == null) {
            return stages.isEmpty() ? null : stages.get(0);
        }
        int index = stages.indexOf(stage);
        if (index < 0 || index >= stages.size() - 1) {
            return null;
        }
        return stages.get(index + 1);
    }

    @Override
    public void addTrigger(Trigger trigger)
    {
        trigger.setQuest(this);
        triggers.add(trigger);

        Bukkit.getPluginManager().callEvent(new TriggerAddedEvent(this, trigger));
    }

    public void removeTrigger(int index)
    {
        Trigger trigger = triggers.get(index);
        removeTrigger(trigger);
    }

    @Override
    public void removeTrigger(Trigger trigger)
    {
        if (triggers.remove(trigger))
        {
            trigger.onRemove();
            Bukkit.getPluginManager().callEvent(new TriggerRemovedEvent(this, trigger));
        }
    }

    @Override
    public List<Trigger> getTriggers()
    {
        return Collections.unmodifiableList(triggers);
    }

    public boolean isQuestReady()
    {
        return !stages.isEmpty();
    }

    public void onRemove()
    {
        for (Requirement requirement : requirements)
        {
            requirement.onRemove();
        }

        for (Trigger trigger : triggers)
        {
            trigger.onRemove();
            Bukkit.getPluginManager().callEvent(new TriggerRemovedEvent(this, trigger));
        }

        for (Stage stage : stages)
        {
            stage.onRemove();
        }
    }

    @Override
    public Quest getQuest()
    {
        return this;
    }

    public Quest copy(String newId)
    {
        Quest copy = new Quest(newId);

        copy.title = title;
        copy.description = description;
        copy.settings.setValues(settings.getValues());

        for (Stage stage : stages) {
            copy.stages.add(stage.copy(copy));
        }

        for (Requirement requirement : requirements) {
            copy.requirements.add(requirement.copy(copy));
        }

        for (Trigger trigger : triggers) {
            copy.triggers.add(trigger.copy(copy));
        }

        return copy;
    }

    public static boolean isValidQuestId(String id)
    {
        if ("".equals(id)) {
            return false;
        }
        if (id.contains(" ")) {
            return false;
        }
        return true;
    }

    public static class NameComparator implements Comparator<Quest>
    {
        @Override
        public int compare(Quest quest1, Quest quest2)
        {
            return quest1.id.compareTo(quest2.id);
        }
    }
}
