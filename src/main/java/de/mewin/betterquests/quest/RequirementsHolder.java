/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import java.util.List;

/**
 *
 * @author mewin
 */
public interface RequirementsHolder
{
    public void addRequirement(Requirement requirement);
    public void removeRequirement(Requirement requirement);
    public List<Requirement> getRequirements();
    public Quest getQuest();
}
