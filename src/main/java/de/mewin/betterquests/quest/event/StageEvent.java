/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;

/**
 *
 * @author mewin
 */
public abstract class StageEvent extends QuestEvent
{
    private final Stage stage;

    public StageEvent(Quest quest, Stage stage)
    {
        super(quest);
        this.stage = stage;
    }

    public Stage getStage()
    {
        return stage;
    }
}
