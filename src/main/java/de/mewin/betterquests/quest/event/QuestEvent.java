/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.quest.Quest;
import org.bukkit.event.Event;

/**
 *
 * @author mewin
 */
public abstract class QuestEvent extends Event
{
    private final Quest quest;

    public QuestEvent(Quest quest)
    {
        this.quest = quest;
    }

    public Quest getQuest()
    {
        return quest;
    }
}
