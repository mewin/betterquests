/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;

/**
 *
 * @author mewin
 */
public abstract class TriggerEvent extends QuestEvent
{
    private final Trigger trigger;

    public TriggerEvent(Quest quest, Trigger trigger)
    {
        super(quest);
        this.trigger = trigger;
    }

    public Trigger getTrigger()
    {
        return trigger;
    }
}
