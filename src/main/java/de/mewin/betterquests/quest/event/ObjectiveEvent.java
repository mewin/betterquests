/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;

/**
 *
 * @author mewin
 */
public abstract class ObjectiveEvent extends StageEvent
{
    private final Objective objective;

    public ObjectiveEvent(Quest quest, Stage stage, Objective trigger)
    {
        super(quest, stage);
        this.objective = trigger;
    }

    public Objective getObjective()
    {
        return objective;
    }
}
