/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Stage;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class StageChangeEvent extends QuestProgressEvent
{
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final Stage oldStage;
    private final Stage newStage;

    public StageChangeEvent(Player player, QuestProgress progress, Stage oldStage, Stage newStage)
    {
        super(player, progress);
        this.oldStage = oldStage;
        this.newStage = newStage;
    }

    public Stage getOldStage()
    {
        return oldStage;
    }

    public Stage getNewStage()
    {
        return newStage;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }
}
