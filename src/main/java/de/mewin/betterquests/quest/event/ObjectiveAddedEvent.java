/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class ObjectiveAddedEvent extends ObjectiveEvent
{
    public ObjectiveAddedEvent(Quest quest, Stage stage, Objective objective)
    {
        super(quest, stage, objective);
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}