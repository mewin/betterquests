/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.progress.QuestProgress;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public abstract class QuestProgressEvent extends QuestEvent
{
    private final Player player;
    private final QuestProgress progress;

    public QuestProgressEvent(Player player, QuestProgress progress)
    {
        super(progress.getQuest());
        this.player = player;
        this.progress = progress;
    }

    public Player getPlayer()
    {
        return player;
    }

    public QuestProgress getProgress()
    {
        return progress;
    }
}
