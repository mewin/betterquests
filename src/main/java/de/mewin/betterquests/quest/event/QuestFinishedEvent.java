/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.progress.QuestProgress;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class QuestFinishedEvent extends QuestProgressEvent
{
    private boolean silenced = false;

    public QuestFinishedEvent(Player player, QuestProgress progress)
    {
        super(player, progress);
    }

    public boolean isSilenced()
    {
        return silenced;
    }

    public void setSilenced(boolean silenced)
    {
        this.silenced = silenced;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}
