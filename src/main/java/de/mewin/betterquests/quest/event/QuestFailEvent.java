/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.progress.QuestProgress;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class QuestFailEvent extends QuestProgressEvent implements Cancellable
{
    private boolean cancelled = false;
    private boolean silenced = false;

    public QuestFailEvent(Player player, QuestProgress progress)
    {
        super(player, progress);
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    public static final HandlerList HANDLER_LIST = new HandlerList();

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel)
    {
        cancelled = cancel;
    }

    public boolean isSilenced()
    {
        return silenced;
    }

    public void setSilenced(boolean silenced)
    {
        this.silenced = silenced;
    }
}
