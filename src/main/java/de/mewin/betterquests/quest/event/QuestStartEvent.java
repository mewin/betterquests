/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.progress.QuestProgress;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class QuestStartEvent extends QuestProgressEvent implements Cancellable
{
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private boolean cancelled;

    public QuestStartEvent(Player player, QuestProgress progress)
    {
        super(player, progress);
        this.cancelled = false;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel)
    {
        cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }
}
