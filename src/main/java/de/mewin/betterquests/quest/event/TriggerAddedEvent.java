/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class TriggerAddedEvent extends TriggerEvent
{
    private static final HandlerList HANDLER_LIST = new HandlerList();

    public TriggerAddedEvent(Quest quest, Trigger trigger)
    {
        super(quest, trigger);
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }
}
