/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.event;

import de.mewin.betterquests.progress.QuestProgress;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class QuestUpdateEvent extends QuestProgressEvent
{
    public QuestUpdateEvent(Player player, QuestProgress progress)
    {
        super(player, progress);
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}
