/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.quest.event.ObjectiveAddedEvent;
import de.mewin.betterquests.quest.event.ObjectiveRemovedEvent;
import de.mewin.betterquests.quest.extra.Settings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;

/**
 *
 * @author mewin
 */
public class Stage implements ObjectivesHolder, RewardsHolder
{
    private final Quest quest;
    private final Settings settings;
    private final ArrayList<Objective> objectives;
    private final ArrayList<Reward> rewards;
    private UUID uuid;

    public Stage(Quest quest)
    {
        this.quest = quest;
        settings = new Settings();
        objectives = new ArrayList<>();
        rewards = new ArrayList<>();
        uuid = UUID.randomUUID();
    }

    public Stage(Quest quest, UUID uuid)
    {
        this.quest = quest;
        this.uuid = uuid;
        settings = new Settings();
        objectives = new ArrayList<>();
        rewards = new ArrayList<>();
    }

    public Quest getQuest()
    {
        return quest;
    }

    public void setUuid(UUID uuid)
    {
        this.uuid = uuid;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public Settings getSettings()
    {
        return settings;
    }

    public int getMaxUpdateInterval()
    {
        int interval = Integer.MAX_VALUE;
        for (Objective objective : objectives) {
            interval = Math.min(interval, objective.getMaxUpdateInterval());
        }
        return interval;
    }

    @Override
    public void addObjective(Objective objective)
    {
        objectives.add(objective);
        Bukkit.getPluginManager().callEvent(new ObjectiveAddedEvent(quest, this, objective));
    }

    public Objective findObjective(UUID uuid)
    {
        for (Objective objective : objectives)
        {
            if (objective.getUuid().equals(uuid)) {
                return objective;
            }
        }

        return null;
    }

    public void removeObjective(int index)
    {
        Objective objective = objectives.get(index);
        removeObjective(objective);
    }

    @Override
    public void removeObjective(Objective objective)
    {
        if (objectives.remove(objective))
        {
            objective.onRemove();
            Bukkit.getPluginManager().callEvent(new ObjectiveRemovedEvent(quest, this, objective));
        }
    }

    @Override
    public List<Objective> getObjectives()
    {
        return Collections.unmodifiableList(objectives);
    }

    @Override
    public void addReward(Reward reward)
    {
        rewards.add(reward);
    }

    public void removeReward(int index)
    {
        rewards.get(index).onRemove();
        rewards.remove(index);
    }

    @Override
    public void removeReward(Reward reward)
    {
        if (rewards.remove(reward)) {
            reward.onRemove();
        }
    }

    @Override
    public List<Reward> getRewards()
    {
        return Collections.unmodifiableList(rewards);
    }

    @Override
    public Stage getStage()
    {
        return this;
    }

    public Stage copy(Quest quest)
    {
        Stage copy = new Stage(quest);

        copy.settings.setValues(settings.getValues());

        for (Objective objective : objectives) {
            copy.objectives.add(objective.copy(copy));
        }

        for (Reward reward : rewards) {
            copy.rewards.add(reward.copy(copy));
        }

        return copy;
    }

    public void onRemove()
    {
        for (Objective objective : objectives)
        {
            objective.onRemove();
            Bukkit.getPluginManager().callEvent(new ObjectiveRemovedEvent(quest, this, objective));
        }

        for (Reward reward : rewards)
        {
            reward.onRemove();
        }
    }
}
