/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.chat.DefaultQuestFilter;
import de.mewin.betterquests.io.QuestRegistryBackend;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import de.mewin.betterquests.chat.QuestFilter;

/**
 *
 * @author mewin
 */
public class QuestRegistry
{
    private final HashMap<String, Quest> quests;
    private ArrayList<Quest> sortedQuests;
    private final QuestRegistryBackend backend;

    public QuestRegistry(QuestRegistryBackend backend)
    {
        this.quests = new HashMap<>();
        this.sortedQuests = null;
        this.backend = backend;
    }

    public Quest copyQuest(Quest quest, String newId) throws DuplicateQuestException
    {
        Quest copy = quest.copy(newId);
        registerQuest(copy);
        return copy;
    }

    public Quest findQuest(String id, boolean asIndex, QuestFilter filter)
    {
        if (asIndex)
        {
            try
            {
                int index = Integer.valueOf(id) - 1;

                List<Quest> sQuests = getSortedQuests();

                if (filter.isFiltering())
                {
                    int i = 0;
                    for (Quest quest : sQuests)
                    {
                        if (!filter.showQuest(quest)) {
                            continue;
                        }
                        if (i == index) {
                            return quest;
                        }

                        ++i;
                    }
                }
                if (index >= 0 && index < quests.size())
                {
                    return sQuests.get(index);
                }
                else
                {
                    return null;
                }
            }
            catch(NumberFormatException ex)
            {

            }
        }
        return quests.get(id);
    }

    public Quest findQuest(String id, boolean asIndex)
    {
        return findQuest(id, asIndex, new DefaultQuestFilter());
    }

    public Quest findQuest(String id, QuestFilter filter)
    {
//        Quest q = findQuest(id, true, filter);
//        if (q != null) {
//        }
        return findQuest(id, true, filter);
    }

    public Quest findQuest(String id)
    {
        return findQuest(id, new DefaultQuestFilter());
    }

    public Quest findQuest(UUID uuid)
    {
        for (Quest quest : quests.values())
        {
            if (quest.getUuid().equals(uuid)) {
                return quest;
            }
        }

        return null;
    }

    public void registerQuest(Quest quest) throws DuplicateQuestException
    {
        String id = quest.getId();
        if (quests.containsKey(id)) {
            throw new DuplicateQuestException(id, quests.get(id));
        }

        quests.put(id, quest);

        sortedQuests = null;
    }

    public void removeQuest(String id)
    {
        Quest quest = quests.remove(id);
        if (quest != null)
        {
            quest.onRemove();
            if (sortedQuests != null) {
                sortedQuests.remove(quest);
            }
        }

        sortedQuests = null;
    }

    public void removeQuest(Quest quest)
    {
        removeQuest(quest.getId());
    }

    public void removeQuest(int index)
    {
        Quest quest = getSortedQuests().get(index);

        removeQuest(quest);
    }

    public void renameQuest(Quest quest, String questId) throws DuplicateQuestException
    {
        if (quest.getId().equals(questId)) {
            return;
        }

        if (quests.containsKey(questId)) {
            throw new DuplicateQuestException(questId, quests.get(questId));
        }

        quests.remove(quest.getId());
        quest.setId(questId);
        quests.put(questId, quest);

        sortedQuests = null;
    }

    public Map<String, Quest> getQuests()
    {
        return Collections.unmodifiableMap(quests);
    }

    public void loadQuests() throws IOException
    {
        backend.loadQuests(this);
    }

    public void saveQuests() throws IOException
    {
        backend.saveQuests(this);
    }

    public List<Quest> getSortedQuests()
    {
        if (sortedQuests == null)
        {
            sortedQuests = new ArrayList<>(quests.values());
            Collections.sort(sortedQuests, new Quest.NameComparator());
        }

        return Collections.unmodifiableList(sortedQuests);
    }

    public static class DuplicateQuestException extends Exception
    {
        private Quest quest;

        public DuplicateQuestException(String id, Quest quest)
        {
            super("duplicate quest id: " + id);
            this.quest = quest;
        }

        public Quest getQuest()
        {
            return quest;
        }
    }
}
