/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import java.util.List;

/**
 *
 * @author mewin
 */
public interface TriggersHolder
{
    public void addTrigger(Trigger trigger);
    public void removeTrigger(Trigger trigger);
    public List<Trigger> getTriggers();
    public Quest getQuest();
}
