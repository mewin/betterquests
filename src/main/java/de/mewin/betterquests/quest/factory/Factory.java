/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.factory;

import de.mewin.betterquests.quest.Customizable;
import java.io.IOException;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author mewin
 */
public abstract class Factory<T, S extends Customizable<T>>
{
    public abstract S createNew(T base);
    public abstract String getName(CommandSender sender);
    public abstract S fromYaml(T base, Map<String, Object> yaml) throws IOException;
    public abstract Map<String, Object> toYaml(S object) throws IOException;

    public S fromString(T base, String string) throws IOException
    {
        Yaml yaml = new Yaml();
        Object data = yaml.load(string);
        if (!(data instanceof Map)) {
            throw new IOException("invalid yaml string");
        }
        return fromYaml(base, (Map<String, Object>) data);
    }

    public String makeString(S object) throws IOException
    {
        Yaml yaml = new Yaml();
        Object data = toYaml(object);
        return yaml.dump(data);
    }
}
