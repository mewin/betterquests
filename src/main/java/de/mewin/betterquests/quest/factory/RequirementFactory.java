/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.factory;

import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;

/**
 *
 * @author mewin
 */
public abstract class RequirementFactory extends Factory<RequirementsHolder, Requirement>
{

}
