/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.factory;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;

/**
 *
 * @author mewin
 */
public abstract class TriggerFactory extends Factory<Quest, Trigger>
{

}
