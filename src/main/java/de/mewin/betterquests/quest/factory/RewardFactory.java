/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.factory;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;

/**
 *
 * @author mewin
 */
public abstract class RewardFactory extends Factory<RewardsHolder, Reward>
{

}
