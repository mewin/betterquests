/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest.factory;

import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author mewin
 */
public abstract class ObjectiveFactory extends Factory<ObjectivesHolder, Objective>
{
    public Object progressToYaml(Object data)
    {
        return Serializer.makeYaml(data);
    }

    public Object progressFromYaml(Object yaml)
    {
        return yaml;
    }

    public String progressToString(Object progressObj)
    {
        Object asYaml = progressToYaml(progressObj);
        if (asYaml == null) {
            return "";
        }

        Yaml yaml = new Yaml();
        return yaml.dump(asYaml);
    }

    public Object progressFromString(String asString)
    {
        if ("".equals(asString)) {
            return null;
        }
        Yaml yaml = new Yaml();
        return progressFromYaml(yaml.load(asString));
    }
}
