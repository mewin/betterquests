/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.conditions.AdvertisementCondition;
import de.mewin.betterquests.quest.extra.Settings;
import de.mewin.betterquests.quest.factory.TriggerFactory;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Location;

/**
 *
 * @author mewin
 */
public abstract class Trigger implements Customizable<Quest>
{
    private Quest quest;
    private final Settings settings;

    public Trigger()
    {
        this.settings = new Settings();
    }

    public void onRemove()
    {

    }

    public void setQuest(Quest quest)
    {
        if (this.quest != null) {
            throw new IllegalStateException("quest already set");
        }
        this.quest = quest;
    }

    public Quest getQuest()
    {
        return quest;
    }

    @Override
    public Settings getSettings()
    {
        return settings;
    }

    public List<AdvertisementCondition> getConditions()
    {
        return Collections.EMPTY_LIST;
    }

    public Location getLocation()
    {
        return null;
    }

    public Trigger copy(Quest quest)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        TriggerFactory factory = plugin
                .getFactoryRegistry()
                .findTriggerFactory(getFactoryId());

        if (factory == null)
        {
            plugin.getLogger().log(Level.SEVERE, "missing factory while copying trigger!");
            return null; // TODO: return dummy (?)
        }

        try
        {
            Trigger copy = factory.fromString(quest, factory.makeString(this));
            copy.settings.setValues(settings.getValues());
            return copy;
        }
        catch(IOException ex)
        {
            plugin.getLogger().log(Level.SEVERE, "could not clone trigger: ", ex);
            throw new RuntimeException("could not clone trigger", ex);
        }
    }
}
