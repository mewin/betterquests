/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.quest.factory.Factory;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;
import de.mewin.betterquests.quest.factory.RequirementFactory;
import de.mewin.betterquests.quest.factory.RewardFactory;
import de.mewin.betterquests.quest.factory.TriggerFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mewin
 */
public class FactoryRegistry
{
    private final HashMap<String, ObjectiveFactory> objectiveFactories;
    private final HashMap<String, RequirementFactory> requirementFactories;
    private final HashMap<String, RewardFactory> rewardFactories;
    private final HashMap<String, TriggerFactory> triggerFactories;

    public FactoryRegistry()
    {
        objectiveFactories = new HashMap<>();
        requirementFactories = new HashMap<>();
        rewardFactories = new HashMap<>();
        triggerFactories = new HashMap<>();
    }

    public boolean tryRegisterObjectiveFactory(String id, ObjectiveFactory factory)
    {
        if (objectiveFactories.containsKey(id)) {
            return false;
        }

        objectiveFactories.put(id, factory);
        return true;
    }

    public void registerObjectiveFactory(String id, ObjectiveFactory factory) throws DuplicateFactoryException
    {
        if (!tryRegisterObjectiveFactory(id, factory)) {
            throw new DuplicateFactoryException(id, objectiveFactories.get(id));
        }
    }

    public ObjectiveFactory findObjectiveFactory(String id)
    {
        return objectiveFactories.get(id);
    }

    public Map<String, ObjectiveFactory> getObjectiveFactories()
    {
        return Collections.unmodifiableMap(objectiveFactories);
    }

    public boolean tryRegisterRequirementFactory(String id, RequirementFactory factory)
    {
        if (requirementFactories.containsKey(id)) {
            return false;
        }

        requirementFactories.put(id, factory);
        return true;
    }

    public void registerRequirementFactory(String id, RequirementFactory factory) throws DuplicateFactoryException
    {
        if (!tryRegisterRequirementFactory(id, factory)) {
            throw new DuplicateFactoryException(id, requirementFactories.get(id));
        }
    }

    public RequirementFactory findRequirementFactory(String id)
    {
        return requirementFactories.get(id);
    }

    public Map<String, RequirementFactory> getRequirementsFactories()
    {
        return Collections.unmodifiableMap(requirementFactories);
    }

    public boolean tryRegisterRewardFactory(String id, RewardFactory factory)
    {
        if (rewardFactories.containsKey(id)) {
            return false;
        }

        rewardFactories.put(id, factory);
        return true;
    }

    public void registerRewardFactory(String id, RewardFactory factory) throws DuplicateFactoryException
    {
        if (!tryRegisterRewardFactory(id, factory)) {
            throw new DuplicateFactoryException(id, rewardFactories.get(id));
        }
    }

    public RewardFactory findRewardFactory(String id)
    {
        return rewardFactories.get(id);
    }

    public Map<String, RewardFactory> getRewardFactories()
    {
        return Collections.unmodifiableMap(rewardFactories);
    }

    public boolean tryRegisterTriggerFactory(String id, TriggerFactory factory)
    {
        if (triggerFactories.containsKey(id)) {
            return false;
        }

        triggerFactories.put(id, factory);
        return true;
    }

    public void registerTriggerFactory(String id, TriggerFactory factory) throws DuplicateFactoryException
    {
        if (!tryRegisterTriggerFactory(id, factory)) {
            throw new DuplicateFactoryException(id, triggerFactories.get(id));
        }
    }

    public TriggerFactory findTriggerFactory(String id)
    {
        return triggerFactories.get(id);
    }

    public Map<String, TriggerFactory> getTriggerFactories()
    {
        return Collections.unmodifiableMap(triggerFactories);
    }

    public static class DuplicateFactoryException extends Exception
    {
        private Factory<?, ?> factory;

        public DuplicateFactoryException(String id, Factory<?, ?> factory)
        {
            super("duplicate factory with id " + id);
            this.factory = factory;
        }

        public Factory<?, ?> getFactory()
        {
            return factory;
        }
    }
}
