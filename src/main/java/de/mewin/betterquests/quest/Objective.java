/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.extra.Settings;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public abstract class Objective implements Customizable<ObjectivesHolder>
{
    private final Settings settings;
    private final ObjectivesHolder holder;
    private UUID uuid;

    public Objective(ObjectivesHolder holder)
    {
        this.holder = holder;
        settings = new Settings();
        uuid = UUID.randomUUID();
    }

    public Objective(ObjectivesHolder holder, UUID uuid)
    {
        this.holder = holder;
        this.settings = new Settings();
        this.uuid = uuid;
    }

    public void setUuid(UUID uuid)
    {
        this.uuid = uuid;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public ObjectivesHolder getHolder()
    {
        return holder;
    }

    public double getProgress(Player player, Object data) {
        return -1.0;
    }

    public Object initProgress(Player player)
    {
        return null;
    }

    @Override
    public Settings getSettings()
    {
        return settings;
    }

    public Location getTargetLocation()
    {
        return null;
    }

    public Objective copy(ObjectivesHolder holder)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        ObjectiveFactory factory = plugin
                .getFactoryRegistry()
                .findObjectiveFactory(getFactoryId());

        if (factory == null)
        {
            plugin.getLogger().log(Level.SEVERE, "missing factory while copying objective!");
            return null; // TODO: return dummy (?)
        }

        try
        {
            Objective copy = factory.fromString(holder, factory.makeString(this));
            copy.settings.setValues(settings.getValues());
            return copy;
        }
        catch(IOException ex)
        {
            plugin.getLogger().log(Level.SEVERE, "could not clone objective: ", ex);
            throw new RuntimeException("could not clone objective", ex);
        }
    }

    public int getMaxUpdateInterval()
    {
        return Integer.MAX_VALUE;
    }

    public void onStageEnd(Player player, Object data) {}
    public void onRemove() {}

    public abstract boolean testObjective(Player player, Object data);
    public abstract String getDescription(Player player, Object data);
}
