/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.quest.extra.Settings;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

/**
 *
 * @author mewin
 */
public interface Customizable<T>
{
    Settings getSettings();
    String getMenuLabel(CommandSender sender);
    Prompt createPrompt(MenuPrompt parentPrompt, T base, CommandSender sender);
    String getFactoryId();
}
