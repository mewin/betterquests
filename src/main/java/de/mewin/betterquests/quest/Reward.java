/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.extra.Settings;
import de.mewin.betterquests.quest.factory.RewardFactory;
import java.io.IOException;
import java.util.logging.Level;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public abstract class Reward implements Customizable<RewardsHolder>
{
    private final RewardsHolder holder;
    private final Settings settings;

    public Reward(RewardsHolder holder)
    {
        this.holder = holder;
        this.settings = new Settings();
    }

    public RewardsHolder getHolder()
    {
        return holder;
    }

    @Override
    public Settings getSettings()
    {
        return settings;
    }

    public Reward copy(RewardsHolder holder)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        RewardFactory factory = plugin
                .getFactoryRegistry()
                .findRewardFactory(getFactoryId());

        if (factory == null)
        {
            plugin.getLogger().log(Level.SEVERE, "missing factory while copying reward!");
            return null; // TODO: return dummy (?)
        }

        try
        {
            Reward copy = factory.fromString(holder, factory.makeString(this));
            copy.settings.setValues(settings.getValues());
            return copy;
        }
        catch(IOException ex)
        {
            plugin.getLogger().log(Level.SEVERE, "could not clone reward: ", ex);
            throw new RuntimeException("could not clone reward", ex);
        }
    }

    public abstract void giveReward(Player player);

    public abstract String getMessage(Player player);

    public void onRemove() {}
}
