/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.quest;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.extra.Settings;
import de.mewin.betterquests.quest.factory.RequirementFactory;
import java.io.IOException;
import java.util.logging.Level;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public abstract class Requirement implements Customizable<RequirementsHolder>
{
    private final RequirementsHolder holder;
    private final Settings settings;

    public Requirement(RequirementsHolder holder)
    {
        this.holder = holder;
        this.settings = new Settings();
    }

    @Override
    public Settings getSettings()
    {
        return settings;
    }

    public RequirementsHolder getHolder()
    {
        return holder;
    }

    public Requirement copy(RequirementsHolder holder)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        RequirementFactory factory = plugin
                .getFactoryRegistry()
                .findRequirementFactory(getFactoryId());

        if (factory == null)
        {
            plugin.getLogger().log(Level.SEVERE, "missing factory while copying requirement!");
            return null; // TODO: return dummy (?)
        }

        try
        {
            Requirement copy = factory.fromString(holder, factory.makeString(this));
            copy.settings.setValues(settings.getValues());
            return copy;
        }
        catch(IOException ex)
        {
            plugin.getLogger().log(Level.SEVERE, "could not clone requirement: ", ex);
            throw new RuntimeException("could not clone requirement", ex);
        }
    }

    public abstract boolean testRequirement(Player player);
    public abstract String getFailMessage(Player player);
    public void onApply(Player player) {}
    public void onRemove() {}
}
