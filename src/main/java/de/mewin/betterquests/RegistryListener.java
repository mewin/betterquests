/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import de.mewin.betterquests.quest.event.ObjectiveAddedEvent;
import de.mewin.betterquests.quest.event.ObjectiveRemovedEvent;
import de.mewin.betterquests.quest.event.TriggerAddedEvent;
import de.mewin.betterquests.quest.event.TriggerRemovedEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author mewin
 */
public class RegistryListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public RegistryListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onObjectiveAdded(ObjectiveAddedEvent event)
    {
        plugin.getObjectiveRegistry().register(event.getObjective());
    }

    @EventHandler
    public void onObjectiveRemoved(ObjectiveRemovedEvent event)
    {
        plugin.getObjectiveRegistry().unregister(event.getObjective());
    }

    @EventHandler
    public void onTriggerAdded(TriggerAddedEvent event)
    {
        plugin.getTriggerRegistry().register(event.getTrigger());
    }

    @EventHandler
    public void onTriggerRemoved(TriggerRemovedEvent event)
    {
        plugin.getTriggerRegistry().unregister(event.getTrigger());
        plugin.getLogger().info(plugin.getTriggerRegistry().getRegisteredObjects().toString());
    }
}
