/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import de.mewin.mewinbukkitlib.conversation.acceptors.LocationAcceptorPrompt;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CONVERSATION_MANAGER;

/**
 *
 * @author mewin
 */
public class PromptListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public PromptListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        Block block = event.getClickedBlock();
        Player player = event.getPlayer();

        if (block == null || block.getType() == Material.AIR) {
            return;
        }

        Conversation currentConversation = CONVERSATION_MANAGER.getCurrentConversation(player);
        if (currentConversation == null) {
            return;
        }
        ConversationContext context = currentConversation.getContext();
        Object tmp;
        if ((tmp = context.getSessionData(LocationAcceptorPrompt.IN_LOCATION_PROMPT)) != null
                && tmp instanceof Boolean
                && (Boolean) tmp)
        {
            Location loc = block.getLocation();
            currentConversation.acceptInput(
                    String.format("%s: %d %d %d",
                            loc.getWorld().getUID().toString(),
                            loc.getBlockX(),
                            loc.getBlockY(),
                            loc.getBlockZ()));
            event.setCancelled(true);
        }
    }
}
