/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Stage;
import de.mewin.mewinbukkitlib.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public class QuestUpdateTimer implements Runnable
{
    private final BetterQuestsPlugin plugin;

    public QuestUpdateTimer(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public void run()
    {
        long now = System.currentTimeMillis();
        ProgressManager progressManager = plugin.getProgressManager();
        Map<UUID, PlayerProgress> playerProgresses = progressManager.getPlayerProgresses();

        ArrayList<Pair<Player, QuestProgress>> toUpdate = new ArrayList<>();
        for (Map.Entry<UUID, PlayerProgress> entry : playerProgresses.entrySet())
        {
            UUID playerUUID = entry.getKey();
            Player player = Bukkit.getPlayer(playerUUID);
            if (player == null) { // only update for online players
                continue;
            }

            HashMap<UUID, QuestProgress> questProgresses = entry.getValue().getQuestProgresses();
            for (QuestProgress progress : questProgresses.values())
            {
                Stage stage = progress.getCurrentStage();
                int interval = stage.getMaxUpdateInterval();
                int diff = (int) (now - progress.getLastUpdate());

                if (interval < diff) {
                    toUpdate.add(new Pair<>(player, progress));
                }
            }
        }

        for (Pair<Player, QuestProgress> pair : toUpdate) {
            progressManager.updateQuestProgress(pair.getFirst(), pair.getSecond());
        }
    }
}
