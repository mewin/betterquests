/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.input;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public abstract class BQCommand
{
    protected final BetterQuestsPlugin plugin;
    protected final CommandSender sender;
    protected final Command command;
    protected final String label;
    protected final String[] args;
    protected final UserInputHandler userInputHandler;

    public BQCommand(BetterQuestsPlugin plugin, CommandSender sender, Command command, String label, String[] args)
    {
        this.plugin = plugin;
        this.sender = sender;
        this.command = command;
        this.label = label;
        this.args = args;
        this.userInputHandler = plugin.getUserInputHandler();
    }

    public abstract boolean run();

    public List<String> tabComplete() {
        return null;
    }

    protected List<String> tabCompletePlayerName(String startOrig, boolean offline)
    {
        String start = startOrig.toLowerCase();

        ArrayList<String> completions = new ArrayList<>();

        Collection<? extends OfflinePlayer> players = null;
        if (offline) {
            players = Arrays.asList(Bukkit.getOfflinePlayers());
        } else {
            players = Bukkit.getOnlinePlayers();
        }
        
        for (OfflinePlayer player : players)
        {
            String nameOrig = player.getName();
            String name = nameOrig.toLowerCase();

            if (name.startsWith(start)) {
                completions.add(nameOrig);
            }
        }

        return completions;
    }

    protected List<String> tabCompletePlayerName(String startOrig)
    {
        return tabCompletePlayerName(startOrig, false);
    }

    protected List<String> tabCompleteQuestId(String startOrig)
    {
        String start = startOrig.toLowerCase();

        ArrayList<String> completions = new ArrayList<>();

        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Map<String, Quest> quests = questRegistry.getQuests();

        for (String questIdOrig : quests.keySet())
        {
            String questId = questIdOrig.toLowerCase();

            if (questId.startsWith(start)) {
                completions.add(questIdOrig);
            }
        }

        return completions;
    }

    protected List<String> tabCompleteAny(String startOrig, String ... strings)
    {
        String start = startOrig.toLowerCase();

        ArrayList<String> completions = new ArrayList<>();

        for (String stringOrig : strings)
        {
            String string = stringOrig.toLowerCase();

            if (string.startsWith(start)) {
                completions.add(stringOrig);
            }
        }

        return completions;
    }

    protected int positionalArgPos(String[] args, String ... namedArgNames)
    {
        HashSet<String> named = new HashSet<>(Arrays.asList(namedArgNames));
        int pos = args.length - 1;

        for (String arg : args)
        {
            if ("".equals(arg)) {
                continue;
            }

            if (arg.charAt(0) == '-')
            {
                --pos;
                if (named.contains(arg.substring(1))) {
                    --pos; // next one is named arg value
                }
            }
        }

        return Math.max(0, pos);
    }
}
