/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.input;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.conversation.DeleteQuestPrompt;
import de.mewin.betterquests.conversation.editors.EditQuestPrompt;
import de.mewin.betterquests.conversation.NewQuestPrompt;
import de.mewin.betterquests.conversation.SelectQuestPrompt;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.util.ConfigKeys;
import de.mewin.mewinbukkitlib.util.InputUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CommandBetterQuests extends BQCommand
{
    public CommandBetterQuests(BetterQuestsPlugin plugin, CommandSender sender, Command command, String label, String[] args)
    {
        super(plugin, sender, command, label, args);
    }

    @Override
    public boolean run()
    {
        if (sender instanceof Conversable && ((Conversable) sender).isConversing())
        {
            CHAT_HELPER.sendMessage(sender, "in-conversation");
            return true;
        }

        if (args.length < 1)
        {
            return cmdSendHelp(sender, label);
        }
        else
        {
            switch(args[0].toLowerCase())
            {
                case "a":
                case "advertise":
                    return cmdAdvertiseQuest(sender, args);
                case "f":
                case "finish":
                    return cmdFinishQuest(sender, args);
                case "c":
                case "n":
                case "new":
                case "create":
                    return cmdNewQuest(sender, args);
                case "cp":
                case "copy":
                    return cmdCopyQuest(sender, args);
                case "e":
                case "edit":
                    return cmdEditQuest(sender, args);
                case "d":
                case "del":
                case "delete":
                    return cmdDeleteQuest(sender, args);
                case "l":
                case "ls":
                case "list":
                    return cmdListQuests(sender, args);
                case "near":
                    return cmdNearQuests(sender, args);
                case "p":
                case "progress":
                    return cmdPlayerProgress(sender, args);
                case "s":
                case "save":
                    return cmdSaveQuests(sender);
                case "help":
                    return cmdSendHelp(sender, label);
                case "version":
                    return cmdSendVersion(sender);
                case "ls-objectives":
                    return cmdSendObjectiveRegistry(sender);
                case "ls-triggers":
                    return cmdSendTriggerRegistry(sender);
                case "tp-trigger":
                    return cmdTpToTrigger(sender, args);
            }
        }
        return false;
    }

    @Override
    public List<String> tabComplete()
    {
        if (args.length < 1) {
            return tabCompleteSubCmds(""); // this actually doesnt happen, just for safety
        } else if (args.length < 2) {
            return tabCompleteSubCmds(args[0]);
        }
        else // args.length >= 2
        {
            switch(args[0])
            {
                case "a":
                case "advertise":
                    return tabCompleteAdvertiseQuest(sender, args);
                case "f":
                case "finish":
                    return tabCompleteFinishQuest(sender, args);
                case "c":
                case "n":
                case "new":
                case "create":
                    return tabCompleteNewQuest(sender, args);
                case "cp":
                case "copy":
                    return tabCompleteCopyQuest(sender, args);
                case "e":
                case "edit":
                    return tabCompleteEditQuest(sender, args);
                case "d":
                case "del":
                case "delete":
                    return tabCompleteDeleteQuest(sender, args);
                case "l":
                case "ls":
                case "list":
                    return tabCompleteListQuests(sender, args);
                case "near":
                    return tabCompleteNearQuests(sender, args);
                case "p":
                case "progress":
                    return tabCompleteQuestProgress(sender, args);
                case "s":
                case "save":
                    return tabCompleteSaveQuests(sender, args);
                case "help":
                    return tabCompleteHelp(sender, args);
                case "version":
                    return tabCompleteVersion(sender, args);
                case "tp-trigger":
                    return tabCompleteTpTrigger(sender, args);
            }
        }

        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteSubCmds(String start_)
    {
        final String[] available = {
            "advertise", "copy", "create", "delete", "edit", "finish", "list", "near",
            "new", "progress", "save", "help", "version", "tp-trigger"
        };

        if ("".equals(start_)) {
            return Arrays.asList(available);
        }

        String start = start_.toLowerCase();

        ArrayList<String> cmds = new ArrayList<>();

        for (String cmd : available)
        {
            if (cmd.startsWith(start)) {
                cmds.add(cmd);
            }
        }

        return cmds;
    }

    private boolean cmdAdvertiseQuest(CommandSender sender, String[] args)
    {
        userInputHandler.doAdvertiseQuest(sender, args);
        return true;
    }

    private boolean cmdFinishQuest(CommandSender sender, String[] args)
    {
        userInputHandler.doFinishQuest(sender, args);
        return true;
    }

    private boolean cmdSendHelp(CommandSender sender, String label)
    {
        CHAT_HELPER.sendMessage(sender, "help-text", label);
        return true;
    }

    private boolean cmdSendVersion(CommandSender sender)
    {
        CHAT_HELPER.sendMessage(sender, "version-info", plugin.getDescription().getVersion());
        return true;
    }

    private boolean cmdSendObjectiveRegistry(CommandSender sender)
    {
        userInputHandler.doSendObjectiveRegistry(sender);
        return true;
    }

    private boolean cmdSendTriggerRegistry(CommandSender sender)
    {
        userInputHandler.doSendTriggerRegistry(sender);
        return true;
    }

    private boolean cmdTpToTrigger(CommandSender sender, String[] args)
    {
        userInputHandler.doTpToTrigger(sender, args);
        return true;
    }

    private boolean cmdNewQuest(CommandSender sender, String[] args)
    {
        if (args.length > 1) {
            return userInputHandler.doCreateQuest(sender, args[1]);
        }

        if (sender instanceof Conversable)
        {
            plugin.startConversation(new NewQuestPrompt(plugin), (Conversable) sender);
            return true;
        }

        return false;
    }

    private boolean cmdCopyQuest(CommandSender sender, String[] args)
    {
        if (args.length > 2) {
            return userInputHandler.doCopyQuest(sender, args[1], args[2]);
        }

        // if (sender instanceof)
        return false;
    }

    private boolean cmdEditQuest(final CommandSender sender, String[] args)
    {
        if (args.length > 1)
        {
            return userInputHandler.doEditQuest(sender, args[1]);
        }

        if (sender instanceof Conversable)
        {
            Function<Quest, Prompt> func = new Function<Quest, Prompt>() {
                @Override
                public Prompt apply(Quest quest)
                {
                    EditQuestPrompt editQuestPrompt = new EditQuestPrompt(quest, sender);

                    return editQuestPrompt;
                }
            };

            plugin.startConversation(new SelectQuestPrompt(plugin, func), (Conversable) sender);
            return true;

        }
        return false;
    }

    private boolean cmdDeleteQuest(CommandSender sender, String[] args)
    {
        if (args.length > 1)
        {
            return userInputHandler.doDeleteQuest(sender, args[1]);
        }

        if (sender instanceof Conversable)
        {
            DeleteQuestPrompt deleteQuestPrompt = new DeleteQuestPrompt(plugin);

            plugin.startConversation(new SelectQuestPrompt(plugin, deleteQuestPrompt), (Conversable) sender);
            return true;
        }

        return false;
    }

    private boolean cmdListQuests(CommandSender sender, String[] args)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();

        Map<String, Quest> quests = questRegistry.getQuests();
        if (quests.size() < 1) {
            CHAT_HELPER.sendMessage(sender, "no-quests");
        }
        else
        {
            int perSite = plugin.getConfig().getInt(ConfigKeys.QUESTS_PER_PAGE, 8);
            int nQuests = questRegistry.getQuests().size();
            int page = 1;
            int maxPage = InputUtil.intDivCeil(nQuests, perSite);
            if (args.length > 1)
            {
                page = InputUtil.toInt(args[1], page);
            }
            if (page < 1) {
                page = 1;
            }
            if (page > maxPage) {
                page = maxPage;
            }
            userInputHandler.doListQuests(sender, (page - 1) * perSite, perSite);
            CHAT_HELPER.sendMessage(sender, "quest-list-page", page, maxPage);
        }
        return true;
    }

    private boolean cmdNearQuests(CommandSender sender, String[] args)
    {
        if (!(sender instanceof Player))
        {
            CHAT_HELPER.sendMessage(sender, "cmd-player-only");
            return true;
        }

        userInputHandler.doShowNearQuests(sender, 50.0); // TODO: paramterize this

        return true;
    }

    @SuppressWarnings("deprecation")
    private boolean cmdPlayerProgress(CommandSender sender, String[] args)
    {
        userInputHandler.doShowProgressEditor(sender, args);

        return true;
    }

    private boolean cmdSaveQuests(CommandSender sender)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        ProgressManager progressManager = plugin.getProgressManager();

        try
        {
            questRegistry.saveQuests();
            progressManager.saveProgress();
            CHAT_HELPER.sendMessage(sender, "quests-saved");
        }
        catch(IOException ex)
        {
            CHAT_HELPER.sendMessage(sender, "quests-not-saved", ex.getMessage());
        }
        return true;
    }

    private List<String> tabCompleteAdvertiseQuest(CommandSender sender, String[] args)
    {
        switch(args.length)
        {
            case 2:
                return tabCompletePlayerName(args[1]);
            case 3:
                return tabCompleteQuestId(args[2]);
        }

        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteFinishQuest(CommandSender sender, String[] args)
    {
        String arg = args[args.length - 1];
        if (arg.startsWith("-")) {
            return tabCompleteAny(arg, "-f");
        }

        int pos = positionalArgPos(args);

        switch(pos)
        {
            case 1:
                return tabCompletePlayerName(arg);
            case 2:
                return tabCompleteQuestId(arg);
        }

        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteNewQuest(CommandSender sender, String[] args)
    {
        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteCopyQuest(CommandSender sender, String[] args)
    {
        switch (args.length)
        {
            case 2:
                return tabCompleteQuestId(args[1]);
        }

        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteEditQuest(CommandSender sender, String[] args)
    {
        switch (args.length)
        {
            case 2:
                return tabCompleteQuestId(args[1]);
        }

        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteDeleteQuest(CommandSender sender, String[] args)
    {
        switch (args.length)
        {
            case 2:
                return tabCompleteQuestId(args[1]);
        }

        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteListQuests(CommandSender sender, String[] args)
    {
        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteNearQuests(CommandSender sender, String[] args)
    {
        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteQuestProgress(CommandSender sender, String[] args)
    {
        switch (args.length)
        {
            case 2:
                return tabCompletePlayerName(args[1], true);
        }

        return Collections.emptyList();
    }

    private List<String> tabCompleteSaveQuests(CommandSender sender, String[] args)
    {
        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteHelp(CommandSender sender, String[] args)
    {
        if (args.length < 1) { // this shouldnt happen
            return Collections.EMPTY_LIST;
        }
        return tabCompleteSubCmds(args[1]);
    }

    private List<String> tabCompleteVersion(CommandSender sender, String[] args)
    {
        return Collections.EMPTY_LIST;
    }

    private List<String> tabCompleteTpTrigger(CommandSender sender, String[] args)
    {
        switch (args.length)
        {
            case 2:
                return tabCompleteQuestId(args[1]);
        }

        return Collections.EMPTY_LIST;
    }
}
