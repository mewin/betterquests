/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.input;

import de.mewin.betterquests.BetterQuestsPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CommandQuest extends BQCommand
{
    public CommandQuest(BetterQuestsPlugin plugin, CommandSender sender, Command command, String label, String[] args)
    {
        super(plugin, sender, command, label, args);
    }

    @Override
    public boolean run()
    {
        if (!(sender instanceof Player))
        {
            CHAT_HELPER.sendMessage(sender, "cmd-player-only");
            return true;
        }

        Player player = (Player) sender;

        if (args.length < 1) {
            return cmdListCurrentQuests(player, args);
        }

        switch(args[0])
        {
            case "l":
            case "list":
                return cmdListCurrentQuests(player, args);
//            case "i":
//            case "info":
//                return cmdQuestInfo(player, args);
            case "log":
                return cmdQuestLog(player, args);
        }

        return false;
    }

    private boolean cmdListCurrentQuests(Player player, String[] args)
    {
        userInputHandler.doListCurrentQuests(player, args);
        return true;
    }

    private boolean cmdQuestInfo(Player player, String[] args)
    {
        if (args.length < 2) {
            // CHAT_HELPER.sendMessage(player, "")
            return true;
        }

        userInputHandler.doQuestInfo(player, args[1]);

        return true;
    }

    private boolean cmdQuestLog(Player player, String[] args)
    {
        QuestLog.giveToPlayer(player);

        return true;
    }
}
