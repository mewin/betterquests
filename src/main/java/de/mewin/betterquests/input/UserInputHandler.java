/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.input;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.chat.DefaultQuestFilter;
import de.mewin.betterquests.chat.InteractiveQuestFilter;
import de.mewin.betterquests.conversation.DeleteQuestPrompt;
import de.mewin.betterquests.conversation.editors.EditQuestPrompt;
import de.mewin.betterquests.progress.FinishedQuestInfo;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.event.ShowQuestLogEvent;
import de.mewin.betterquests.util.ProtoHelper;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.entity.Player;
import de.mewin.betterquests.chat.QuestFilter;
import de.mewin.betterquests.conversation.ProgressQuestPrompt;
import de.mewin.betterquests.conversation.editors.EditFinishedQuestPrompt;
import de.mewin.betterquests.conversation.editors.EditProgressPrompt;
import de.mewin.betterquests.util.MinecraftUtil;
import java.util.function.Function;
import org.bukkit.OfflinePlayer;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class UserInputHandler
{
    private final BetterQuestsPlugin plugin;

    public UserInputHandler(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    public ArgParseResult parseArgs(String[] args, HashSet<String> namedArgs)
    {
        ArgParseResult result = new ArgParseResult();
        boolean noflags = false;

        for (int i = 1; i < args.length; ++i) // skip first (command itself)
        {
            String arg = args[i];

            if ("".equals(arg)) {
                continue; // this actually shouldnt happen when using split()
            }

            if (!noflags && arg.charAt(0) == '-')
            {
                String flagName = arg.substring(1);
                if ("".equals(flagName))
                { // stop parsing flags
                    noflags = true;
                    continue;
                }

                if (namedArgs.contains(flagName))
                {
                    String value = (i < args.length - 1) ? args[i + 1] : "";
                    result.namedParams.put(flagName, value);
                    ++i;
                }
                else {
                    result.flags.add(flagName);
                }
            }
            else {
                result.positional.add(arg);
            }
        }

        return result;
    }

    private ArgParseResult parseArgs(String[] args)
    {
        return parseArgs(args, new HashSet<String>());
    }

    public boolean doCreateQuest(CommandSender sender, String questId)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        try
        {
            questRegistry.registerQuest(new Quest(questId));
        }
        catch (QuestRegistry.DuplicateQuestException ex)
        {
            CHAT_HELPER.sendMessage(sender, "duplicate-quest", questId);
            return false;
        }

        CHAT_HELPER.sendMessage(sender, "quest-created", questId);
        plugin.getLogger().log(Level.INFO, "Registered new quest with id \"{0}\"", questId);

        return doEditQuest(sender, questId);
    }

    public boolean doCopyQuest(CommandSender sender, String questId, String newId)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(questId);

        if (quest == null)
        {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", questId);
            return true;
        }

        try
        {
            questRegistry.copyQuest(quest, newId);
        }
        catch(QuestRegistry.DuplicateQuestException ex)
        {
            CHAT_HELPER.sendMessage(sender, "duplicate-quest", newId);
            return false;
        }

        CHAT_HELPER.sendMessage(sender, "quest-created", newId);
        plugin.getLogger().log(Level.INFO, "Registered new quest with id \"{0}\"", newId);

        return doEditQuest(sender, newId);
    }

    public void doListQuests(CommandSender sender, int startIndex, int count, final QuestFilter format, List<Quest> quests)
    {
        if (format.isFiltering())
        {
            ArrayList<Quest> filtered = new ArrayList<>();
            for (Quest quest : quests)
            {
                if (format.showQuest(quest)) {
                    filtered.add(quest);
                }
            }
            quests = filtered;
        }

        int endIndex = Math.min(startIndex + count, quests.size());
        CHAT_HELPER.sendMessage(sender, "quest-list-header", startIndex + 1, endIndex);
        for (int i = startIndex; i < endIndex; ++i)
        {
            Quest quest = quests.get(i);

            format.sendQuest(sender, i, quest);
        }
    }

    public void doListQuests(CommandSender sender, int startIndex, int count, final QuestFilter format)
    {
        QuestRegistry qr = plugin.getQuestRegistry();

        doListQuests(sender, startIndex, count, format, qr.getSortedQuests());
    }

    public void doListQuests(CommandSender sender, int startIndex, int count, boolean interactive)
    {
        if (interactive) {
            doListQuests(sender, startIndex, count, new InteractiveQuestFilter());
        } else {
            doListQuests(sender, startIndex, count, new DefaultQuestFilter());
        }
    }

    public void doListQuests(CommandSender sender, int startIndex, int count)
    {
        doListQuests(sender, startIndex, count, false);
    }

    public void doShowNearQuests(CommandSender sender, double distance)
    {
        if (!(sender instanceof Player)) {
            return;
        }

        double distanceSq = distance * distance;

        Player player = (Player) sender;
        Location playerLoc = player.getLocation();

        ArrayList<Trigger> nearTriggers = new ArrayList<>();
        ObjectsByClassRegistry<Trigger> triggerRegistry = plugin.getTriggerRegistry();
        Map<Class<?>, ArrayList<Trigger>> allTriggers = triggerRegistry.getRegisteredObjects();
        for (ArrayList<Trigger> triggers : allTriggers.values())
        {
            for (Trigger trigger : triggers)
            {
                Location loc = trigger.getLocation();
                if (loc != null && loc.getWorld().equals(playerLoc.getWorld())
                        && loc.distanceSquared(playerLoc) < distanceSq)
                {
                    nearTriggers.add(trigger);
                }
            }
        }

        for (Trigger trigger : nearTriggers)
        {
            ProtoHelper.sendPlayerHoloDisplay(player, trigger.getLocation(), "hi");
        }
    }

    public boolean doEditQuest(CommandSender sender, String arg)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(arg);

        if (quest == null)
        {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", arg);
            return true;
        }

        if (sender instanceof Conversable)
        {

            EditQuestPrompt editQuestPrompt = new EditQuestPrompt(quest, sender);

            plugin.startConversation(editQuestPrompt, (Conversable) sender);
        }

        return true;
    }

    public boolean doDeleteQuest(CommandSender sender, String arg)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(arg);

        if (quest == null)
        {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", arg);
            return true;
        }

        if (sender instanceof Conversable)
        {
            DeleteQuestPrompt deleteQuestPrompt = new DeleteQuestPrompt(plugin, quest);

            plugin.startConversation(deleteQuestPrompt, (Conversable) sender);
        }

        return true;
    }

    public boolean doRenameQuest(CommandSender sender, Quest quest, String questId)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();

        if (!Quest.isValidQuestId(questId))
        {
            CHAT_HELPER.sendMessage(sender, "quest-illegalname", questId);
            return false;
        }

        try
        {
            questRegistry.renameQuest(quest, questId);
        }
        catch (QuestRegistry.DuplicateQuestException ex)
        {
            CHAT_HELPER.sendMessage(sender, "duplicate-quest", questId);
            return false;
        }

        CHAT_HELPER.sendMessage(sender, "quest-renamed", questId);
        plugin.getLogger().log(Level.INFO, "Renamed quest with id \"{0}\"", questId);

        return true;
    }

    @SuppressWarnings("deprecation")
    public void doAdvertiseQuest(CommandSender sender, String[] args)
    {
        if (args.length < 3)
        {
            CHAT_HELPER.sendMessage(sender, "cmd-advertise-usage");
            return;
        }

        Player target = Bukkit.getPlayer(args[1]);
        if (target == null) {
            CHAT_HELPER.sendMessage(sender, "player-not-found", args[1]);
            return;
        }
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(args[2]);
        if (quest == null) {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", args[2]);
            return;
        }
        if (!quest.isQuestReady()) {
            CHAT_HELPER.sendMessage(sender, "quest-not-ready", quest.getId());
            return;
        }

        ProgressManager progressTracker = plugin.getProgressManager();
        progressTracker.advertiseQuest(target, quest);
    }

    @SuppressWarnings("deprecation")
    public void doFinishQuest(CommandSender sender, String[] args)
    {
        ArgParseResult parsed = parseArgs(args);

        if (parsed.positional.size() < 2)
        {
            CHAT_HELPER.sendMessage(sender, "cmd-finish-usage");
            return;
        }

        boolean failed = parsed.flags.contains("f");
        String playerName = parsed.positional.get(0);
        String questId = parsed.positional.get(1);

        Player player = Bukkit.getPlayer(playerName);

        if (player == null)
        {
            CHAT_HELPER.sendMessage(sender, "player-not-found", playerName);
            return;
        }

        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(questId);

        if (quest == null)
        {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", questId);
            return;
        }

        ProgressManager progressManager = plugin.getProgressManager();

        if (failed) {
            progressManager.failQuest(player, quest);
        }
        else {
            progressManager.finishQuest(player, quest);
        }

    }

    public void doAcceptAdvertisement(CommandSender sender, String arg)
    {
        if (!(sender instanceof Player))
        {
            sender.sendMessage("Only players can accept quests :/");
            return;
        }

        try
        {
            UUID uuid = UUID.fromString(arg);

            doAcceptAdvertisement(sender, uuid);
        }
        catch(IllegalArgumentException ex) {

        }
    }

    public void doAcceptAdvertisement(CommandSender sender, UUID uuid)
    {
        ProgressManager progressManager = plugin.getProgressManager();

        progressManager.acceptAdvertisement(uuid);
    }

    public void doPrintQuest(Player player, int index, Quest quest)
    {
        JSONText[] texts = new JSONText.MultiBuilder()
                .setText(CHAT_HELPER.getMessage(player, "quest-list-entry", index, quest.getTitle()))
                .next()
                .setText(" ")
                .next()
                .setText(CHAT_HELPER.getMessage(player, "menu-view-quest"))
                .setClickEvent(new RunCommandEvent("/bq_callback info " + quest.getId()))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(player, "menu-view-quest-hover")))
                .next()
                .setText(" ")
                .next()
                .setText(CHAT_HELPER.getMessage(player, "menu-abort-quest-player"))
                .setClickEvent(new RunCommandEvent("/bq_callback abort " + quest.getId()))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(player, "menu-abort-quest-player-hover")))
                .build();

        CHAT_HELPER.sendJSONMessage(player, texts);
    }

    public void doListCurrentQuests(Player player, String[] args)
    {
        ProgressManager progressManager = plugin.getProgressManager();
        PlayerProgress playerProgress = progressManager.getPlayerProgress(player);

        boolean showFinished = false;
        String filter = null;

        for (int i = 1; i < args.length; ++i)
        {
            switch(args[i])
            {
                case "-f":
                    showFinished = true;
                    break;
                default:
                    if (filter == null) {
                        filter = args[i];
                    }
                    else {
                        filter = filter + " " + args[i];
                    }
                    break;
            }
        }

        if (showFinished)
        {
            List<FinishedQuestInfo> finishedQuests = new ArrayList<>(playerProgress.getFinishedQuests().values());

            if (finishedQuests.isEmpty())
            {
                CHAT_HELPER.sendMessage(player, "no-finished-quests");
                return;
            }

            CHAT_HELPER.sendMessage(player, "finished-quests-header");

            for (int i = 0; i < finishedQuests.size(); ++i)
            {
                FinishedQuestInfo finishedQuestInfo = finishedQuests.get(i);
                Quest quest = finishedQuestInfo.getQuest();

                doPrintQuest(player, i + 1, quest);
            }
        }
        else
        {
            List<QuestProgress> questProgresses = new ArrayList<>(playerProgress.getQuestProgresses().values());

            if (questProgresses.isEmpty())
            {
                CHAT_HELPER.sendMessage(player, "no-active-quests");
                return;
            }

            CHAT_HELPER.sendMessage(player, "active-quests-header");
            for (int i = 0; i < questProgresses.size(); ++i)
            {
                QuestProgress progress = questProgresses.get(i);
                Quest quest = progress.getQuest();
                doPrintQuest(player, i + 1, quest);
            }
        }
    }

    public void doQuestInfo(Player player, String arg)
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(arg);

        if (quest == null)
        {
            CHAT_HELPER.sendMessage(player, "quest-unknown", arg);
            return;
        }

        ProgressManager progressManager = plugin.getProgressManager();
        QuestProgress questProgess = progressManager.getQuestProgress(player, quest);

        if (questProgess == null)
        {
            CHAT_HELPER.sendMessage(player, "no-info");
            return;
        }

        progressManager.sendObjectives(player, questProgess);
    }

    public void doSendObjectiveRegistry(CommandSender sender)
    {
        ObjectsByClassRegistry<Objective> objectiveRegistry = plugin.getObjectiveRegistry();
        Map<Class<?>, ArrayList<Objective>> objectives = objectiveRegistry.getRegisteredObjects();

        for (Map.Entry<Class<?>, ArrayList<Objective>> entry : objectives.entrySet())
        {
            sender.sendMessage("§e" + entry.getKey().getName());

            for (Objective objective : entry.getValue())
            {
                sender.sendMessage(" " + objective.getUuid());
            }
        }
    }

    public void doSendTriggerRegistry(CommandSender sender)
    {
        ObjectsByClassRegistry<Trigger> triggerRegistry = plugin.getTriggerRegistry();
        Map<Class<?>, ArrayList<Trigger>> triggers = triggerRegistry.getRegisteredObjects();

        for (Map.Entry<Class<?>, ArrayList<Trigger>> entry : triggers.entrySet())
        {
            sender.sendMessage("§e" + entry.getKey().getName());

            for (Trigger trigger : entry.getValue())
            {
                sender.sendMessage(" " + trigger.getFactoryId());
            }
        }
    }

    public void doTpToTrigger(CommandSender sender, String[] args)
    {
        if (!(sender instanceof Player))
        {
            CHAT_HELPER.sendMessage(sender, "cmd-player-only");
            return;
        }

        if (args.length < 2)
        {
            CHAT_HELPER.sendMessage(sender, "cmd-tp-trigger-usage");
            return;
        }

        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(args[1]);

        if (quest == null)
        {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", args[1]);
            return;
        }

        List<Trigger> triggers = quest.getTriggers();
        if (triggers.isEmpty())
        {
            CHAT_HELPER.sendMessage(sender, "cmd-tp-trigger-no-triggers");
            return;
        }
        Trigger trigger;
        if (args.length < 3)
        {
            if (triggers.size() > 2)
            {
                CHAT_HELPER.sendMessage(sender, "cmd-tp-trigger-multiple-triggers", triggers.size());
                return;
            }
            trigger = triggers.get(0);
        }
        else
        {
            try
            {
                int index = Integer.valueOf(args[2]) - 1;
                if (index < 0 || index >= triggers.size())
                {
                    CHAT_HELPER.sendMessage(sender, "cmd-invalid-index", args[2]);
                    return;
                }
                trigger = triggers.get(index);
            }
            catch(NumberFormatException ex)
            {
                CHAT_HELPER.sendMessage(sender, "cmd-invalid-number", args[2]);
                return;
            }
        }

        Location loc = trigger.getLocation();
        if (loc == null)
        {
            CHAT_HELPER.sendMessage(sender, "cmd-tp-trigger-no-location");
            return;
        }

        Player player = (Player) sender;
        if (!MinecraftUtil.teleportNear(player, loc)) {
            CHAT_HELPER.sendMessage(sender, "cmd-tp-trigger-no-teleport");
        }
    }

    public void doShowProgressEditor(CommandSender sender, String[] args)
    {

        if (args.length < 2)
        {
            CHAT_HELPER.sendMessage(sender, "cmd-progress-usage");
            return;
        }

        if (sender instanceof Conversable)
        {
            final OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);

            Function<Quest, Prompt> func = new Function<Quest, Prompt>() {
                @Override
                public Prompt apply(Quest quest)
                {
                    ProgressManager pm = plugin.getProgressManager();
                    QuestProgress progress = pm.getQuestProgress(player, quest);
                    if (progress != null) {
                        return new EditProgressPrompt(player, progress);
                    }
                    FinishedQuestInfo finishedQuest = pm.getFinishedQuest(player, quest);
                    if (finishedQuest != null) {
                        return new EditFinishedQuestPrompt(player, finishedQuest);
                    }
                    return null;
                }
            };

            plugin.startConversation(new ProgressQuestPrompt(plugin, Prompt.END_OF_CONVERSATION,
                    func, player.getUniqueId()), (Conversable) sender);
        }
    }

    public void doShowQuestLog(Player player)
    {
        ShowQuestLogEvent event = new ShowQuestLogEvent(player);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return;
        }
    }

    public void doSendAdvertisement(Player player, String arg)
    {
        ProgressManager progressManager = plugin.getProgressManager();
        try {
            progressManager.sendAdvertisement(UUID.fromString(arg));
        } catch(IllegalArgumentException ex) {}
    }

    public static class ArgParseResult
    {
        public final HashSet<String> flags;
        public final HashMap<String, String> namedParams;
        public final ArrayList<String> positional;

        public ArgParseResult()
        {
            this.flags = new HashSet<>();
            this.namedParams = new HashMap<>();
            this.positional = new ArrayList<>();
        }
    }
}
