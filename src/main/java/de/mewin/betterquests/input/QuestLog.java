/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.input;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.FinishedQuestInfo;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.util.TextUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class QuestLog
{
    private static String formatFirstPage(Player player, int nActive, int nFinished)
    {
        StringBuilder sb = new StringBuilder();

        sb.append(MAGIC_STRING).append("§r");

        sb.append("\n\n");
        sb.append(ChatColor.BOLD);
        sb.append(TextUtil.centerString(CHAT_HELPER.getMessage(player, "quest-log-title"), LINE_WIDTH));

        sb.append("\n\n§r");
        sb.append(CHAT_HELPER.formatMessage(player, "quest-log-num-active", nActive)).append("\n§r");
        sb.append(CHAT_HELPER.formatMessage(player, "quest-log-num-finished", nFinished)).append("\n§r");

        return sb.toString();
    }

    private static ArrayList<String> bookSplitLines(String text)
    {
        ArrayList<String> lines = new ArrayList<>();

        for (String line : text.split("\n"))
        {
            int lineLen = TextUtil.formatStringLength(line);
            for (int i = 0; i < lineLen; i += LINE_WIDTH) {
                String part = TextUtil.formatSubString(line, i, Math.min(lineLen, i + LINE_WIDTH));
                lines.add(part);
            }
        }

        return lines;
    }

    private static String[] bookSplitPages(ArrayList<String> lines)
    {
        int nPage = (int) Math.ceil((double) lines.size() / LINES_PER_PAGE);
        String[] pages = new String[nPage];

        for (int page = 0; page < pages.length; ++page)
        {
            pages[page] = String.join("\n", lines.subList(page * LINES_PER_PAGE,
                                Math.min((page + 1) * LINES_PER_PAGE, lines.size())));
        }

        return pages;
    }

    private static ArrayList<String> formatObjective(Player player, QuestProgress progress)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        ProgressManager progressManager = plugin.getProgressManager();
        Quest quest = progress.getQuest();

        ArrayList<String> lines = new ArrayList<>();

        lines.add("§n" + CHAT_HELPER.formatPlayerString(quest.getTitle(), player) + "§r");
        String objectives = progressManager.formatObjectiveMessage(player, progress);

        lines.addAll(bookSplitLines(objectives));

        return lines;
    }

    private static ToCEntry formatObjectives(Player player, List<QuestProgress> active)
    {
        ArrayList<String> lines = new ArrayList<>();
        ArrayList<String> pages = new ArrayList<>();

        String txtObjectives = CHAT_HELPER.formatMessage(player, "quest-log-objectives");

        lines.add(txtObjectives);

        for (QuestProgress progress : active)
        {
            ArrayList<String> progressLines = formatObjective(player, progress);

            if (progressLines.size() + lines.size() + 1 > LINES_PER_PAGE)
            {
                pages.add(String.join("\n", lines));
                lines = progressLines;
            }
            else
            {
                lines.add("§r");
                lines.addAll(progressLines);
            }
        }

        if (!lines.isEmpty()) {
            pages.add(String.join("\n", lines));
        }

        return new ToCEntry(txtObjectives, pages.toArray(new String[0]));
    }

    private static void formatActiveQuest(Player player, QuestProgress progress, ArrayList<String> lines)
    {
        Quest quest = progress.getQuest();

        lines.add("§n" + CHAT_HELPER.formatPlayerString(quest.getTitle(), player));

        lines.add("§r");

        lines.addAll(bookSplitLines(CHAT_HELPER.formatPlayerString(quest.getDescription(), player)));
    }

    private static ToCEntry formatActiveQuests(Player player, List<QuestProgress> active)
    {
        ArrayList<String> lines = new ArrayList<>();

        String txtActive = CHAT_HELPER.formatMessage(player, "quest-log-active");

        lines.add(txtActive);

        for (QuestProgress progress : active)
        {
            lines.add("§r");

            formatActiveQuest(player, progress, lines);
        }

        return new ToCEntry(txtActive, bookSplitPages(lines));
    }

    private static void formatFinishedQuest(Player player, FinishedQuestInfo info, ArrayList<String> lines)
    {
        Quest quest = info.getQuest();

        lines.add("§n" + CHAT_HELPER.formatPlayerString(quest.getTitle(), player));

        lines.add("§r");

        lines.addAll(bookSplitLines(CHAT_HELPER.formatPlayerString(quest.getDescription(), player)));
    }

    private static ToCEntry formatFinishedQuests(Player player, List<FinishedQuestInfo> active)
    {
        ArrayList<String> lines = new ArrayList<>();

        String txtFinished = CHAT_HELPER.formatMessage(player, "quest-log-finished");

        lines.add(txtFinished);

        for (FinishedQuestInfo info : active)
        {
            lines.add("§r");

            formatFinishedQuest(player, info, lines);
        }

        return new ToCEntry(txtFinished, bookSplitPages(lines));
    }

    private static String[] formatTableOfContents(int offset, Player player, ToCEntry ... entries)
    {
        int myPages = (int) Math.ceil((entries.length + 2.0) / LINES_PER_PAGE);

        ArrayList<String> pages = new ArrayList<>(myPages);

        StringBuilder sb = new StringBuilder(CHAT_HELPER.formatMessage(player, "quest-log-toc"));
        sb.append("\n\n§r");

        int page = 1 + offset + myPages;

        for (int i = 0; i < entries.length; ++i)
        {
            ToCEntry entry = entries[i];

            sb.append(CHAT_HELPER.formatMessage(player, "quest-log-page", page));
            sb.append(" §r").append(entry.name);
            sb.append("§r\n");

            page += entry.pages.length;

            if ((i + 2) % LINES_PER_PAGE == 0)
            {
                pages.add(sb.toString());
                sb = new StringBuilder();
            }
        }

        String lastPage = sb.toString();
        if (!"".equals(lastPage.trim())) {
            pages.add(lastPage);
        }

        return pages.toArray(new String[0]);
    }

    private static void fillQuestLog(Player player, BookMeta bookMeta)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        ProgressManager progressManager = plugin.getProgressManager();
        PlayerProgress playerProgress = progressManager.getPlayerProgress(player);

        ArrayList<QuestProgress> active = new ArrayList<>(playerProgress.getQuestProgresses().values());
        ArrayList<FinishedQuestInfo> finished = new ArrayList<>(playerProgress.getFinishedQuests().values());

        active.sort(QuestProgress.TIME_COMPARATOR_DESC);
        finished.sort(FinishedQuestInfo.TIME_COMPARATOR);

        String firstPage = formatFirstPage(player, active.size(), finished.size());

        bookMeta.addPage(firstPage);

        ToCEntry objectivePages = formatObjectives(player, active);
        ToCEntry activePages = formatActiveQuests(player, active);
        ToCEntry finishedPages = formatFinishedQuests(player, finished);

        String[] toc = formatTableOfContents(1, player, objectivePages, activePages, finishedPages);

        bookMeta.addPage(toc);

        bookMeta.addPage(objectivePages.pages);
        bookMeta.addPage(activePages.pages);
        bookMeta.addPage(finishedPages.pages);
    }

    public static ItemStack genQuestLog(Player player)
    {
        ItemStack stack = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) Bukkit.getItemFactory().getItemMeta(Material.WRITTEN_BOOK);

        bookMeta.setTitle(CHAT_HELPER.getMessage(player, "quest-log-item-name"));
        fillQuestLog(player, bookMeta);

        stack.setItemMeta(bookMeta);

        return stack;
    }

    public static boolean isQuestLog(ItemStack stack)
    {
        if (stack.getType() != Material.WRITTEN_BOOK) {
            return false;
        }

        ItemMeta meta = stack.getItemMeta();
        if (meta == null || !(meta instanceof BookMeta)) {
            return false;
        }

        BookMeta bookMeta = (BookMeta) meta;
        List<String> pages = bookMeta.getPages();
        if (pages.size() < 1) {
            return false;
        }

        String firstPage = pages.get(0);

        return firstPage.startsWith(MAGIC_STRING);
    }

    public static boolean takeFromPlayer(Player player)
    {
        boolean removed = false;

        PlayerInventory inventory = player.getInventory();

        for (int i = 0; i < inventory.getSize(); ++i)
        {
            ItemStack stack = inventory.getItem(i);
            if (stack != null && isQuestLog(stack))
            {
                inventory.setItem(i, null);
                removed = true;
            }
        }

        return removed;
    }

    public static void giveToPlayer(Player player)
    {
        takeFromPlayer(player);

        HashMap<Integer, ItemStack> rest = player.getInventory().addItem(genQuestLog(player));

        if (!rest.isEmpty()) {
            CHAT_HELPER.sendMessage(player, "quest-log-inventory-full");
        }
    }

    public static void update(Player player)
    {
        if (takeFromPlayer(player)) {
            giveToPlayer(player);
        }
    }

    private static class ToCEntry
    {
        public final String name;
        public final String[] pages;

        public ToCEntry(String name, String[] pages)
        {
            this.name = name;
            this.pages = pages;
        }
    }

    private static final String MAGIC_STRING = "§b §5 §4 §5 §1 §0 §8 §2 §4 §8 "; // if anyone has a better idea on identifying the books, please tell me :)
    private static final int LINE_WIDTH = 20;
    private static final int LINES_PER_PAGE = 14;
}
