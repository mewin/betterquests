/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.input;

import de.mewin.betterquests.BetterQuestsPlugin;

import java.util.Arrays;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversation;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;
import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CONVERSATION_MANAGER;

/**
 *
 * @author mewin
 */
public class CommandBQCallback extends BQCommand
{

    public CommandBQCallback(BetterQuestsPlugin plugin, CommandSender sender, Command command, String label, String[] args)
    {
        super(plugin, sender, command, label, args);
    }

    @Override
    public boolean run()
    {
        if (args.length < 1) {
            return false;
        }

        switch (args[0])
        {
            case "accept":
                return cbAcceptQuest(sender, args);
            case "conversation-send":
                return cbConversationSend(sender, args);
            case "info":
                return cbQuestInfo(sender, args);
            case "show-advert":
                return cbQuestAdvert(sender, args);
        }

        return false;
    }


    private boolean cbAcceptQuest(CommandSender sender, String[] args)
    {
        if (args.length < 2) {
            return false;
        }
        userInputHandler.doAcceptAdvertisement(sender, args[1]);
        return true;
    }

    private boolean cbConversationSend(CommandSender sender, String[] args)
    {
        if (args.length < 2) {
            return false;
        }

        if (!(sender instanceof Player)) {
            return false;
        }

        Conversation conversation = CONVERSATION_MANAGER.getCurrentConversation((Player) sender);

        if (conversation != null) {
            List<String> restArgs = Arrays.asList(args).subList(1, args.length);
            conversation.acceptInput(String.join(" ", restArgs));
        }

        return true;
    }

    private boolean cbQuestInfo(CommandSender sender, String[] args)
    {
        if (!(sender instanceof Player))
        {
            CHAT_HELPER.sendMessage(sender, "cmd-player-only");
            return true;
        }

        if (args.length < 2) {
            return false;
        }

        userInputHandler.doQuestInfo((Player) sender, args[1]);

        return true;
    }

    private boolean cbQuestAdvert(CommandSender sender, String[] args)
    {
        if (!(sender instanceof Player))
        {
            CHAT_HELPER.sendMessage(sender, "cmd-player-only");
            return true;
        }

        if (args.length < 2) {
            return false;
        }

        userInputHandler.doSendAdvertisement((Player) sender, args[1]);

        return true;
    }
}
