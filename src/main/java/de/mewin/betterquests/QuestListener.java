/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import de.mewin.betterquests.input.QuestLog;
import de.mewin.betterquests.quest.event.QuestFailedEvent;
import de.mewin.betterquests.quest.event.QuestFinishedEvent;
import de.mewin.betterquests.quest.event.QuestStartedEvent;
import de.mewin.betterquests.quest.event.QuestUpdateEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author mewin
 */
public class QuestListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public QuestListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        if (event.hasItem() && QuestLog.isQuestLog(event.getItem())) {
            QuestLog.update(event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent event)
    {
        if (QuestLog.isQuestLog(event.getItemDrop().getItemStack())) {
            event.getItemDrop().remove();
        }
    }
}
