/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.factories;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.TriggerFactory;
import de.mewin.betterquests.triggers.PasswordTrigger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PasswordTriggerFactory extends TriggerFactory
{

    @Override
    public Trigger createNew(Quest quest)
    {
        PasswordTrigger passwordTrigger = new PasswordTrigger();
        quest.addTrigger(passwordTrigger);
        return passwordTrigger;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "trigger-password-factory");
    }

    @Override
    public Trigger fromYaml(Quest quest, Map<String, Object> yaml) throws IOException
    {
        String password = "";
        Object tmp;

        if ((tmp = yaml.get("password")) != null) {
            password = tmp.toString();
        }

        return new PasswordTrigger(password);
    }

    @Override
    public Map<String, Object> toYaml(Trigger trigger) throws IOException
    {
        PasswordTrigger passwordTrigger = (PasswordTrigger) trigger;

        HashMap<String, Object> map = new HashMap<>();

        map.put("password", passwordTrigger.getPassword());

        return map;
    }

    public static final String MY_ID = "password";
}
