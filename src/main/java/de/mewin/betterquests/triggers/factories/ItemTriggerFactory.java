/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.factories;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.TriggerFactory;
import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.triggers.ItemTrigger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemTriggerFactory extends TriggerFactory
{
    public static final String MY_ID = "use-item";

    @Override
    public Trigger createNew(Quest quest)
    {
        ItemTrigger trigger = new ItemTrigger();
        quest.addTrigger(trigger);
        return trigger;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "trigger-item-factory");
    }

    @Override
    public Trigger fromYaml(Quest quest, Map<String, Object> yaml) throws IOException
    {
        Object itemsObj = yaml.get("items");
        if (itemsObj == null || !(itemsObj instanceof Map)) {
            return new ItemTrigger();
        }
        Map<String, Object> items = (Map<String, Object>) itemsObj;

        return new ItemTrigger(Serializer.deserializeItemStack(items));
    }

    @Override
    public Map<String, Object> toYaml(Trigger object) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();
        ItemTrigger trigger = (ItemTrigger) object;

        ItemStack items = trigger.getItemStack();

        if (items != null) {
            map.put("items", Serializer.serializeItemStack(items));
        }

        return map;
    }
}
