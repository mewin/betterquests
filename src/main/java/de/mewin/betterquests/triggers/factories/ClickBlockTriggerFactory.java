/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.factories;

import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.TriggerFactory;
import de.mewin.betterquests.triggers.ClickBlockTrigger;
import de.mewin.mewinbukkitlib.world.LocationProxy;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ClickBlockTriggerFactory extends TriggerFactory
{
    public static final String MY_ID = "click-block";

    @Override
    public Trigger createNew(Quest quest)
    {
        ClickBlockTrigger trigger = new ClickBlockTrigger();
        quest.addTrigger(trigger);
        return trigger;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "trigger-click-block-factory");
    }

    @Override
    public Trigger fromYaml(Quest quest, Map<String, Object> yaml) throws IOException
    {
        ItemStack withItem = null;
        Object itemsObj = yaml.get("with-item");
        if (itemsObj instanceof Map)
        {
            Map<String, Object> itemsMap = (Map<String, Object>) itemsObj;
            withItem = Serializer.deserializeItemStack(itemsMap);
        }

        LocationProxy location = new LocationProxy();
        Object locationObj = yaml.get("location");
        if (locationObj instanceof Map)
        {
            Map<String, Object> locationMap = (Map<String, Object>) locationObj;
            location = Serializer.deserializeLocation(locationMap);
        }
        return new ClickBlockTrigger(withItem, location);
    }

    @Override
    public Map<String, Object> toYaml(Trigger object) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();
        ClickBlockTrigger trigger = (ClickBlockTrigger) object;

        ItemStack withItem = trigger.getWithItem();

        if (withItem != null) {
            map.put("with-item", Serializer.serializeItemStack(withItem));
        }

        Location location = trigger.getLocation();
        if (location != null) {
            map.put("location", Serializer.serializeLocation(location));
        }

        return map;
    }
}
