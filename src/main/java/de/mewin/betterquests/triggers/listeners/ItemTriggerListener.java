/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.triggers.ItemTrigger;
import de.mewin.mewinbukkitlib.util.Items;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author mewin
 */
public class ItemTriggerListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public ItemTriggerListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        ItemStack item = event.getItem();
        if (item == null || item.getType() == Material.AIR) {
            return;
        }

        List<Trigger> triggers = plugin.getTriggerRegistry().getRegistered(ItemTrigger.class);

        for (Trigger trigger_ : triggers)
        {
            ItemTrigger trigger = (ItemTrigger) trigger_;
            boolean ignoreMeta = trigger.getSettings().getBoolean(ItemTrigger.IGNORE_META, false);

            if (Items.compareStacks(item, trigger.getItemStack(), ignoreMeta))
            {
                ProgressManager progressManager = plugin.getProgressManager();
                progressManager.advertiseQuest(event.getPlayer(), trigger.getQuest(), trigger);
            }
        }
    }
}
