/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.triggers.ClickBlockTrigger;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author mewin
 */
public class ClickBlockTriggerListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public ClickBlockTriggerListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }
        
        if (!event.hasBlock()) {
            return;
        }

        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        Location location = block.getLocation();

        ObjectsByClassRegistry<Trigger> triggerRegistry = plugin.getTriggerRegistry();
        List<Trigger> triggers = triggerRegistry.getRegistered(ClickBlockTrigger.class);

        for (Trigger trigger_ : triggers)
        {
            ClickBlockTrigger trigger = (ClickBlockTrigger) trigger_;
            Location triggerLocation = trigger.getLocation();
            ItemStack withItem = trigger.getWithItem();

            if (withItem != null &&
                    (!event.hasItem() || !withItem.isSimilar(event.getItem()))) {
                return;
            }

            if (location.getBlockX() == triggerLocation.getBlockX()
                    && location.getBlockY() == triggerLocation.getBlockY()
                    && location.getBlockZ() == triggerLocation.getBlockZ()) {
                plugin.getProgressManager().advertiseQuest(player, trigger.getQuest(), trigger);
                event.setCancelled(true);
            }
        }
    }
}
