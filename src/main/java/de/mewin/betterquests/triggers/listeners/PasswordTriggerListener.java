/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.triggers.PasswordTrigger;
import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author mewin
 */
public class PasswordTriggerListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public PasswordTriggerListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event)
    {
        Player player = event.getPlayer();
        String message = event.getMessage();

        ProgressManager progressManager = plugin.getProgressManager();
        ObjectsByClassRegistry<Trigger> triggerRegistry = plugin.getTriggerRegistry();
        List<Trigger> triggers = triggerRegistry.getRegistered(PasswordTrigger.class);

        for (Trigger trigger : triggers)
        {
            PasswordTrigger passwordTrigger = (PasswordTrigger) trigger;

            if (passwordTrigger.getPassword().equals(message))
            {
                progressManager.advertiseQuest(player, trigger.getQuest(), trigger);
                event.setCancelled(true);
            }
        }
    }
}
