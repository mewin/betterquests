/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.conversation;

import de.mewin.betterquests.conversation.editors.CustomizableEditorPrompt;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.quest.extra.TriggerSettings;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class TriggerPrompt extends CustomizableEditorPrompt<Quest, Trigger>
{
    public TriggerPrompt(Trigger trigger, Quest quest, MenuPrompt parentPrompt)
    {
        super(trigger, quest, parentPrompt, "advanced-trigger");
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-edit-trigger");
    }

    @Override
    protected void doDelete()
    {
        base.removeTrigger(object);
    }

    @Override
    protected String getString(CustomizableEditorPrompt.Strings id)
    {
        switch(id)
        {
            case MENU_DELETE:
                return "menu-delete-trigger";
            default:
                return "<missing string>";
        }
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        return TriggerSettings.getKnownSettingsList();
    }
}