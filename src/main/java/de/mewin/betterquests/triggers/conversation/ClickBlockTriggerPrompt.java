/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.conversation;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.triggers.ClickBlockTrigger;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemMaterialAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.LocationAcceptorPrompt;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ClickBlockTriggerPrompt extends TriggerPrompt
{
    private final ClickBlockTrigger clickBlockTrigger;

    public ClickBlockTriggerPrompt(ClickBlockTrigger trigger, Quest base, MenuPrompt parentPrompt)
    {
        super(trigger, base, parentPrompt);
        this.clickBlockTrigger = trigger;
    }

    @Override
    protected void fillOptions(final CommandSender receiver)
    {
        ItemStack withItem = clickBlockTrigger.getWithItem();
        String materialString;

        if (withItem == null) {
            materialString = CHAT_HELPER.getMessage(receiver, "setting-unset");
        } else {
            materialString = withItem.getType().toString();
        }

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-item-material", materialString),
                        new ItemMaterialAcceptorPrompt(clickBlockTrigger, this));
        if (receiver instanceof Player)
        {
            addComplexOption(CHAT_HELPER.getMessage(receiver, "menu-item-fromhand"),
            new MenuPrompt.PromptSupplier()
            {
                @Override
                public Prompt get()
                {
                    Player player = (Player) receiver;
                    ItemStack stack = player.getInventory().getItemInMainHand();
                    if (stack == null) { // just in case
                        stack = new ItemStack(Material.AIR, 0);
                    }
                    clickBlockTrigger.setWithItem(stack);
                    ClickBlockTriggerPrompt.this.reloadMenu();
                    return ClickBlockTriggerPrompt.this;
                }
            });
        }

        addComplexOption(CHAT_HELPER.getMessage(receiver, "menu-item-reset"),
        new MenuPrompt.PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                clickBlockTrigger.setWithItem(null);
                ClickBlockTriggerPrompt.this.reloadMenu();
                return ClickBlockTriggerPrompt.this;
            }
        });

        Location loc = clickBlockTrigger.getLocation();
        if (loc == null) {
            if (receiver instanceof Player)  {
                loc = ((Player) receiver).getLocation();
            } else {
                loc = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
            }
            clickBlockTrigger.setLocation(loc);
        }
        String msg = CHAT_HELPER.getMessage(receiver, "menu-block", loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());
        addSimpleOption(msg, new LocationAcceptorPrompt(this, "prompt-edit-blocklocation", clickBlockTrigger));

        super.fillOptions(receiver);
    }
}
