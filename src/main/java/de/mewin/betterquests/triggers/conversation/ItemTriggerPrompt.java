/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.conversation;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.triggers.ItemTrigger;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemMaterialAcceptorPrompt;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemTriggerPrompt extends TriggerPrompt
{
    private final ItemTrigger itemTrigger;

    public ItemTriggerPrompt(ItemTrigger trigger, Quest quest, MenuPrompt parentPrompt)
    {
        super(trigger, quest, parentPrompt);

        this.itemTrigger = trigger;
    }

    @Override
    protected void fillOptions(final CommandSender sender)
    {
        ItemStack items = itemTrigger.getItemStack();

        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-material", items.getType().toString()),
                        new ItemMaterialAcceptorPrompt(itemTrigger, this));

        if (sender instanceof Player)
        {
            addComplexOption(CHAT_HELPER.getMessage(sender, "menu-item-fromhand"),
            new MenuPrompt.PromptSupplier()
            {
                @Override
                public Prompt get()
                {
                    Player player = (Player) sender;
                    ItemStack stack = player.getInventory().getItemInMainHand();
                    if (stack == null) { // just in case
                        stack = new ItemStack(Material.AIR, 0);
                    }
                    itemTrigger.setItemStack(stack);
                    ItemTriggerPrompt.this.reloadMenu();
                    return ItemTriggerPrompt.this;
                }
            });
        }

        super.fillOptions(sender);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());
        settings.add(new KnownSetting(ItemTrigger.IGNORE_META, DataType.BOOLEAN));
        return settings;
    }
}
