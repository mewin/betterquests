/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers.conversation;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.triggers.PasswordTrigger;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PasswordTriggerPrompt extends TriggerPrompt
{
    private final PasswordTrigger passwordTrigger;

    public PasswordTriggerPrompt(PasswordTrigger trigger, Quest quest, MenuPrompt parentPrompt)
    {
        super(trigger, quest, parentPrompt);

        this.passwordTrigger = trigger;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-password", passwordTrigger.getPassword()),
                new StringAcceptorPrompt(this, "prompt-password", passwordTrigger));

        super.fillOptions(receiver);
    }
}
