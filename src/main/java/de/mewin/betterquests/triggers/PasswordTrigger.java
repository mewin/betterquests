/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.triggers.conversation.PasswordTriggerPrompt;
import de.mewin.betterquests.triggers.factories.PasswordTriggerFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PasswordTrigger extends Trigger implements StringAcceptor
{
    private String password;

    public PasswordTrigger()
    {
        password = "";
    }

    public PasswordTrigger(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "trigger-password", password);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, Quest base, CommandSender sender)
    {
        return new PasswordTriggerPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return PasswordTriggerFactory.MY_ID;
    }

    @Override
    public boolean setString(String value)
    {
        password = value;
        return true;
    }

    @Override
    public String getString()
    {
        return password;
    }
}
