/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers;

import de.mewin.betterquests.triggers.conversation.ItemTriggerPrompt;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.triggers.factories.ItemTriggerFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemStackAcceptor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemTrigger extends Trigger implements ItemStackAcceptor
{
    private ItemStack items;

    public ItemTrigger()
    {
        items = new ItemStack(Material.AIR);
    }

    public ItemTrigger(ItemStack items)
    {
        this.items = items;
    }

    @Override
    public ItemStack getItemStack()
    {
        return items;
    }

    @Override
    public void setItemStack(ItemStack itemStack)
    {
        items = new ItemStack(itemStack);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "trigger-item");
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, Quest base, CommandSender sender)
    {
        return new ItemTriggerPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return ItemTriggerFactory.MY_ID;
    }

    public static final String IGNORE_META = "ignore-meta";
}
