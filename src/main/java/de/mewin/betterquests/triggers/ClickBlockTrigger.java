/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.triggers;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.triggers.conversation.ClickBlockTriggerPrompt;
import de.mewin.betterquests.triggers.factories.ClickBlockTriggerFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemStackAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.LocationAcceptor;
import de.mewin.mewinbukkitlib.world.LocationProxy;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ClickBlockTrigger extends Trigger implements ItemStackAcceptor, LocationAcceptor
{
    private ItemStack withItem;
    private LocationProxy locationProxy;

    public ClickBlockTrigger()
    {
        this.withItem = null;
        this.locationProxy = new LocationProxy();
    }

    public ClickBlockTrigger(ItemStack withItem, LocationProxy location)
    {
        this.withItem = withItem;
        this.locationProxy = location;
    }

    public ItemStack getWithItem()
    {
        return withItem;
    }

    public void setWithItem(ItemStack withItem)
    {
        this.withItem = withItem;
    }

    @Override
    public Location getLocation()
    {
        return locationProxy.getLocation();
    }

    @Override
    public boolean setLocation(Location location)
    {
        this.locationProxy = new LocationProxy(location);
        return true;
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "trigger-click-block");
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, Quest base, CommandSender sender)
    {
        return new ClickBlockTriggerPrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return ClickBlockTriggerFactory.MY_ID;
    }

    @Override
    public void setItemStack(ItemStack itemStack)
    {
        withItem = itemStack;
    }

    @Override
    public ItemStack getItemStack()
    {
        return withItem;
    }
}
