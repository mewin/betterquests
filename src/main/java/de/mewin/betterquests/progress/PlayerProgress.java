/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.progress;

import de.mewin.betterquests.progress.event.TrackedQuestChangedEvent;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.*;

/**
 *
 * @author mewin
 */
public class PlayerProgress
{
    private final UUID playerUuid;
    private final HashMap<UUID, QuestProgress> questProgresses;
    private final HashMap<UUID, FinishedQuestInfo> finishedQuests;
    private Quest trackedQuest;

    PlayerProgress(UUID playerUuid)
    {
        this.playerUuid = playerUuid;
        this.questProgresses = new HashMap<>();
        this.finishedQuests = new HashMap<>();
    }

    public UUID getPlayerUUID() {
        return playerUuid;
    }

    public HashMap<UUID, QuestProgress> getQuestProgresses()
    {
        return questProgresses;
    }

    public HashMap<UUID, FinishedQuestInfo> getFinishedQuests()
    {
        return finishedQuests;
    }

    public Quest getTrackedQuest()
    {
        return trackedQuest;
    }

    public void setTrackedQuest(Quest quest)
    {
        if (trackedQuest == quest) {
            return;
        }

        Player player = Bukkit.getPlayer(playerUuid);
        if (player != null)
        {
            // TODO: no event when player is offline?
            TrackedQuestChangedEvent event = new TrackedQuestChangedEvent(player, trackedQuest, quest);
            Bukkit.getPluginManager().callEvent(event);
            Bukkit.getPluginManager().callEvent(event);

            if (event.isCancelled()) {
                return;
            }
        }

        trackedQuest = quest;
    }

    public List<QuestProgress> findProgressesByObjectiveType(Class<? extends Objective> clazz)
    {
        ArrayList<QuestProgress> list = new ArrayList<>();

QUEST_PROGRESS_LOOP:
        for (QuestProgress questProgress : questProgresses.values())
        {
            Stage currentStage = questProgress.getCurrentStage();
            for (Objective objective : currentStage.getObjectives())
            {
                if (clazz.equals(objective.getClass()))
                {
                    list.add(questProgress);
                    continue QUEST_PROGRESS_LOOP;
                }
            }
        }

        return list;
    }
}
