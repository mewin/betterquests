package de.mewin.betterquests.progress.event;

import de.mewin.betterquests.quest.Quest;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class TrackedQuestChangedEvent extends PlayerEvent implements Cancellable
{
    private final Quest oldTrackedQuest;
    private Quest newTrackedQuest;
    private boolean cancelled;

    public TrackedQuestChangedEvent(Player who, Quest oldTrackedQuest, Quest newTrackedQuest)
    {
        super(who);
        this.oldTrackedQuest = oldTrackedQuest;
        this.newTrackedQuest = newTrackedQuest;
        cancelled = false;
    }

    public Quest getOldTrackedQuest() {
        return oldTrackedQuest;
    }

    public Quest getNewTrackedQuest() {
        return newTrackedQuest;
    }

    public void setNewTrackedQuest(Quest newTrackedQuest) {
        this.newTrackedQuest = newTrackedQuest;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel)
    {
        cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}
