/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.progress;

import de.mewin.betterquests.quest.Objective;

/**
 *
 * @author mewin
 */
public interface ObjectiveProgressAcceptor
{
    public Object getObjectiveProgress(Objective objective, Object data);
    public boolean setObjectiveProgress(Objective objective, Object progress, Object data);
}
