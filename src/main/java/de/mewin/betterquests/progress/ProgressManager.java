/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.progress;

import de.mewin.betterquests.conditions.AdvertisementCondition;
import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.ObjectsByClassRegistry;
import de.mewin.betterquests.conditions.LocationCondition;
import de.mewin.betterquests.conditions.TimeoutCondition;
import de.mewin.betterquests.io.ProgressManagerBackend;
import de.mewin.betterquests.event.AdvertiseQuestEvent;
import de.mewin.betterquests.quest.*;
import de.mewin.betterquests.quest.event.*;
import de.mewin.betterquests.quest.extra.*;
import de.mewin.betterquests.util.ConfigKeys;
import de.mewin.mewinbukkitlib.util.Pair;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ProgressManager
{
    private final BetterQuestsPlugin plugin;
    private final HashMap<UUID, PlayerProgress> playerProgresses;
    private final HashMap<UUID, QuestAdvertisement> openAdvertisements;
    private final ProgressManagerBackend backend;

    public ProgressManager(BetterQuestsPlugin plugin, ProgressManagerBackend backend)
    {
        this.plugin = plugin;
        this.backend = backend;
        this.playerProgresses = new HashMap<>();
        this.openAdvertisements = new HashMap<>();
    }

    public QuestProgress getQuestProgress(UUID playerUUID, UUID questUUID)
    {
        PlayerProgress playerProgress = getPlayerProgress(playerUUID);

        return playerProgress.getQuestProgresses().get(questUUID);
    }

    public QuestProgress getQuestProgress(OfflinePlayer player, UUID questUUID)
    {
        return ProgressManager.this.getQuestProgress(player.getUniqueId(), questUUID);
    }

    public QuestProgress getQuestProgress(UUID playerUUID, Quest quest)
    {
        return ProgressManager.this.getQuestProgress(playerUUID, quest.getUuid());
    }

    public QuestProgress getQuestProgress(OfflinePlayer player, Quest quest)
    {
        return ProgressManager.this.getQuestProgress(player.getUniqueId(), quest.getUuid());
    }

    public void setQuestProgress(UUID playerUUID, QuestProgress progress)
    {
        UUID questUUID = progress.getQuest().getUuid();

        PlayerProgress playerProgress = getPlayerProgress(playerUUID);
        playerProgress.getQuestProgresses().put(questUUID, progress);
    }

    public void setQuestProgress(OfflinePlayer player, QuestProgress progress)
    {
        setQuestProgress(player.getUniqueId(), progress);
    }

    public Map<UUID, PlayerProgress> getPlayerProgresses()
    {
        return Collections.unmodifiableMap(playerProgresses);
    }

    public FinishedQuestInfo getFinishedQuest(UUID playerUUID, UUID questUUID)
    {
        PlayerProgress playerProgress = getPlayerProgress(playerUUID);

        return playerProgress.getFinishedQuests().get(questUUID);
    }

    public FinishedQuestInfo getFinishedQuest(UUID playerUUID, Quest quest)
    {
        return getFinishedQuest(playerUUID, quest.getUuid());
    }

    public FinishedQuestInfo getFinishedQuest(OfflinePlayer player, UUID questUUID)
    {
        return getFinishedQuest(player.getUniqueId(), questUUID);
    }

    public FinishedQuestInfo getFinishedQuest(OfflinePlayer player, Quest quest)
    {
        return getFinishedQuest(player.getUniqueId(), quest.getUuid());
    }

    public void addFinishedQuest(OfflinePlayer player, Quest quest, boolean failed, Date time)
    {
        PlayerProgress playerProgress = getPlayerProgress(player.getUniqueId());

        HashMap<UUID, FinishedQuestInfo> finishedQuests = playerProgress.getFinishedQuests();
        FinishedQuestInfo info = finishedQuests.get(quest.getUuid());
        if (info == null)
        {
            info = new FinishedQuestInfo(quest, failed, time);
            finishedQuests.put(quest.getUuid(), info);
        }
        else
        {
            info.setLastFailed(failed);
            info.setTime(time);
            if (failed) {
                info.setNumFailed(info.getNumFailed() + 1);
            } else {
                info.setNumSuccess(info.getNumSuccess() + 1);
            }
        }
    }

    public void addFinishedQuest(OfflinePlayer player, Quest quest, boolean failed)
    {
        addFinishedQuest(player, quest, failed, new Date());
    }

    public boolean removeFinishedQuest(OfflinePlayer player, Quest quest)
    {
        PlayerProgress playerProgress = getPlayerProgress(player.getUniqueId());

        return playerProgress.getFinishedQuests().remove(quest.getUuid()) != null;
    }

    public FinishedQuestInfo getFinishedQuestInfo(OfflinePlayer player, Quest quest)
    {
        PlayerProgress playerProgress = getPlayerProgress(player.getUniqueId());

        return playerProgress.getFinishedQuests().get(quest.getUuid());
    }

    public void setFinishedQuestInfo(UUID playerid, FinishedQuestInfo finishedQuestInfo)
    {
        PlayerProgress playerProgress = getPlayerProgress(playerid);

        playerProgress.getFinishedQuests().put(finishedQuestInfo.getQuest().getUuid(), finishedQuestInfo);
    }

    public void setFinishedQuestInfo(OfflinePlayer player, FinishedQuestInfo finishedQuestInfo)
    {
        setFinishedQuestInfo(player.getUniqueId(), finishedQuestInfo);
    }

    public PlayerProgress getPlayerProgress(UUID playerid)
    {
        PlayerProgress playerProgress = playerProgresses.get(playerid);

        if (playerProgress == null)
        {
            playerProgress = new PlayerProgress(playerid);
            playerProgresses.put(playerid, playerProgress);
        }

        return playerProgress;
    }

    public PlayerProgress getPlayerProgress(OfflinePlayer player)
    {
        return getPlayerProgress(player.getUniqueId());
    }

    private <T extends Objective> void addProgressByObjectiveType(ArrayList<Pair<QuestProgress, T>> result,
            QuestProgress progress, ObjectivesHolder holder, Class<T> clazz)
    {

        for (Objective objective : holder.getObjectives())
        {
            if (clazz.isAssignableFrom(objective.getClass())) {
                result.add(new Pair<>(progress, clazz.cast(objective)));
            }
            if (objective instanceof ObjectivesHolder) {
                addProgressByObjectiveType(result, progress, (ObjectivesHolder) objective, clazz);
            }
        }
    }

    public <T extends Objective> List<Pair<QuestProgress, T>> findProgressByObjectiveType(OfflinePlayer player, Class<T> clazz)
    {
        ArrayList<Pair<QuestProgress, T>> result = new ArrayList<>();
        PlayerProgress playerProgress = getPlayerProgress(player.getUniqueId());

        for (Map.Entry<UUID, QuestProgress> entry : playerProgress.getQuestProgresses().entrySet())
        {
            QuestProgress progress = entry.getValue();
            Stage currentStage = progress.getCurrentStage();

            addProgressByObjectiveType(result, progress, currentStage, clazz);
        }

        return result;
    }

    public void saveProgress() throws IOException
    {
        backend.saveProgress(this);
    }

    public void loadProgress() throws IOException
    {
        backend.loadProgress(this);
    }

    public List<Requirement> testRequirements(Player player, RequirementsHolder holder)
    {
        ArrayList<Requirement> missing = new ArrayList<>();

        for (Requirement requirement : holder.getRequirements())
        {
            if (!requirement.testRequirement(player)) {
                missing.add(requirement);
            }
        }

        return missing;
    }

    public boolean processRequirements(Player player, RequirementsHolder holder, boolean print)
    {
        List<Requirement> missingRequirements = testRequirements(player, holder);

        if (!missingRequirements.isEmpty())
        {
            if (!print) {
                return false;
            }

            for (Requirement requirement : missingRequirements)
            {
                if (requirement.getSettings().getBoolean(RequirementSettings.SILENT, false)) {
                    return false;
                }
            }

            for (Requirement requirement : missingRequirements) {
                player.sendMessage(requirement.getFailMessage(player));
            }

            return false;
        }

        return true;
    }

    public void applyRequirements(Player player, RequirementsHolder holder)
    {
        for (Requirement requirement : holder.getRequirements()) {
            requirement.onApply(player);
        }
    }

    private boolean testQuestDelay(int delay, FinishedQuestInfo finishedQuestInfo)
    {
        if (delay < 0) {
            return false;
        }

        return finishedQuestInfo.getTime().getTime() + 1000 * delay <= new Date().getTime();
    }

    public boolean canStartQuest(Player player, Quest quest, boolean print)
    {
        if (!isQuestAvailable(player, quest)) {
            return false;
        }

        String customMessage = quest.getSettings().getString(QuestSettings.REQUIREMENT_MISSING_MESSAGE, null);

        boolean result = processRequirements(player, quest, print && customMessage == null);

        if (!result && print && customMessage != null) {
            CHAT_HELPER.sendUserMessage(player, customMessage);
        }

        return result;
    }

    public boolean canStartQuest(Player player, Quest quest)
    {
        return canStartQuest(player, quest, true);
    }

    public boolean isQuestAvailable(Player player, Quest quest)
    {
        if (getQuestProgress(player, quest) != null) {
            return false;
        }

        FinishedQuestInfo finishedQuestInfo = getFinishedQuest(player, quest);
        if (finishedQuestInfo != null)
        {
            int delay;

            if (finishedQuestInfo.isLastFailed()) {
                delay = quest.getSettings().getInt(QuestSettings.RETRY_DELAY, -1);
            } else {
                delay = quest.getSettings().getInt(QuestSettings.REDO_DELAY, -1);
            }

            if (!testQuestDelay(delay, finishedQuestInfo)) {
                return false;
            }
        }

        return true;
    }

    public boolean isQuestSilenced(Player player, Quest quest)
    {
        List<Requirement> missingRequirements = testRequirements(player, quest);

        for (Requirement requirement : missingRequirements)
        {
            if (requirement.getSettings().getBoolean(RequirementSettings.SILENT, false)) {
                return true;
            }
        }

        return false;
    }

    public void advertiseQuest(Player player, Quest quest, Trigger byTrigger)
    {
        if (byTrigger != null && byTrigger.getSettings().getBoolean(TriggerSettings.AUTO_START, false))
        {
            if (!canStartQuest(player, quest)) {
                return;
            }
            sendQuestDescription(player, quest);
            startQuest(player, quest);
            return;
        }

        if (!isQuestAvailable(player, quest)) {
            return;
        }

        if (isQuestSilenced(player, quest)) {
            return;
        }

        QuestAdvertisement advertisement = new QuestAdvertisement(player, quest, byTrigger);

        int timeoutSeconds = plugin.getConfig().getInt(ConfigKeys.ADVERTISEMENT_TIMEOUT, 600);
        if (timeoutSeconds > 0)
        {
            long timeoutTime = System.currentTimeMillis() + (timeoutSeconds * 1000);
            advertisement.addCondition(new TimeoutCondition(timeoutTime));
        }

        double maxDistance = plugin.getConfig().getDouble(ConfigKeys.ADVERTISEMENT_MAX_DISTANCE, 20.0);
        if (maxDistance > 0.0 && byTrigger != null)
        {
            Location triggerLocation = byTrigger.getLocation();

            if (triggerLocation != null) {
                advertisement.addCondition(new LocationCondition(triggerLocation, maxDistance));
            }
        }

        openAdvertisements.put(advertisement.getUuid(), advertisement);
    }

    public void advertiseQuest(Player player, Quest quest)
    {
        advertiseQuest(player, quest, null);
    }

    public void processAdvertisements()
    {
        HashMap<Player, ArrayList<QuestAdvertisement>> toSend = new HashMap<>();

        for (QuestAdvertisement advertisement : openAdvertisements.values())
        {
            if (advertisement.isPresented()) {
                continue;
            }

            Player player = advertisement.getPlayer();
            if (!toSend.containsKey(player)) {
                toSend.put(player, new ArrayList<QuestAdvertisement>());
            }

            toSend.get(player).add(advertisement);
            advertisement.setPresented(true);
        }

        for (Map.Entry<Player, ArrayList<QuestAdvertisement>> entry : toSend.entrySet())
        {
            ArrayList<QuestAdvertisement> advertisements = entry.getValue();

            advertisements.sort(QuestAdvertisement.ID_COMPARATOR);

            if (advertisements.size() > 1) {
                sendMultiAdvertisement(advertisements);
            } else {
                sendAdvertisement(advertisements.get(0));
            }
        }
    }

    public void sendMultiAdvertisement(List<QuestAdvertisement> advertisements)
    {
        if (advertisements.isEmpty()) {
            return;
        }

        Player player = advertisements.get(0).getPlayer();

        CHAT_HELPER.sendMessage(player, "quest-multi-advertise");

        for (QuestAdvertisement advertisement : advertisements)
        {
            Quest quest = advertisement.getQuest();

            String title = CHAT_HELPER.formatPlayerString(quest.getTitle(), player);
            String msgId = "quest-advertise-list-entry";
            if (!canStartQuest(player, quest, false)) {
                msgId = "quest-advertise-list-entry-disabled";
            }

            RunCommandEvent advertiseQuestEvent = new RunCommandEvent("/bq_callback show-advert " + advertisement.getUuid().toString());
            ShowTextEvent hoverEvent = new ShowTextEvent(CHAT_HELPER.getMessage(player, "quest-advertise-hover"));
            JSONText[] texts =
                    new JSONText.MultiBuilder()
                    .setText(CHAT_HELPER.getMessage(player, msgId, title))
                    .setClickEvent(advertiseQuestEvent)
                    .setHoverEvent(hoverEvent)
                    .build();
            CHAT_HELPER.sendJSONMessage(player, texts);
        }
    }

    public void sendQuestDescription(Player player, Quest quest)
    {
        if (!quest.getSettings().getBoolean(QuestSettings.PRINT_DESCRIPTION, true)) {
            return;
        }

        CHAT_HELPER.sendUserMessage(player, quest.getDescription());
    }

    public void sendAdvertisement(QuestAdvertisement advertisement)
    {
        Quest quest = advertisement.getQuest();
        Player player = advertisement.getPlayer();
        Trigger trigger = advertisement.getTrigger();

        AdvertiseQuestEvent event = new AdvertiseQuestEvent(advertisement, player, trigger);
        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled()) {
            return;
        }

        if (!canStartQuest(player, quest)) {
            return;
        }

        sendQuestDescription(player, quest);

        RunCommandEvent acceptQuestEvent = new RunCommandEvent("/bq_callback accept " + advertisement.getUuid().toString());
        ShowTextEvent hoverEvent = new ShowTextEvent(CHAT_HELPER.getMessage(player, "quest-accept-tooltip"));
        JSONText[] texts =
                new JSONText.MultiBuilder()
                .setText(CHAT_HELPER.getMessage(player, "quest-accept-prefix"))
                .next()
                .setText(CHAT_HELPER.getMessage(player, "quest-accept"))
                .setClickEvent(acceptQuestEvent)
                .setHoverEvent(hoverEvent)
                .next()
                .setText(CHAT_HELPER.getMessage(player, "quest-accept-suffix"))
                .build();

        CHAT_HELPER.sendJSONMessage(player, texts);
    }

    public void sendAdvertisement(UUID uuid)
    {
        QuestAdvertisement advertisement = openAdvertisements.get(uuid);

        if (advertisement != null) {
            sendAdvertisement(advertisement);
        }
    }

    public void removeAdvertisements(Player player, Quest quest)
    {
        HashSet<UUID> toRemove = new HashSet<>();

        for (QuestAdvertisement advertisement : openAdvertisements.values())
        {
            if (advertisement.getPlayer().equals(player)
                    && advertisement.getQuest().equals(quest)) {
                toRemove.add(advertisement.getUuid());
            }
        }

        for (UUID uuid : toRemove) {
            openAdvertisements.remove(uuid);
        }
    }

    public void acceptAdvertisement(QuestAdvertisement advertisement)
    {
        List<AdvertisementCondition> conditions = advertisement.getConditions();
        Player player = advertisement.getPlayer();
        Quest quest = advertisement.getQuest();

        for (AdvertisementCondition condition : conditions)
        {
            if (!condition.test(player))
            {
                player.sendMessage(condition.getMessage(player));
                removeAdvertisements(player, quest);
                return;
            }
        }

        startQuest(player, quest);
    }

    public void acceptAdvertisement(UUID uuid)
    {
        QuestAdvertisement advertisement = openAdvertisements.get(uuid);

        if (advertisement != null) {
            acceptAdvertisement(advertisement);
        }
    }

    public void initStage(Player player, QuestProgress progress)
    {
        progress.resetObjectiveProgress();

        Stage stage = progress.getCurrentStage();

        for (Objective objective : stage.getObjectives()) {
            progress.setObjectiveProgress(objective, objective.initProgress(player));
        }
    }

    private void setQuestFinished(Player player, QuestProgress progress, boolean failed)
    {
        abortQuest(player, progress.getQuest());
        abortQuest(player, progress.getQuest());
        addFinishedQuest(player, progress.getQuest(), failed);
    }

    public void finishQuest(Player player, QuestProgress progress)
    {
        setQuestFinished(player, progress, false);

        QuestFinishedEvent event = new QuestFinishedEvent(player, progress);

        Bukkit.getPluginManager().callEvent(event);

        if (event.isSilenced()) {
            return;
        }

        Quest quest = progress.getQuest();

        String message;
        if ((message = quest.getSettings().getString(QuestSettings.FINISH_MESSAGE, null)) != null) {
            CHAT_HELPER.sendUserMessage(player, message);
        }
        else {
            CHAT_HELPER.sendMessage(player, "quest-finished", quest.getTitle());
        }
    }

    public void failQuest(Player player, QuestProgress progress)
    {
        QuestFailEvent event = new QuestFailEvent(player, progress);

        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled()) {
            return;
        }

        setQuestFinished(player, progress, true);

        if (event.isSilenced()) {
            return;
        }

        Quest quest = progress.getQuest();

        String message;
        if ((message = progress.getCurrentStage().getSettings().getString(StageSettings.FAIL_MESSAGE, null)) != null) {
            CHAT_HELPER.sendUserMessage(player, message);
        }
        else if ((message = quest.getSettings().getString(QuestSettings.FAIL_MESSAGE, null)) != null) {
            CHAT_HELPER.sendUserMessage(player, message);
        }
        else {
            CHAT_HELPER.sendMessage(player, "quest-failed", quest.getTitle());
        }

        Bukkit.getPluginManager().callEvent(new QuestFailedEvent(player, progress));
    }

    public void finishQuest(Player player, Quest quest)
    {
        QuestProgress progress;
        if ((progress = getQuestProgress(player, quest)) != null) {
            finishQuest(player, progress);
        }
    }

    public void failQuest(Player player, Quest quest)
    {
        QuestProgress progress;
        if ((progress = getQuestProgress(player, quest)) != null) {
            failQuest(player, progress);
        }
    }

    public boolean abortQuest(OfflinePlayer player, Quest quest)
    {
        PlayerProgress playerProgress = getPlayerProgress(player.getUniqueId());

        if (playerProgress.getTrackedQuest() == quest) {
            playerProgress.setTrackedQuest(null);
        }
        return playerProgress.getQuestProgresses().remove(quest.getUuid()) != null;
    }

    public void resetQuest(Player player, Quest quest)
    {
        QuestProgress questProgress = new QuestProgress(quest, null, new Date());
        setQuestProgress(player, questProgress);
        progressStage(player, questProgress);
    }

    public void finishStage(Player player, QuestProgress progress, boolean printMessages)
    {
        Stage stage = progress.getCurrentStage();

        // notify objectives
        for (Objective objective : stage.getObjectives()) {
            objective.onStageEnd(player, progress.getObjectiveProgress(objective));
        }

        // give rewards
        for (Reward reward : stage.getRewards())
        {
            if (printMessages) {
                String msg = reward.getMessage(player);
                if (msg != null && !"".equals(msg)) {
                    player.sendMessage(msg);
                }
            }
            reward.giveReward(player);
        }
    }

    public void progressStage(Player player, QuestProgress progress)
    {
        Quest quest = progress.getQuest();
        Stage currentStage = progress.getCurrentStage();
        Stage nextStage = quest.getNextStage(currentStage);

        if (currentStage != null)
        {
            String finishMessage = currentStage.getSettings().getString(StageSettings.FINISH_MESSAGE, null);
            if (finishMessage != null) {
                CHAT_HELPER.sendUserMessage(player, finishMessage);
            }
            finishStage(player, progress, finishMessage == null);
        }

        Bukkit.getPluginManager().callEvent(new StageChangeEvent(player, progress, currentStage, nextStage));

        if (nextStage != null)
        {
            String startMessage = nextStage.getSettings().getString(StageSettings.START_MESSAGE, null);
            if (startMessage != null) {
                CHAT_HELPER.sendUserMessage(player, startMessage);
            }
        }
        else
        {
            finishQuest(player, progress);
            return;
        }

        progress.setCurrentStage(nextStage);
        initStage(player, progress);
        sendObjectives(player, progress);

        if (testStageFinished(player, progress, nextStage)) {
            progressStage(player, progress);
        }
    }

    public void sendObjectives(Player player, QuestProgress progress)
    {
        String objectivesMessage = formatObjectiveMessage(player, progress); // TODO: create listener (?)

        if (objectivesMessage != null && !"".equals(objectivesMessage))
        {
            CHAT_HELPER.sendMessage(player, "quest-current-objectives");
            CHAT_HELPER.sendUserMessage(player, objectivesMessage);
        }
    }

    public boolean startQuest(Player player, Quest quest, boolean force)
    {
        if (getQuestProgress(player, quest) != null)
        {
            // TODO: dont send message if quest has been auto-started
            CHAT_HELPER.sendMessage(player, "quest-already-started");
            return false;
        }

        if (!force && !canStartQuest(player, quest, true)) {
            return false;
        }

        removeAdvertisements(player, quest);

        applyRequirements(player, quest);

        List<Stage> stages = quest.getStages();
        if (stages.isEmpty())
        {
            CHAT_HELPER.sendMessage(player, "empty-quest", quest.getId());
            plugin.getLogger().log(Level.WARNING, "Attempt to start empty quest {0}", quest.getId());
            return false;
        }

        QuestProgress questProgress = new QuestProgress(quest, null, new Date());

        QuestStartEvent event = new QuestStartEvent(player, questProgress);
        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled()) {
            return false;
        }

        String startMessage = quest.getSettings().getString(QuestSettings.START_MESSAGE, null);
        if (startMessage == null) {
            startMessage = CHAT_HELPER.getMessage(player, "quest-started");
        }
        CHAT_HELPER.sendUserMessage(player, startMessage);

        setQuestProgress(player, questProgress);
        progressStage(player, questProgress);

        Bukkit.getPluginManager().callEvent(new QuestStartedEvent(player, questProgress));

        if (plugin.getConfig().getBoolean(ConfigKeys.TRACK_ON_START, true))
        {
            PlayerProgress playerProgress = getPlayerProgress(player);
            playerProgress.setTrackedQuest(quest);
        }

        return true;
    }

    public boolean startQuest(Player player, Quest quest)
    {
        return startQuest(player, quest, false);
    }

    public boolean testStageFinished(Player player, QuestProgress progress, Stage stage)
    {
        boolean result = true;
        for (Objective objective : stage.getObjectives())
        {
            if (!objective.testObjective(player, progress.getObjectiveProgress(objective))) {
                result = false;
            }
        }

        return result;
    }

    public void updateQuestProgress(Player player, QuestProgress progress)
    {
        Stage currentStage = progress.getCurrentStage();

        Bukkit.getPluginManager().callEvent(new QuestUpdateEvent(player, progress));

        if (plugin.getConfig().getBoolean(ConfigKeys.LOG_FULL, false)) {
            plugin.getLogger().log(Level.INFO, "Updating quest \"{0}\" for player \"{1}\"",
                    new Object[]{progress.getQuest().getId(), player.getName()});
        }

        if (testStageFinished(player, progress, currentStage)) {
            progressStage(player, progress);
        }

        progress.setLastUpdate(System.currentTimeMillis());
    }

    public void updateQuestProgress(Player player, Quest quest)
    {
        QuestProgress progress = getQuestProgress(player, quest);
        if (progress != null) {
            updateQuestProgress(player, progress);
        }
    }

    public void updateQuestProgressByObjectiveType(Player player, Class<? extends Objective> clazz)
    {
        PlayerProgress playerProgress = getPlayerProgress(player.getUniqueId());

        ObjectsByClassRegistry<Objective> objectiveRegistry = plugin.getObjectiveRegistry();
        List<Objective> objectives = objectiveRegistry.getRegistered(clazz);
        HashSet<Quest> quests = new HashSet<>();

        for (Objective objective : objectives) {
            quests.add(objective.getHolder().getStage().getQuest());
        }

        for (Quest quest : quests)
        {
            QuestProgress progress = playerProgress.getQuestProgresses().get(quest.getUuid());

            // TODO: check stage ?
            if (progress != null) {
                updateQuestProgress(player, quest);
            }
        }
    }

    public String formatObjectiveMessage(Player player, QuestProgress progress)
    {
        Stage stage = progress.getCurrentStage();
        String msg;

        if ((msg = stage.getSettings().getString(StageSettings.OBJECTIVES_TEXT, null)) != null) {
            return CHAT_HELPER.formatPlayerString(msg, player);
        }

        ArrayList<String> objectives =  new ArrayList<>();

        for (Objective objective : stage.getObjectives())
        {
            if ((msg = objective.getSettings().getString(ObjectiveSettings.DESCRIPTION, null)) != null) {
                objectives.add(msg);
            } else {
                objectives.add(objective.getDescription(player, progress.getObjectiveProgress(objective)));
            }
        }

        return String.join("\n", objectives);
    }
}
