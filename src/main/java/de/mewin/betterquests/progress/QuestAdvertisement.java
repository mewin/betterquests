/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.progress;

import de.mewin.betterquests.conditions.AdvertisementCondition;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public class QuestAdvertisement
{
    private final UUID uuid;
    private final Player player;
    private final Quest quest;
    private final Trigger trigger;
    private final ArrayList<AdvertisementCondition> conditions;
    private boolean presented;

    public QuestAdvertisement(Player player, Quest quest, Trigger trigger)
    {
        this.uuid = UUID.randomUUID();
        this.player = player;
        this.quest = quest;
        this.trigger = trigger;
        this.presented = false;
        this.conditions = new ArrayList<>();

        if (trigger != null) {
            this.conditions.addAll(trigger.getConditions());
        }
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public Player getPlayer()
    {
        return player;
    }

    public Quest getQuest()
    {
        return quest;
    }

    public Trigger getTrigger()
    {
        return trigger;
    }

    public List<AdvertisementCondition> getConditions()
    {
        return Collections.unmodifiableList(conditions);
    }

    public boolean isPresented()
    {
        return presented;
    }

    public void addCondition(AdvertisementCondition condition)
    {
        conditions.add(condition);
    }

    public void removeCondition(int index)
    {
        conditions.remove(index);
    }

    public void removeCondition(AdvertisementCondition condition)
    {
        conditions.remove(condition);
    }

    public void setPresented(boolean presented)
    {
        this.presented = presented;
    }

    public static final Comparator<QuestAdvertisement> ID_COMPARATOR = new Comparator<QuestAdvertisement>()
    {
        @Override
        public int compare(QuestAdvertisement a1, QuestAdvertisement a2)
        {
            return a1.getQuest().getId().compareTo(a2.getQuest().getId());
        }
    };
}
