/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.progress;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author mewin
 */
public class QuestProgress
{
    private final Quest quest;
    private final HashMap<UUID, Object> objectiveProgress;
    private Stage currentStage;
    private Date timeStarted;
    private long lastUpdate;

    public QuestProgress(Quest quest, Stage currentStage, Date time)
    {
        this.objectiveProgress = new HashMap<>();
        this.quest = quest;
        this.currentStage = currentStage;
        this.timeStarted = new Date(time.getTime());
        this.lastUpdate = System.currentTimeMillis();
    }

    public Stage getCurrentStage()
    {
        return currentStage;
    }

    public void setCurrentStage(Stage currentStage)
    {
        this.currentStage = currentStage;
    }

    public Quest getQuest()
    {
        return quest;
    }

    public Date getTimeStarted()
    {
        return new Date(timeStarted.getTime());
    }

    public long getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(long time)
    {
        this.lastUpdate = time;
    }

    public Object getObjectiveProgress(Objective objective)
    {
        Object tmp;

        for (Objective stageObjective : currentStage.getObjectives())
        {
            if (stageObjective instanceof ObjectiveProgressAcceptor
                    && (tmp = ((ObjectiveProgressAcceptor) stageObjective).getObjectiveProgress(objective, objectiveProgress.get(stageObjective.getUuid()))) != null) {
                return tmp;
            }
        }

        if (!currentStage.getObjectives().contains(objective)) {
            return null;
        }

        return objectiveProgress.get(objective.getUuid());
    }

    public void setObjectiveProgress(Objective objective, Object progress)
    {
        for (Objective stageObjective : currentStage.getObjectives())
        {
            if (stageObjective instanceof ObjectiveProgressAcceptor
                    && ((ObjectiveProgressAcceptor) stageObjective).setObjectiveProgress(objective, progress, objectiveProgress.get(stageObjective.getUuid()))) {
                return;
            }
        }

        if (!currentStage.getObjectives().contains(objective)) {
            return;
        }

        if (progress == null) {
            objectiveProgress.remove(objective.getUuid());
        } else {
            objectiveProgress.put(objective.getUuid(), progress);
        }
    }

    public void resetObjectiveProgress()
    {
        objectiveProgress.clear();
    }

    public Map<UUID, Object> getObjectivesProgress()
    {
        return Collections.unmodifiableMap(objectiveProgress);
    }

    public static final Comparator<QuestProgress> TIME_COMPARATOR_DESC = new Comparator<QuestProgress>()
    {
        @Override
        public int compare(QuestProgress p1, QuestProgress p2)
        {
            return p2.timeStarted.compareTo(p1.timeStarted);
        }
    };
}
