/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.progress;

import de.mewin.betterquests.quest.Quest;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author mewin
 */
public class FinishedQuestInfo
{
    private final Quest quest;
    private boolean lastFailed;
    private Date time;
    private int numFailed;
    private int numSuccess;

    public FinishedQuestInfo(Quest quest, boolean failed, Date time)
    {
        this.quest = quest;
        this.lastFailed = failed;
        this.time = time;
        this.numFailed = failed ? 1 : 0;
        this.numSuccess = failed ? 0 : 1;
    }

    public FinishedQuestInfo(Quest quest, boolean lastFailed, Date time, int numFailed, int numSuccess)
    {
        this.quest = quest;
        this.lastFailed = lastFailed;
        this.time = time;
        this.numFailed = numFailed;
        this.numSuccess = numSuccess;
    }

    public Quest getQuest()
    {
        return quest;
    }

    public Date getTime()
    {
        return new Date(time.getTime());
    }

    public void setTime(Date time)
    {
        this.time = new Date(time.getTime());
    }

    public boolean isLastFailed()
    {
        return lastFailed;
    }

    public void setLastFailed(boolean lastFailed)
    {
        this.lastFailed = lastFailed;
    }

    public int getNumFailed()
    {
        return numFailed;
    }

    public void setNumFailed(int numFailed)
    {
        this.numFailed = numFailed;
    }

    public int getNumSuccess()
    {
        return numSuccess;
    }

    public void setNumSuccess(int numSuccess)
    {
        this.numSuccess = numSuccess;
    }

    public static final Comparator<FinishedQuestInfo> TIME_COMPARATOR = new Comparator<FinishedQuestInfo>()
    {
        @Override
        public int compare(FinishedQuestInfo f1, FinishedQuestInfo f2)
        {
            return f1.time.compareTo(f2.getTime());
        }
    };
}
