/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util.nbt;

/**
 *
 * @author mewin
 */
public abstract class NBTValue
{
    public abstract String toNBTString();
}
