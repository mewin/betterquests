/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util.nbt;

/**
 *
 * @author mewin
 */
public class NBTString extends NBTValue
{
    private final String value;

    public NBTString(String value)
    {
        this.value = value;
    }

    @Override
    public String toNBTString()
    {
        return "\"" + value.replace("\"", "\\\"") + "\"";
    }
}
