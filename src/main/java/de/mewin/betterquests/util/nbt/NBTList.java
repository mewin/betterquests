/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util.nbt;

import java.util.ArrayList;

/**
 *
 * @author mewin
 */
public class NBTList extends NBTValue
{
    private final ArrayList<NBTValue> values;

    public NBTList()
    {
        this.values = new ArrayList<>();
    }

    public void addValue(NBTValue value)
    {
        values.add(value);
    }

    @Override
    public String toNBTString()
    {
        ArrayList<String> strings = new ArrayList<>(values.size());

        for (NBTValue value : values) {
            strings.add(value.toNBTString());
        }

        return "[" + String.join(",", strings) + "]";
    }
}
