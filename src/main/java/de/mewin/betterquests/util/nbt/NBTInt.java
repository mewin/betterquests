/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util.nbt;

/**
 *
 * @author mewin
 */
public class NBTInt extends NBTValue
{
    private final int value;

    public NBTInt(int value)
    {
        this.value = value;
    }

    @Override
    public String toNBTString()
    {
        return String.valueOf(value);
    }
}
