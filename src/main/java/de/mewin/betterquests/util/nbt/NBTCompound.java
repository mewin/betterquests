/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util.nbt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mewin
 */
public class NBTCompound extends NBTValue
{
    private final HashMap<String, NBTValue> values;

    public NBTCompound()
    {
        this.values = new HashMap<>();
    }

    public void addValue(String key, NBTValue value)
    {
        values.put(key, value);
    }

    public void add(String key, String value)
    {
        addValue(key, new NBTString(value));
    }

    public void add(String key, int value)
    {
        addValue(key, new NBTInt(value));
    }

    @Override
    public String toNBTString()
    {
        ArrayList<String> strings = new ArrayList<>(values.size());

        for (Map.Entry<String, NBTValue> entry : values.entrySet()) {
            strings.add(entry.getKey() + ":" + entry.getValue().toNBTString());
        }

        return "{" + String.join(",", strings) + "}";
    }
}
