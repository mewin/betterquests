/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util;

/**
 *
 * @author mewin
 */
public class TextUtil
{
    public static String repeat(String str, int amount)
    {
        if (amount < 1) {
            return "";
        }

        return String.format("%0" + amount + "d", 0).replace("0", str);
    }
    public static String centerString(String str, int width)
    {
        if (str.length() >= width) {
            return str;
        }

        int spaces = width - str.length();
        int spacesLeft = spaces / 2;


        return repeat(" ", spacesLeft) + str;
    }
    public static int formatStringPos(String str, int pos)
    {
        int realPos = 0;

        for (int i = 0; i < pos; ++realPos)
        {
            if (str.charAt(realPos) != '§' || realPos == str.length() - 1) { // ignore last '&'
                ++i;
            }
            else {
                ++realPos;
            }
        }

        return realPos;
    }

    public static int formatStringLength(String str)
    {
        int len = str.length();
        int count = len;

        for (int i = 0; i < len - 1; ++i)
        {
            if (str.charAt(i) == '§')
            {
                count -= 2;
                ++i;
            }
        }

        return count;
    }

    public static String formatSubString(String str, int start, int end)
    {
        int realStart = formatStringPos(str, start);
        int realEnd = formatStringPos(str, end);

        return str.substring(realStart, realEnd);
    }

    public static String formatSubString(String str, int start)
    {
        return formatSubString(str, start, formatStringLength(str));
    }
}
