/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util;

import de.mewin.betterquests.BetterQuestsPlugin;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

/**
 *
 * @author mewin
 */
public class Util
{
    public static <TKey extends Comparable<? super TKey>, TValue> List<TValue> getSortedList(Map<TKey, TValue> map)
    {
        ArrayList<TKey> keys = new ArrayList<>(map.keySet());

        Collections.sort(keys);

        ArrayList<TValue> values = new ArrayList<>(keys.size());

        for (int i = 0; i < keys.size(); ++i)
        {
            values.add(map.get(keys.get(i)));
        }

        return values;
    }

    public static <T extends Comparable<T>> T limit(T value, T min, T max)
    {
        if (value.compareTo(min) < 0) {
            return min;
        }

        if (value.compareTo(max) > 0) {
            return max;
        }

        return value;
    }

    public static void setPlayerMeta(Player player, String key, Object value)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();

        player.setMetadata(key, new FixedMetadataValue(plugin, value));
    }

    public static MetadataValue getPlayerMeta(Player player, String key)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();

        List<MetadataValue> metadata = player.getMetadata(key);

        for (MetadataValue value : metadata) {
            if (value.getOwningPlugin() == plugin) {
                return value;
            }
        }

        return null;
    }
}
