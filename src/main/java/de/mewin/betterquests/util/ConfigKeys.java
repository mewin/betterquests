/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util;

/**
 *
 * @author mewin
 */
public class ConfigKeys
{
    public static final String ADVERTISEMENT_TIMEOUT = "advertisement-timeout";
    public static final String ADVERTISEMENT_MAX_DISTANCE = "advertisement-max-distance";
    public static final String ENABLE_NETWORK = "enable-network";
    public static final String LOG_FULL = "log-full";
    public static final String QUESTS_PER_PAGE = "quests-per-page";
    public static final String DEFAULT_LOCALE = "default-locale";
    public static final String TRACK_ON_START = "track-on-start";
    public static final String COMPASS_TRACKING = "compass-tracking";
    public static final String SCOREBOARD_TRACKING = "scoreboard-tracking";
}
