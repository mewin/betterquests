/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util;

/**
 *
 * @author mewin
 */
public class MetaKeys
{
    public static final String LOCALE = "locale";
    public static final String SCOREBOARD = "scoreboard";
}
