/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquests.util;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public class MinecraftUtil
{
    public static boolean playerCanEnter(Block block)
    {
        return block.isEmpty() || !block.getType().isSolid();
    }

    public static boolean isValidTpLocation(Location loc)
    {
        Block block = loc.getBlock();
        Block blockAbove = block.getRelative(BlockFace.UP);

        return playerCanEnter(block) && playerCanEnter(blockAbove);
    }

    public static Location findTeleportLocation(Location loc, int maxDist)
    {
        if (isValidTpLocation(loc)) {
            return loc;
        }

        for (int dist = 1; dist <= maxDist; ++dist)
        {
            // -x direction
            for (int y = -dist; y <= dist; ++y)
            {
                for (int z = -dist; z <= dist; ++z)
                {
                    Location loc2 = loc.add(-dist, y, z);
                    if (isValidTpLocation(loc2)) {
                        return loc2;
                    }
                }
            }
            // +x direction
            for (int y = -dist; y <= dist; ++y)
            {
                for (int z = -dist; z <= dist; ++z)
                {
                    Location loc2 = loc.add(dist, y, z);
                    if (isValidTpLocation(loc2)) {
                        return loc2;
                    }
                }
            }
            // -y direction
            for (int x = -dist + 1; x <= dist - 1; ++x)
            {
                for (int z = -dist; z <= dist; ++z)
                {
                    Location loc2 = loc.add(x, -dist, z);
                    if (isValidTpLocation(loc2)) {
                        return loc2;
                    }
                }
            }
            // +y direction
            for (int x = -dist + 1; x <= dist - 1; ++x)
            {
                for (int z = -dist; z <= dist; ++z)
                {
                    Location loc2 = loc.add(x, dist, z);
                    if (isValidTpLocation(loc2)) {
                        return loc2;
                    }
                }
            }
            // -z direction
            for (int x = -dist + 1; x <= dist - 1; ++x)
            {
                for (int y = -dist + 1; y <= dist - 1; ++y)
                {
                    Location loc2 = loc.add(x, y, -dist);
                    if (isValidTpLocation(loc2)) {
                        return loc2;
                    }
                }
            }
            // +z direction
            for (int x = -dist + 1; x <= dist - 1; ++x)
            {
                for (int y = -dist + 1; y <= dist - 1; ++y)
                {
                    Location loc2 = loc.add(x, y, dist);
                    if (isValidTpLocation(loc2)) {
                        return loc2;
                    }
                }
            }
        }

        return null;
    }

    public static Location findTeleportLocation(Location loc)
    {
        return findTeleportLocation(loc, 5);
    }

    public static boolean teleportNear(Player player, Location loc, int maxDist)
    {
        Location loc2 = findTeleportLocation(loc, maxDist);
        if (loc2 == null) {
            return false;
        }
        return player.teleport(loc2);
    }

    public static boolean teleportNear(Player player, Location loc)
    {
        return teleportNear(player, loc, 5);
    }
}
