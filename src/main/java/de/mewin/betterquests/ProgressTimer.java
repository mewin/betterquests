/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests;

import de.mewin.betterquests.progress.ProgressManager;

/**
 *
 * @author mewin
 */
public class ProgressTimer implements Runnable
{
    private final BetterQuestsPlugin plugin;

    public ProgressTimer(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public void run()
    {
        ProgressManager progressManager = plugin.getProgressManager();
        progressManager.processAdvertisements();
    }
}
