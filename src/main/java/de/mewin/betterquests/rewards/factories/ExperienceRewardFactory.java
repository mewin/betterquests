/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.factories;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.factory.RewardFactory;
import de.mewin.betterquests.rewards.ExperienceReward;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ExperienceRewardFactory extends RewardFactory
{

    @Override
    public Reward createNew(RewardsHolder holder)
    {
        ExperienceReward experienceReward = new ExperienceReward(holder);
        holder.addReward(experienceReward);
        return experienceReward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-experience-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        int exp = 0;
        int levels = 0;
        Object tmp;

        if ((tmp = yaml.get("exp")) != null && tmp instanceof Number) {
            exp = ((Number) tmp).intValue();
        }

        if ((tmp = yaml.get("levels")) != null && tmp instanceof Number) {
            levels = ((Number) tmp).intValue();
        }

        return new ExperienceReward(holder, exp, levels);
    }

    @Override
    public Map<String, Object> toYaml(Reward reward) throws IOException
    {
        ExperienceReward experienceReward = (ExperienceReward) reward;

        HashMap<String, Object> map = new HashMap<>();

        map.put("exp", experienceReward.getExp());
        map.put("levels", experienceReward.getLevels());

        return map;
    }

    public static final String MY_ID = "experience";
}
