/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.factories;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.factory.RewardFactory;
import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.ItemReward;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemRewardFactory extends RewardFactory
{
    public static final String MY_ID = "item-stack";

    @Override
    public Reward createNew(RewardsHolder holder)
    {
        ItemReward reward = new ItemReward(holder);
        holder.addReward(reward);
        return reward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-item-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        Object itemsObj = yaml.get("items");
        if (!(itemsObj instanceof Map)) {
            return new ItemReward(holder);
        }
        Map<String, Object> items = (Map<String, Object>) itemsObj;

        return new ItemReward(holder, Serializer.deserializeItemStack(items));
    }

    @Override
    public Map<String, Object> toYaml(Reward object) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();
        ItemReward reward = (ItemReward) object;

        ItemStack items = reward.getItemStack();

        if (items != null) {
            map.put("items", Serializer.serializeItemStack(items));
        }

        return map;
    }
}
