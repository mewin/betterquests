/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.factories;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.factory.RewardFactory;
import de.mewin.betterquests.rewards.CommandReward;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CommandRewardFactory extends RewardFactory
{
    @Override
    public Reward createNew(RewardsHolder holder)
    {
        CommandReward commandReward = new CommandReward(holder);
        holder.addReward(commandReward);
        return commandReward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-command-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String command = "";
        Object tmp;

        if ((tmp = yaml.get("command")) != null) {
            command = tmp.toString();
        }

        return new CommandReward(holder, command);
    }

    @Override
    public Map<String, Object> toYaml(Reward reward) throws IOException
    {
        CommandReward commandReward = (CommandReward) reward;

        HashMap<String, Object> map = new HashMap<>();

        map.put("command", commandReward.getCommand());

        return map;
    }

    public static final String MY_ID = "command";
}
