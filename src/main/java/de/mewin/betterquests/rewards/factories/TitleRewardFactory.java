/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.factories;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.factory.RewardFactory;
import de.mewin.betterquests.rewards.TitleReward;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TitleRewardFactory extends RewardFactory
{
    public static final String MY_ID = "title";

    @Override
    public Reward createNew(RewardsHolder holder)
    {
        TitleReward reward = new TitleReward(holder);
        holder.addReward(reward);
        return reward;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-title-factory");
    }

    @Override
    public Reward fromYaml(RewardsHolder holder, Map<String, Object> yaml) throws IOException
    {
        String title = "";
        String subtitle = "";
        Object tmp;

        if ((tmp = yaml.get("title")) != null) {
            title = tmp.toString();
        }

        if ((tmp = yaml.get("subtitle")) != null) {
            subtitle = tmp.toString();
        }

        return new TitleReward(holder, title, subtitle);
    }

    @Override
    public Map<String, Object> toYaml(Reward object) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();
        TitleReward reward = (TitleReward) object;

        String title = reward.getTitle();
        String subtitle = reward.getSubtitle();

        if (!"".equals(title)) {
            map.put("title", title);
        }

        if (!"".equals(subtitle)) {
            map.put("subtitle", subtitle);
        }

        return map;
    }
}
