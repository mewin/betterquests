/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.conversation;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.TitleReward;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TitleRewardPrompt extends RewardPrompt
{
    private TitleReward titleReward;

    public TitleRewardPrompt(TitleReward reward, RewardsHolder holder, MenuPrompt parentPrompt, CommandSender sender)
    {
        super(reward, holder, parentPrompt);

        this.titleReward = reward;
    }



    @Override
    protected void fillOptions(final CommandSender sender)
    {
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-title-title", titleReward.getTitle()),
                        new StringAcceptorPrompt(this, "prompt-title-title", titleAcceptor()));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-title-subtitle", titleReward.getSubtitle()),
                        new StringAcceptorPrompt(this, "prompt-title-subtitle", subtitleAcceptor()));

        super.fillOptions(sender);
    }

    private StringAcceptor titleAcceptor()
    {
        return new StringAcceptor()
        {
            @Override
            public boolean setString(String value)
            {
                titleReward.setTitle(value);
                return true;
            }

            @Override
            public String getString()
            {
                return titleReward.getTitle();
            }
        };
    };

    private StringAcceptor subtitleAcceptor()
    {
        return new StringAcceptor()
        {
            @Override
            public boolean setString(String value)
            {
                titleReward.setSubtitle(value);
                return true;
            }

            @Override
            public String getString()
            {
                return titleReward.getSubtitle();
            }
        };
    };
}
