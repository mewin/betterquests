/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.conversation;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.rewards.ItemReward;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemAmountAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemDamageAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemMaterialAcceptorPrompt;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemRewardPrompt extends RewardPrompt
{
    private final ItemReward itemReward;

    public ItemRewardPrompt(ItemReward reward, RewardsHolder holder, MenuPrompt parentPrompt, CommandSender sender)
    {
        super(reward, holder, parentPrompt);

        this.itemReward = reward;
    }

    @Override
    protected void fillOptions(final CommandSender sender)
    {
        ItemStack items = itemReward.getItemStack();

        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-material", items.getType().toString()),
                        new ItemMaterialAcceptorPrompt(itemReward, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-amount", items.getAmount()),
                        new ItemAmountAcceptorPrompt(itemReward, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-meta", items.getDurability()),
                        new ItemDamageAcceptorPrompt(itemReward, this));

        if (sender instanceof Player)
        {
            addComplexOption(CHAT_HELPER.getMessage(sender, "menu-item-fromhand"),
            new PromptSupplier()
            {
                @Override
                public Prompt get()
                {
                    Player player = (Player) sender;
                    ItemStack stack = player.getInventory().getItemInMainHand();
                    if (stack == null) { // just in case
                        stack = new ItemStack(Material.AIR, 0);
                    }
                    itemReward.setItemStack(stack);
                    ItemRewardPrompt.this.reloadMenu();
                    return ItemRewardPrompt.this;
                }
            });
        }

        super.fillOptions(sender);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());

        settings.add(new KnownSetting(ItemReward.TAKE_ITEMS, DataType.BOOLEAN));

        return settings;
    }
}
