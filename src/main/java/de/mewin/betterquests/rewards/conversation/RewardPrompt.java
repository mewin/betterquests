/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.conversation;

import de.mewin.betterquests.conversation.editors.CustomizableEditorPrompt;
import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RewardPrompt extends CustomizableEditorPrompt<RewardsHolder, Reward>
{
    public RewardPrompt(Reward reward, RewardsHolder holder, MenuPrompt parentPrompt)
    {
        super(reward, holder, parentPrompt, "advanced-reward");
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-edit-reward");
    }

    @Override
    protected void doDelete()
    {
        base.removeReward(object);
    }

    @Override
    protected String getString(Strings id)
    {
        switch(id)
        {
            case MENU_DELETE:
                return "menu-delete-reward";
            default:
                return "<missing string>";
        }
    }
}
