/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.conversation;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.ExperienceReward;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ExperienceRewardPrompt extends RewardPrompt
{
    private final ExperienceReward experienceReward;

    public ExperienceRewardPrompt(ExperienceReward reward, RewardsHolder holder, MenuPrompt parentPrompt)
    {
        super(reward, holder, parentPrompt);

        this.experienceReward = reward;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-experience", experienceReward.getExp()),
            new IntAcceptorPrompt(this, "prompt-experience", experienceReward.getExpAcceptor()));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-levels", experienceReward.getLevels()),
            new IntAcceptorPrompt(this, "prompt-levels", experienceReward.getLevelsAcceptor()));

        super.fillOptions(receiver);
    }
}
