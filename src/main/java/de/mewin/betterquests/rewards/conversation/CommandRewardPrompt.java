/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards.conversation;

import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.rewards.CommandReward;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CommandRewardPrompt extends RewardPrompt
{
    private final CommandReward commandReward;

    public CommandRewardPrompt(CommandReward reward, RewardsHolder holder, MenuPrompt parentPrompt)
    {
        super(reward, holder, parentPrompt);

        this.commandReward = reward;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-command", commandReward.getCommand()),
                new StringAcceptorPrompt(this, "prompt-enter-command", commandReward));

        super.fillOptions(receiver);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> knownSettings = new ArrayList<>(super.getKnownSettings());

        knownSettings.add(new KnownSetting(CommandReward.AS_PLAYER, DataType.BOOLEAN));

        return knownSettings;
    }
}
