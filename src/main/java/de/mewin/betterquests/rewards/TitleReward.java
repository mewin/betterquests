/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.TitleRewardPrompt;
import de.mewin.betterquests.rewards.factories.TitleRewardFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TitleReward extends Reward
{
    private String title;
    private String subtitle;

    public TitleReward(RewardsHolder holder)
    {
        super(holder);

        title = "";
        subtitle = "";
    }

    public TitleReward(RewardsHolder holder, String title, String subtitle)
    {
        super(holder);

        this.title = title;
        this.subtitle = subtitle;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSubtitle()
    {
        return subtitle;
    }

    public void setSubtitle(String subtitle)
    {
        this.subtitle = subtitle;
    }

    @Override
    public void giveReward(Player player)
    {
        // TODO: use formatUserMessage when there is a possibility to send json titles
        player.sendTitle(CHAT_HELPER.formatPlayerString(title, player),
                CHAT_HELPER.formatPlayerString(subtitle, player));
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-title", title, subtitle);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new TitleRewardPrompt(this, holder, parentPrompt, sender);
    }

    @Override
    public String getMessage(Player player)
    {
        return "";
    }

    @Override
    public String getFactoryId()
    {
        return TitleRewardFactory.MY_ID;
    }

}
