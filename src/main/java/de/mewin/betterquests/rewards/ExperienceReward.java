/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.ExperienceRewardPrompt;
import de.mewin.betterquests.rewards.factories.ExperienceRewardFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ExperienceReward extends Reward
{
    private int exp;
    private int levels;

    public ExperienceReward(RewardsHolder holder)
    {
        super(holder);
        exp = 0;
        levels = 0;
    }

    public ExperienceReward(RewardsHolder holder, int exp, int levels)
    {
        super(holder);
        this.exp = exp;
        this.levels = levels;
    }

    public int getExp()
    {
        return exp;
    }

    public void setExp(int exp)
    {
        this.exp = exp;
    }

    public int getLevels()
    {
        return levels;
    }

    public void setLevels(int levels)
    {
        this.levels = levels;
    }

    @Override
    public void giveReward(Player player)
    {
        if (exp != 0) {
            player.giveExp(exp);
        }

        if (levels != 0) {
            player.giveExpLevels(levels);
        }
    }

    @Override
    public String getMessage(Player player)
    {
        if (levels == 0) {
            return CHAT_HELPER.getMessage(player, "reward-experience-message-exp", exp);
        }
        if (exp == 0) {
            return CHAT_HELPER.getMessage(player, "reward-experience-message-levels", levels);
        }
        return CHAT_HELPER.getMessage(player, "reward-experience-message", levels, exp);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-experience", levels, exp);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new ExperienceRewardPrompt(this, holder, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return ExperienceRewardFactory.MY_ID;
    }


    public IntAcceptor getExpAcceptor()
    {
        return new IntAcceptor()
        {
            @Override
            public int getInt()
            {
                return exp;
            }

            @Override
            public boolean setInt(int value)
            {
                exp = value;
                return true;
            }
        };
    }

    public IntAcceptor getLevelsAcceptor()
    {
        return new IntAcceptor()
        {
            @Override
            public int getInt()
            {
                return levels;
            }

            @Override
            public boolean setInt(int value)
            {
                levels = value;
                return true;
            }
        };
    }
}
