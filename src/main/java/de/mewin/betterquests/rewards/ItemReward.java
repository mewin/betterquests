/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.ItemRewardPrompt;
import de.mewin.betterquests.rewards.factories.ItemRewardFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemStackAcceptor;
import de.mewin.mewinbukkitlib.util.Items;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemReward extends Reward implements ItemStackAcceptor
{
    private ItemStack items;

    public ItemReward(RewardsHolder holder)
    {
        super(holder);
        items = new ItemStack(Material.AIR);
    }

    public ItemReward(RewardsHolder holder, ItemStack items)
    {
        super(holder);
        this.items = items;
    }

    @Override
    public ItemStack getItemStack()
    {
        return items;
    }

    @Override
    public void setItemStack(ItemStack itemStack)
    {
        items = new ItemStack(itemStack);
    }

    @Override
    public void giveReward(Player player)
    {
        if (getSettings().getBoolean(ItemReward.TAKE_ITEMS, false)) // take, dont give
        {
            // player.getInventory().removeItem(new ItemStack(items)); // if we cant take it, dont do it
            Items.removeFromInventory(player.getInventory(), new ItemStack(items), false);
            return;
        }

        ItemStack rest = player.getInventory().addItem(new ItemStack(items)).get(0);

        if (rest != null && rest.getAmount() > 0) {
            player.getWorld().dropItem(player.getLocation(), rest);
        }
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-item");
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new ItemRewardPrompt(this, holder, parentPrompt, sender);
    }

    @Override
    public String getMessage(Player player)
    {
        return CHAT_HELPER.getMessage(player, "reward-item-message", items.getAmount(), items.getType().toString());
    }

    @Override
    public String getFactoryId()
    {
        return ItemRewardFactory.MY_ID;
    }

    public static final String TAKE_ITEMS = "take-items";
}
