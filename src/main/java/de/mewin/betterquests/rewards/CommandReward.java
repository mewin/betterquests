/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.rewards;

import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;
import de.mewin.betterquests.rewards.conversation.CommandRewardPrompt;
import de.mewin.betterquests.rewards.factories.CommandRewardFactory;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CommandReward extends Reward implements StringAcceptor
{
    private String command;

    public CommandReward(RewardsHolder holder)
    {
        super(holder);
        command = "";
    }

    public CommandReward(RewardsHolder holder, String command)
    {
        super(holder);
        this.command = command;
    }

    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    @Override
    public void giveReward(Player player)
    {
        CommandSender sender;
        if (getSettings().getBoolean(CommandReward.AS_PLAYER, false)) {
            sender = player;
        } else {
            sender = Bukkit.getConsoleSender();
        }

        String cmd = CHAT_HELPER.formatPlayerString(command, player);
        Bukkit.dispatchCommand(sender, cmd);
    }

    @Override
    public String getMessage(Player player)
    {
        return ""; // this doesnt really apply here
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "reward-command", command);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, RewardsHolder holder, CommandSender sender)
    {
        return new CommandRewardPrompt(this, holder, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return CommandRewardFactory.MY_ID;
    }

    @Override
    public String getString()
    {
        return command;
    }

    @Override
    public boolean setString(String value)
    {
        command = value;
        return true;
    }

    public static final String AS_PLAYER = "as-player";
}
