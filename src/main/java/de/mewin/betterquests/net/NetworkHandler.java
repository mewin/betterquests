/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.net;

import de.mewin.betterquests.BetterQuestsPlugin;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 *
 * @author mewin
 */
public class NetworkHandler extends AbstractNetworkBackend<Player> implements PluginMessageListener
{
    private final BetterQuestsPlugin plugin;
    private final NetworkManager<Player> networkManager;

    public NetworkHandler(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
        this.networkManager = new NetworkManager<>(this);
    }

    public void init()
    {
        Bukkit.getMessenger().registerOutgoingPluginChannel(plugin, CHANNEL);
        Bukkit.getMessenger().registerIncomingPluginChannel(plugin, CHANNEL, this);

        plugin.getLogger().info("Registered network handler");
    }

    public void sendMessage(Message message, Player player)
    {
        networkManager.sendMessage(message, player);
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message)
    {
        networkManager.handleMessage(message, player);
    }

    @Override
    public void sendMessage(byte[] data, Player player)
    {
        player.sendPluginMessage(plugin, CHANNEL, data);
    }

    @Override
    public void printError(String msg, Throwable throwable)
    {
        plugin.getLogger().log(Level.WARNING, "Error in network handler: {0}", msg);
        if (throwable != null) {
            plugin.getLogger().log(Level.WARNING, "", throwable);
        }
    }

    @Override
    public void onMessageHandshake(MessageHandshake messageHandshake, Player player)
    {
        player.setMetadata(META_PROTOCOL_VERSION, new FixedMetadataValue(plugin, messageHandshake.getVersion()));
        plugin.getLogger().log(Level.INFO, "Player {0} uses the UI mod. Thats great!", player.getName());

        // shake back
        networkManager.sendMessage(new MessageHandshake(), player);
    }

    @Override
    public void onMessageAcceptQuest(MessageAcceptQuest messageAcceptQuest, Player owner)
    {
        plugin.getUserInputHandler().doAcceptAdvertisement(owner, messageAcceptQuest.getUuid());
    }

    @Override
    public void onMessageCommand(MessageCommand messageCommand, Player owner)
    {
        switch (messageCommand.getCommand())
        {
            case MessageCommand.CMD_SHOW_QUESTLOG:
                plugin.getUserInputHandler().doShowQuestLog(owner);
                break;
        }
    }

    public static boolean hasUIMod(Player player)
    {
        return player.hasMetadata(META_PROTOCOL_VERSION);
    }

    public static final String META_PROTOCOL_VERSION = "betterquests-protocol-version";
    public static final String CHANNEL = "betterquests";
}
