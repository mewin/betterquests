/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.net;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.event.AdvertiseQuestEvent;
import de.mewin.betterquests.event.ShowQuestLogEvent;
import de.mewin.betterquests.progress.QuestAdvertisement;
import de.mewin.betterquests.quest.Quest;

import java.util.UUID;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NetActionsListener implements Listener
{
    private final BetterQuestsPlugin plugin;
    private final NetworkHandler networkHandler;

    public NetActionsListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
        this.networkHandler = plugin.getNetworkHandler();
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event)
    {
        // client will have to send the handshake on next join
        // important in case the client later reconnects without the mod
        event.getPlayer().removeMetadata(NetworkHandler.META_PROTOCOL_VERSION, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAdvertiseQuest(AdvertiseQuestEvent event)
    {
        Player player = event.getPlayer();

        if (NetworkHandler.hasUIMod(player))
        {
            QuestAdvertisement advertisement = event.getAdvertisement();

            UUID uuid = advertisement.getUuid();
            Quest quest = event.getQuest();
            String questTitle = CHAT_HELPER.formatPlayerString(quest.getTitle(), player);
            String questDescription = CHAT_HELPER.formatPlayerString(quest.getDescription(), player);

            Message message = new MessageAdvertiseQuest(uuid, questTitle, questDescription);

            networkHandler.sendMessage(message, player);
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onShowQuestLog(ShowQuestLogEvent event)
    {
        Player player = event.getPlayer();

        if (NetworkHandler.hasUIMod(player))
        {
            plugin.getNetworkHelper().sendQuestLog(player);

            event.setCancelled(true);
        }
    }
}
