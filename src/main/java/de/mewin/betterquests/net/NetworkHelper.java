/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.net;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.FinishedQuestInfo;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.Stage;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NetworkHelper
{
    private final BetterQuestsPlugin plugin;
    private final NetworkHandler networkHandler;

    public NetworkHelper(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
        this.networkHandler = plugin.getNetworkHandler();
    }

    private void sendQuestProgress(Player player, QuestProgress progress)
    {
        ProgressManager progressManager = plugin.getProgressManager();
        Quest quest = progress.getQuest();
        Stage stage = progress.getCurrentStage();
        String questId = quest.getId();
        int index = quest.getStages().indexOf(stage);

        if (index < 0) {
            return;
        }

        String objectivesDesc = progressManager.formatObjectiveMessage(player, progress);
        long time = progress.getTimeStarted().getTime();

        MessageQuestProgress message = new MessageQuestProgress(questId, (short) index, time, objectivesDesc);
        networkHandler.sendMessage(message, player);
    }

    private void sendFinishedQuest(Player player, FinishedQuestInfo finishedQuest)
    {
        Quest quest = finishedQuest.getQuest();
        String questId = quest.getId();
        int numSuccess = finishedQuest.getNumSuccess();
        int numFailed = finishedQuest.getNumFailed();
        long time = finishedQuest.getTime().getTime();

        MessageFinishedQuest message = new MessageFinishedQuest(questId, (short) numSuccess, (short) numFailed, time);
        networkHandler.sendMessage(message, player);
    }

    private void sendQuestStage(Player player, Quest quest, int index)
    {
        Stage stage = quest.getStages().get(index);
        String questId = quest.getId();
        ArrayList<String> objectiveDescs = new ArrayList<>();
        ArrayList<String> rewardDescs = new ArrayList<>();

        for (Objective objective : stage.getObjectives()) {
            // objectiveDescs.add(objective.getDescription(player, null)); // TODO: null is bad
        }

        for (Reward reward : stage.getRewards()) {
            rewardDescs.add(reward.getMessage(player));
        }

        String objectivesDesc = String.join("\n", objectiveDescs);
        String rewardsDesc = String.join("\n", rewardDescs);

        MessageQuestStage message = new MessageQuestStage(questId, (short) index, objectivesDesc, rewardsDesc);

        networkHandler.sendMessage(message, player);
    }

    public void sendQuest(Player player, Quest quest)
    {
        String questId = quest.getId();
        String questTitle = CHAT_HELPER.formatPlayerString(quest.getTitle(), player);
        String questDescription = CHAT_HELPER.formatPlayerString(quest.getDescription(), player);
        MessageQuestInfo message = new MessageQuestInfo(questId, questTitle, questDescription);

        networkHandler.sendMessage(message, player);

        for (int i = 0; i < quest.getStages().size(); ++i)
        {
            sendQuestStage(player, quest, i);
        }
    }

    public void sendQuestLog(Player player)
    {
        ProgressManager progressManager = plugin.getProgressManager();
        PlayerProgress playerProgress = progressManager.getPlayerProgress(player);

        List<QuestProgress> questProgresses    = new ArrayList<>(playerProgress.getQuestProgresses().values());
        List<FinishedQuestInfo> finishedQuests = new ArrayList<>(playerProgress.getFinishedQuests().values());
        // first send quests
        for (QuestProgress questProgress : questProgresses) {
            sendQuest(player, questProgress.getQuest());
        }

        for (FinishedQuestInfo finishedQuest : finishedQuests) {
            sendQuest(player, finishedQuest.getQuest());
        }

        for (QuestProgress questProgress : questProgresses) {
            sendQuestProgress(player, questProgress);
        }

        for (FinishedQuestInfo finishedQuest : finishedQuests) {
            sendFinishedQuest(player, finishedQuest);
        }

        MessageCommand message = new MessageCommand(MessageCommand.CMD_SHOW_QUESTLOG);
        networkHandler.sendMessage(message, player);
    }

}
