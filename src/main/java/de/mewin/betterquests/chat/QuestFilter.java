/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.chat;

import de.mewin.betterquests.quest.Quest;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

/**
 *
 * @author mewin
 */
public interface QuestFilter
{
    public abstract boolean isFiltering();
    public abstract boolean showQuest(Quest quest);
    public abstract void sendQuest(CommandSender sender, int index, Quest quest);
    public abstract Prompt onInput(ConversationContext context, String input);
    public abstract boolean hasCheckboxes();
    public abstract void onCheck(int index);
    public abstract void setChecked(int index, boolean check);
    public abstract boolean isChecked(int index);
}
