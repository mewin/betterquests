/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.chat;

import org.bukkit.plugin.Plugin;

/**
 *
 * @author mewin
 */
public abstract class MessageBuilder
{
    public abstract boolean containsMessage(String msgId);
    public abstract String getMessage(Object receiver, String msgId, Object ... args);
    public abstract void loadMessages(Plugin plugin);
}
