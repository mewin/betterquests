/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.chat;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.FinishedQuestInfo;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.json.JSONText;
import java.util.UUID;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ProgressQuestListFilter extends CheckboxQuestFilter
{
    private final UUID playerUuid;

    public ProgressQuestListFilter(UUID playerUuid)
    {
        this.playerUuid = playerUuid;
    }

    @Override
    public boolean isFiltering()
    {
        return true;
    }

    @Override
    public boolean showQuest(Quest quest)
    {
        ProgressManager pm = BetterQuestsPlugin.getInstance().getProgressManager();
        QuestProgress progress = pm.getQuestProgress(playerUuid, quest);

        if (progress != null) {
            return true;
        }

        FinishedQuestInfo finishedQuest = pm.getFinishedQuest(playerUuid, quest);
        return finishedQuest != null;
    }

    @Override
    protected void addQuestSuffix(CommandSender sender, int index, Quest quest, JSONText.MultiBuilder builder)
    {
        ProgressManager pm = BetterQuestsPlugin.getInstance().getProgressManager();
        boolean finished = pm.getQuestProgress(playerUuid, quest) == null;

        builder.setText(" ")
            .next()
            .setText(CHAT_HELPER.getMessage(sender, finished ? "quest-list-finished" : "quest-list-active"));
    }
}
