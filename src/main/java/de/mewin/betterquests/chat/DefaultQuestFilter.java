/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.chat;

import de.mewin.betterquests.quest.Quest;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class DefaultQuestFilter implements QuestFilter
{

    @Override
    public boolean isFiltering()
    {
        return false;
    }

    @Override
    public boolean showQuest(Quest quest)
    {
        return true;
    }

    @Override
    public void sendQuest(CommandSender sender, int index, Quest quest)
    {
        CHAT_HELPER.sendMessage(sender, "quest-list-entry", index + 1, quest.getId());
    }

    @Override
    public void onCheck(int index)
    {

    }

    @Override
    public boolean hasCheckboxes()
    {
        return false;
    }

    @Override
    public void setChecked(int index, boolean check)
    {

    }

    @Override
    public Prompt onInput(ConversationContext context, String input)
    {
        return null;
    }

    @Override
    public boolean isChecked(int index)
    {
        return false;
    }
}
