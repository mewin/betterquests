/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.chat;

import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import java.util.HashSet;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CheckboxQuestFilter extends InteractiveQuestFilter
{
    private final HashSet<Integer> checked;

    public CheckboxQuestFilter()
    {
        this.checked = new HashSet<>();
    }

    @Override
    protected void addQuestPrefix(CommandSender sender, int index, Quest quest, JSONText.MultiBuilder builder)
    {
        String prefix;
        ShowTextEvent hoverEvent;

        if (checked.contains(index))
        {
            prefix = CHAT_HELPER.getMessage(sender, "menu-checkbox-checked");
            hoverEvent = new ShowTextEvent(CHAT_HELPER.getMessage(sender, "menu-checkbox-checked-hover"));
        }
        else
        {
            prefix = CHAT_HELPER.getMessage(sender, "menu-checkbox");
            hoverEvent = new ShowTextEvent(CHAT_HELPER.getMessage(sender, "menu-checkbox-hover"));
        }

        builder.setText(prefix)
               .setHoverEvent(hoverEvent)
               .setClickEvent(new RunCommandEvent(String.format("/bq_callback conversation-send x %d", index)))
               .next()
               .setText(" ");
    }

    @Override
    public boolean hasCheckboxes()
    {
        return true;
    }

    @Override
    public void onCheck(int index)
    {
        if (index < 0) {
            return;
        }

        if (!checked.remove(index)) {
            checked.add(index);
        }
    }

    @Override
    public void setChecked(int index, boolean check)
    {
        if (check) {
            checked.add(index);
        } else {
            checked.remove(index);
        }
    }

    @Override
    public boolean isChecked(int index)
    {
        return checked.contains(index);
    }
}
