/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.chat;

import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class InteractiveQuestFilter extends DefaultQuestFilter
{
    @Override
    public void sendQuest(CommandSender sender, int index, Quest quest)
    {
        if (sender instanceof Player)
        {
            JSONText.MultiBuilder builder = new JSONText.MultiBuilder();
            addQuestPrefix(sender, index, quest, builder);
            builder.next()
                   .setText(CHAT_HELPER.getMessage(sender, "quest-list-entry", index + 1, quest.getId()))
                   .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(sender, "quest-list-hover", quest.getId())))
                   .setClickEvent(new RunCommandEvent("/bq_callback conversation-send " + (index + 1)))
                   .next();
            addQuestSuffix(sender, index, quest, builder);
            CHAT_HELPER.sendJSONMessage((Player) sender, builder.build());
        }
        else {
            super.sendQuest(sender, index, quest);
        }
    }

    protected void addQuestPrefix(CommandSender sender, int index, Quest quest, JSONText.MultiBuilder builder)
    {

    }

    protected void addQuestSuffix(CommandSender sender, int index, Quest quest, JSONText.MultiBuilder builder)
    {

    }
}
