/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.input.UserInputHandler;
import de.mewin.betterquests.quest.Quest;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class RenameQuestPrompt extends StringPrompt
{
    private final Prompt parentPrompt;
    private final Quest quest;

    public RenameQuestPrompt(Quest quest, Prompt parentPrompt)
    {
        this.quest = quest;
        this.parentPrompt = parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), "prompt-rename-quest", quest.getId());
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        CommandSender sender = (CommandSender) context.getForWhom();
        UserInputHandler userInputHandler = plugin.getUserInputHandler();

        if (userInputHandler.doRenameQuest(sender, quest, input))
        {
            return parentPrompt;
        }

        return this;
    }

}
