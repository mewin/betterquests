/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.Factory;
import de.mewin.betterquests.util.Util;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class AddObjectivePrompt extends AddCustomizablePrompt<ObjectivesHolder, Objective>
{
    public AddObjectivePrompt(ObjectivesHolder holder, MenuPrompt parentPrompt, CommandSender sender)
    {
        super(holder, parentPrompt, sender);
    }

    @Override
    protected List<? extends Factory<ObjectivesHolder, Objective>> getFactories()
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        FactoryRegistry factoryRegistry = plugin.getFactoryRegistry();

        return Util.getSortedList(factoryRegistry.getObjectiveFactories());
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-add-objective");
    }
}
