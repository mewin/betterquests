/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.FactoryRegistry;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.factory.Factory;
import de.mewin.betterquests.util.Util;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class AddTriggerPrompt extends AddCustomizablePrompt<Quest, Trigger>
{
    public AddTriggerPrompt(Quest quest, MenuPrompt parentPrompt, CommandSender sender)
    {
        super(quest, parentPrompt, sender);
    }

    @Override
    protected List<? extends Factory<Quest, Trigger>> getFactories()
    {
        BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        FactoryRegistry factoryRegistry = plugin.getFactoryRegistry();

        return Util.getSortedList(factoryRegistry.getTriggerFactories());
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-add-trigger");
    }
}
