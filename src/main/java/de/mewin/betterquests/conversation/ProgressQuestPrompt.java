/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.chat.ProgressQuestListFilter;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import java.util.UUID;
import java.util.function.Function;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ProgressQuestPrompt extends SelectQuestPrompt
{
    private final UUID playerUuid;

    public ProgressQuestPrompt(BetterQuestsPlugin plugin, Prompt nextPrompt, Function<Quest, Prompt> func, UUID playerUuid)
    {
        super(plugin, nextPrompt, func, new ProgressQuestListFilter(playerUuid));

        this.playerUuid = playerUuid;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        String text = super.getPromptText(context);

        Conversable forWhom = context.getForWhom();

        JSONText[] messages = new JSONText.MultiBuilder()
                .setText(CHAT_HELPER.getMessage(forWhom, "menu-prefix-checked"))
                .next()
                .setText(CHAT_HELPER.getMessage(forWhom, "menu-tb-reset-quest"))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(forWhom, "menu-reset-quest")))
                .setClickEvent(new RunCommandEvent("/bq_callback conversation-send s reset"))
                .next()
                .setText(" ")
                .next()
                .setText(CHAT_HELPER.getMessage(forWhom, "menu-tb-abort-quest"))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(forWhom, "menu-abort-remove-quest")))
                .setClickEvent(new RunCommandEvent("/bq_callback conversation-send s abort"))
                .next()
                .setText(" ")
                .next()
                .setText(CHAT_HELPER.getMessage(forWhom, "menu-tb-finish-quest"))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(forWhom, "menu-finish-quest")))
                .setClickEvent(new RunCommandEvent("/bq_callback conversation-send s finish"))
                .next()
                .setText(" ")
                .next()
                .setText(CHAT_HELPER.getMessage(forWhom, "menu-tb-fail-quest"))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(forWhom, "menu-fail-quest")))
                .setClickEvent(new RunCommandEvent("/bq_callback conversation-send s fail"))
                .build();
        CHAT_HELPER.trySendJSONMessage(forWhom, messages);

        return text;
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        if (input.startsWith("s "))
        {
            Conversable forWhom = context.getForWhom();
            ProgressManager pm = BetterQuestsPlugin.getInstance().getProgressManager();
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerUuid);
            PlayerProgress playerProgress = pm.getPlayerProgress(offlinePlayer);

            switch(input.substring(2))
            {
                case "reset":
                {
                    Player player = offlinePlayer.getPlayer();
                    if (player == null)
                    {
                        CHAT_HELPER.trySendMessage(forWhom, "player-not-online");
                        break;
                    }
                    for (int i = 0; i < quests.size(); ++i)
                    {
                        if (questFilter.isChecked(i))
                        {
                            Quest quest = quests.get(i);
                            if (pm.getQuestProgress(offlinePlayer, quest) != null) {
                                pm.resetQuest(player, quest);
                            }
                        }
                    }
                    break;
                }
                case "abort":
                    for (int i = 0; i < quests.size(); ++i)
                    {
                        if (questFilter.isChecked(i))
                        {
                            Quest quest = quests.get(i);
                            if (pm.getQuestProgress(offlinePlayer, quest) != null) {
                                pm.abortQuest(offlinePlayer, quest);
                            } else {
                                pm.removeFinishedQuest(offlinePlayer, quest);
                            }
                        }
                    }
                    break;
                case "finish":
                {
                    Player player = offlinePlayer.getPlayer();
                    if (player == null)
                    {
                        CHAT_HELPER.trySendMessage(forWhom, "player-not-online");
                        break;
                    }

                    for (int i = 0; i < quests.size(); ++i)
                    {
                        if (questFilter.isChecked(i)) {
                            pm.finishQuest(player, quests.get(i));
                        }
                    }
                    break;
                }
                case "fail":
                {
                    Player player = offlinePlayer.getPlayer();
                    if (player == null)
                    {
                        CHAT_HELPER.trySendMessage(forWhom, "player-not-online");
                        break;
                    }

                    for (int i = 0; i < quests.size(); ++i)
                    {
                        if (questFilter.isChecked(i)) {
                            pm.failQuest(player, quests.get(i));
                        }
                    }
                    break;
                }
            }
            return Prompt.END_OF_CONVERSATION;
        }
        else {
            return super.acceptInput(context, input);
        }
    }
}
