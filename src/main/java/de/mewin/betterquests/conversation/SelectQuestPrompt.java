/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.input.UserInputHandler;
import de.mewin.betterquests.chat.InteractiveQuestFilter;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import de.mewin.betterquests.util.ConfigKeys;
import de.mewin.mewinbukkitlib.util.InputUtil;
import java.util.function.Function;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import de.mewin.betterquests.chat.QuestFilter;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class SelectQuestPrompt extends StringPrompt
{
    protected final BetterQuestsPlugin plugin;
    protected final Prompt nextPrompt;
    protected final Function<Quest, Prompt> func;
    protected QuestFilter questFilter;
    protected List<Quest> quests;
    protected int page;
    protected int perPage;
    protected int maxPage;

    public SelectQuestPrompt(BetterQuestsPlugin plugin, Prompt nextPrompt, Function<Quest, Prompt> func, QuestFilter questListFormat)
    {
        this.plugin = plugin;
        this.nextPrompt = nextPrompt;
        this.func = func;
        this.page = 1;
        this.questFilter = questListFormat;
        init();
    }

    public SelectQuestPrompt(BetterQuestsPlugin plugin, Prompt nextPrompt, Function<Quest, Prompt> func)
    {
        this(plugin, nextPrompt, func, new InteractiveQuestFilter());
    }

    public SelectQuestPrompt(BetterQuestsPlugin plugin, Prompt nextPrompt)
    {
        this(plugin, nextPrompt, null);
    }

    public SelectQuestPrompt(BetterQuestsPlugin plugin, Function<Quest, Prompt> func)
    {
        this(plugin, Prompt.END_OF_CONVERSATION, func);
    }

    private void init()
    {
        QuestRegistry questRegistry = plugin.getQuestRegistry();
        List<Quest> allQuests = questRegistry.getSortedQuests();
        quests = new ArrayList<>();
        if (questFilter.isFiltering())
        {
            for (Quest quest : allQuests)
            {
                if (questFilter.showQuest(quest)) {
                    quests.add(quest);
                }
            }
        }
        else
        {
            quests.addAll(allQuests);
        }
        perPage = plugin.getConfig().getInt(ConfigKeys.QUESTS_PER_PAGE, 8);
        maxPage = InputUtil.intDivCeil(quests.size(), perPage);
    }

    protected void checkIndex(int index)
    {
        if (questFilter != null) {
            questFilter.onCheck(index);
        }
    }

    protected void checkAt(String value)
    {
        if (questFilter != null)
        {
            switch(value)
            {
                case "all":
                    for (int i = 0; i < quests.size(); ++i) {
                        questFilter.setChecked(i, true);
                    }
                    break;
                case "none":
                    for (int i = 0; i < quests.size(); ++i) {
                        questFilter.setChecked(i, false);
                    }
                    break;
                default:
                    try {
                        checkIndex(Integer.valueOf(value));
                    } catch(NumberFormatException ex) {}
                    break;
            }
        }
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender sender = (CommandSender) context.getForWhom(); // TODO: I guess we can assume its always a command sender

        UserInputHandler userInputHandler = plugin.getUserInputHandler();
        userInputHandler.doListQuests(sender, (page - 1) * perPage, perPage, questFilter, quests);

        CHAT_HELPER.sendMessage(sender, "quest-list-page", page, maxPage);

        JSONText.MultiBuilder builder = new JSONText.MultiBuilder()
                    .setText(CHAT_HELPER.getMessage(sender, "quest-list-page-prev"))
                    .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(sender, "quest-list-page-prev-hover")))
                    .setClickEvent(new RunCommandEvent("/bq_callback conversation-send p"))
                    .next()
                    .setText(" ")
                    .next()
                    .setText(CHAT_HELPER.getMessage(sender, "quest-list-page-next"))
                    .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(sender, "quest-list-page-next-hover")))
                    .setClickEvent(new RunCommandEvent("/bq_callback conversation-send n"))
                    .next()
                    .setText(" ")
                    .next()
                    .setText(CHAT_HELPER.getMessage(sender, "quest-list-cancel"))
                    .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(sender, "quest-list-cancel-hover")))
                    .setClickEvent(new RunCommandEvent("/bq_callback conversation-send c"));

        if (questFilter != null && questFilter.hasCheckboxes())
        {
            builder.next()
                .setText(CHAT_HELPER.getMessage(sender, "menu-seperator"))
                .next()
                .setText(CHAT_HELPER.getMessage(sender, "menu-checkbox-checked"))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(sender, "menu-checkall")))
                .setClickEvent(new RunCommandEvent("/bq_callback conversation-send x all"))
                .next()
                .setText(" ")
                .next()
                .setText(CHAT_HELPER.getMessage(sender, "menu-checkbox"))
                .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(sender, "menu-checknone")))
                .setClickEvent(new RunCommandEvent("/bq_callback conversation-send x none"));
        }

        CHAT_HELPER.sendJSONMessage((Player) sender, builder.build());

        return CHAT_HELPER.getMessage(sender, "prompt-select-quest");
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        if (questFilter != null)
        {
            Prompt prompt = questFilter.onInput(context, input);
            if (prompt != null) {
                return prompt;
            }
        }

        String[] parts = input.split(" ");
        switch(parts[0].toLowerCase())
        {
            case "n":
            case "next":
                page = Math.min(page + 1, maxPage);
                return this;
            case "p":
            case "prev":
            case "previous":
                page = Math.max(page - 1, 1);
                return this;
            case "c":
            case "cancel":
                return Prompt.END_OF_CONVERSATION;
            case "x":
                if (parts.length > 1) {
                    checkAt(parts[1]);
                }
                return this;
        }

        CommandSender sender = (CommandSender) context.getForWhom(); // TODO: I guess we can assume its always a command sender

        QuestRegistry questRegistry = plugin.getQuestRegistry();
        Quest quest = questRegistry.findQuest(input, questFilter);

        if (quest == null) {
            CHAT_HELPER.sendMessage(sender, "quest-unknown", input);
            return this;
        }
        else
        {
            context.setSessionData("quest", quest);

            if (func != null)
            {
                Prompt prompt = func.apply(quest);
                if (prompt != null) {
                    return prompt;
                }
            }

            return nextPrompt;
        }
    }
}
