/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.input.UserInputHandler;
import de.mewin.betterquests.quest.Quest;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class NewQuestPrompt extends StringPrompt
{
    private final BetterQuestsPlugin plugin;

    public NewQuestPrompt(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), "prompt-newquest");
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        if (".".equals(input)) {
            return Prompt.END_OF_CONVERSATION;
        }

        CommandSender sender = (CommandSender) context.getForWhom(); // TODO: I guess we can assume its always a command sender
        if (!Quest.isValidQuestId(input)) {
            CHAT_HELPER.sendMessage(sender, "quest-illegalname", input);
            return this;
        }

        UserInputHandler userInputHandler = plugin.getUserInputHandler();
        if (!userInputHandler.doCreateQuest(sender, input)) {
            return this;
        }

        return Prompt.END_OF_CONVERSATION;
    }

}
