/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.conversation.AddRewardPrompt;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Reward;
import de.mewin.betterquests.quest.RewardsHolder;

import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditRewardsPrompt extends EditCustomizablePrompt<RewardsHolder, Reward>
{
    private final RewardsHolder holder;

    public EditRewardsPrompt(Quest quest, RewardsHolder holder, Prompt parentPrompt, CommandSender sender)
    {
        super(holder, parentPrompt, sender);

        this.holder = holder;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        printOptions(receiver);
        return CHAT_HELPER.getMessage(receiver, "prompt-edit-rewards");
    }

    @Override
    protected PromptSupplier getAddPrompt(final CommandSender sender)
    {
        return new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                return new AddRewardPrompt(holder, EditRewardsPrompt.this, sender);
            }
        };
    }

    @Override
    protected List<Reward> getObjects()
    {
        return holder.getRewards();
    }

    @Override
    protected String getMsgIdNew()
    {
        return "menu-new-reward";
    }
}
