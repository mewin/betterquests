/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.conversation.RenameQuestPrompt;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.extra.QuestSettings;

import java.util.Arrays;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditQuestPrompt extends MenuPrompt
{
    private final Quest quest;

    public EditQuestPrompt(Quest quest, CommandSender sender)
    {
        super();

        this.quest = quest;
    }

    @Override
    protected void fillOptions(CommandSender sender)
    {
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-rename-quest"), new RenameQuestPrompt(quest, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-advanced"),
                new EditSettingsPrompt(quest.getSettings(), this, Arrays.asList(QuestSettings.getKnownSettings()), "advanced-quest"));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-title", quest.getTitle()), new EditTitlePrompt(quest, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-description"), new EditDescriptionPrompt(quest, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-triggers"), new EditTriggersPrompt(quest, this, sender));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-requirements"), new EditRequirementsPrompt(quest, this, sender));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-stages"), new EditStagesPrompt(quest, this, sender));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-done"), Prompt.END_OF_CONVERSATION);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender sender = (CommandSender) context.getForWhom();

        CHAT_HELPER.sendMessage(sender, "quest-editor-header", quest.getId());
        printOptions(sender);

        return CHAT_HELPER.getMessage(sender, "prompt-edit-quest");
    }
}
