/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.conversation.AddTriggerPrompt;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Trigger;

import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditTriggersPrompt extends EditCustomizablePrompt<Quest, Trigger>
{
    public EditTriggersPrompt(Quest quest, Prompt parentPrompt, CommandSender sender)
    {
        super(quest, parentPrompt, sender);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        printOptions(receiver);
        return CHAT_HELPER.getMessage(receiver, "prompt-edit-triggers");
    }

    @Override
    protected PromptSupplier getAddPrompt(final CommandSender sender)
    {
        return new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                return new AddTriggerPrompt(base, EditTriggersPrompt.this, sender);
            }
        };
    }

    @Override
    protected List<Trigger> getObjects()
    {
        return base.getTriggers();
    }

    @Override
    protected String getMsgIdNew()
    {
        return "menu-new-trigger";
    }
}
