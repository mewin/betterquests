/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.conversation.LongStringPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditDescriptionPrompt extends LongStringPrompt // TODO: more complex text editor?
{
    private final Quest quest;
    private final Prompt parentPrompt;

    public EditDescriptionPrompt(Quest quest, Prompt parentPrompt)
    {
        super("-");
        this.quest = quest;
        this.parentPrompt = parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), "prompt-edit-description");
    }

    @Override
    protected Prompt acceptFinalInput(ConversationContext context, String input)
    {
        CommandSender sender = (CommandSender) context.getForWhom();

        quest.setDescription(input);

        CHAT_HELPER.sendMessage(sender, "quest-description-updated", quest.getId());

        return parentPrompt;
    }
}
