/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.quest.Customizable;
import de.mewin.mewinbukkitlib.conversation.editors.EditListPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

/**
 *
 * @author mewin
 */
public abstract class EditCustomizablePrompt<T, S extends Customizable<T>> extends EditListPrompt<S>
{
    protected final T base;

    public EditCustomizablePrompt(T quest, Prompt parentPrompt, CommandSender sender)
    {
        super(parentPrompt);

        this.base = quest;
    }

    @Override
    protected void addOptionFor(int index, S object, CommandSender sender)
    {
        addSimpleOption(object.getMenuLabel(sender), object.createPrompt(this, base, sender));
    }
}
