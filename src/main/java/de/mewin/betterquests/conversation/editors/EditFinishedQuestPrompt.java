/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.FinishedQuestInfo;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditFinishedQuestPrompt extends MenuPrompt
{
    private final OfflinePlayer offlinePlayer;
    private final FinishedQuestInfo finishedQuest;

    public EditFinishedQuestPrompt(OfflinePlayer offlinePlayer, FinishedQuestInfo finishedQuest)
    {
        this.offlinePlayer = offlinePlayer;
        this.finishedQuest = finishedQuest;
    }

    @Override
    protected void fillOptions(final CommandSender receiver)
    {
        final BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        final ProgressManager pm = plugin.getProgressManager();

        addOption(new MenuOption(CHAT_HELPER.getMessage(receiver, "menu-remove-finished-quest"))
        {
            @Override
            public Prompt handleSelect()
            {
                Quest quest = finishedQuest.getQuest();
                pm.removeFinishedQuest(offlinePlayer, quest);
                CHAT_HELPER.sendMessage(receiver, "finished-quest-removed", quest.getId(),
                                        offlinePlayer.getName());
                return END_OF_CONVERSATION;
            }
        });
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-done"), END_OF_CONVERSATION);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        String name = offlinePlayer.getName();
        if (name == null) { // pretty unlikely
            name = offlinePlayer.getUniqueId().toString();
        }

        Quest quest = finishedQuest.getQuest();
        int numSuccess = finishedQuest.getNumSuccess();
        int numFailed = finishedQuest.getNumFailed();

        CHAT_HELPER.sendMessage(receiver, "finished-quest-editor-header", quest.getId(), name);
        CHAT_HELPER.sendMessage(receiver, "finished-quest-editor-info-header", numSuccess, numFailed);
        printOptions(receiver);
        return CHAT_HELPER.getMessage(receiver, "prompt-edit-finished-quest");
    }


}
