/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;
import de.mewin.betterquests.quest.extra.StageSettings;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditStagePrompt extends MenuPrompt
{
    private final Quest quest;
    private final Stage stage;
    private final EditStagesPrompt parentPrompt;
    private final int index;

    public EditStagePrompt(Quest quest, int index, EditStagesPrompt parentPrompt, CommandSender sender)
    {
        super();

        this.quest = quest;
        this.parentPrompt = parentPrompt;
        this.index = index;

        List<Stage> stages = quest.getStages();
        if (index >= stages.size())
        {
            this.stage = new Stage(quest);
        }
        else {
            this.stage = quest.getStages().get(index);
        }
    }

    @Override
    protected void fillOptions(CommandSender sender)
    {
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-advanced"),
                new EditSettingsPrompt(stage.getSettings(), this, StageSettings.getKnownSettingsList(), "advanced-stage"));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-objectives"), new EditObjectivesPrompt(stage, this, sender));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-edit-rewards"), new EditRewardsPrompt(quest, stage, this, sender));
        addComplexOption(CHAT_HELPER.getMessage(sender, "menu-delete-stage"),
        new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                quest.removeStage(stage);

                parentPrompt.reloadMenu();

                return parentPrompt;
            }
        });

        if (index >= quest.getStages().size()) {
            addComplexOption(CHAT_HELPER.getMessage(sender, "menu-done"),
            new PromptSupplier()
            {
                @Override
                public Prompt get()
                {
                    quest.appendStage(stage);
                    parentPrompt.reloadMenu();

                    return parentPrompt;
                }
            });
        }
        else {
            addUpdatingOption(CHAT_HELPER.getMessage(sender, "menu-done"), parentPrompt);
        }
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender sender = (CommandSender) context.getForWhom();

        CHAT_HELPER.sendMessage(sender, "stage-editor-header", quest.getId(), index + 1);
        printOptions(sender);

        return CHAT_HELPER.getMessage(sender, "prompt-edit-stage");
    }
}
