/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.conversation.AddRequirementPrompt;
import de.mewin.betterquests.quest.Requirement;
import de.mewin.betterquests.quest.RequirementsHolder;

import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditRequirementsPrompt extends EditCustomizablePrompt<RequirementsHolder, Requirement>
{
    public EditRequirementsPrompt(RequirementsHolder holder, Prompt parentPrompt, CommandSender sender)
    {
        super(holder, parentPrompt, sender);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        printOptions(receiver);
        return CHAT_HELPER.getMessage(receiver, "prompt-edit-requirements");
    }

    @Override
    protected PromptSupplier getAddPrompt(final CommandSender sender)
    {
        return new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                return new AddRequirementPrompt(base, EditRequirementsPrompt.this, sender);
            }
        };
    }

    @Override
    protected List<Requirement> getObjects()
    {
        return base.getRequirements();
    }

    @Override
    protected String getMsgIdNew()
    {
        return "menu-new-requirement";
    }
}
