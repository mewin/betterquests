/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditTitlePrompt extends StringPrompt
{
    private final Quest quest;
    private final Prompt parentPrompt;

    public EditTitlePrompt(Quest quest, Prompt parentPrompt)
    {
        this.quest = quest;
        this.parentPrompt = parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), "prompt-edit-title");
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        quest.setTitle(input);

        if (parentPrompt instanceof MenuPrompt) {
            ((MenuPrompt) parentPrompt).reloadMenu();
        }
        
        return parentPrompt;
    }
}
