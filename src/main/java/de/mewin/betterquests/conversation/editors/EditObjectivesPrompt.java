/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.conversation.AddObjectivePrompt;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;

import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditObjectivesPrompt extends EditCustomizablePrompt<ObjectivesHolder, Objective>
{
    private final ObjectivesHolder holder;

    public EditObjectivesPrompt(ObjectivesHolder holder, Prompt parentPrompt, CommandSender sender)
    {
        super(holder, parentPrompt, sender);

        this.holder = holder;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        printOptions(receiver);
        return CHAT_HELPER.getMessage(receiver, "prompt-edit-objectives");
    }

    @Override
    protected PromptSupplier getAddPrompt(final CommandSender sender)
    {
        return new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                return new AddObjectivePrompt(holder, EditObjectivesPrompt.this, sender);
            }
        };
    }

    @Override
    protected List<Objective> getObjects()
    {
        return holder.getObjectives();
    }

    @Override
    protected String getMsgIdNew()
    {
        return "menu-new-objective";
    }
}
