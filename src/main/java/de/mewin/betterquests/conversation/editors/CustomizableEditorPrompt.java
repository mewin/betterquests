/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.quest.Customizable;
import de.mewin.betterquests.quest.extra.KnownSetting;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class CustomizableEditorPrompt<T, S extends Customizable<T>> extends MenuPrompt
{
    protected final S object;
    protected final T base;
    protected final MenuPrompt parentPrompt;
    protected final String settingMsgPrefix;

    public CustomizableEditorPrompt(S object, T base, MenuPrompt parentPrompt, String settingMsgPrefix)
    {
        super();

        this.object = object;
        this.base = base;
        this.parentPrompt = parentPrompt;
        this.settingMsgPrefix = settingMsgPrefix;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        List<KnownSetting> knownSettings = getKnownSettings();
        if (knownSettings != null && !knownSettings.isEmpty())
        {
            addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-edit-advanced"),
                    new EditSettingsPrompt(object.getSettings(), this, knownSettings,
                            settingMsgPrefix));
        }
        addComplexOption(CHAT_HELPER.getMessage(receiver, getString(Strings.MENU_DELETE)),
        new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                doDelete();
                parentPrompt.reloadMenu();

                return parentPrompt;
            }
        });

        addUpdatingOption(CHAT_HELPER.getMessage(receiver, "menu-done"), parentPrompt);
    }

    protected List<KnownSetting> getKnownSettings()
    {
        return new ArrayList<>();
    }
    protected abstract void doDelete();
    protected abstract String getString(Strings id);

    public static enum Strings
    {
        MENU_DELETE
    }
}
