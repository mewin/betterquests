/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditProgressPrompt extends MenuPrompt
{
    private final OfflinePlayer offlinePlayer;
    private final QuestProgress progress;

    public EditProgressPrompt(OfflinePlayer offlinePlayer, QuestProgress progress)
    {
        this.offlinePlayer = offlinePlayer;
        this.progress = progress;
    }

    @Override
    protected void fillOptions(final CommandSender receiver)
    {
        final BetterQuestsPlugin plugin = BetterQuestsPlugin.getInstance();
        final ProgressManager pm = plugin.getProgressManager();

        addOption(new MenuPrompt.MenuOption(CHAT_HELPER.getMessage(receiver, "menu-reset-quest"))
        {
            @Override
            public Prompt handleSelect()
            {
                Player player = offlinePlayer.getPlayer();
                if (player == null) {
                    CHAT_HELPER.sendMessage(receiver, "player-not-online");
                } else {
                    pm.resetQuest(player, progress.getQuest());
                }
                return EditProgressPrompt.this;
            }
        });
        addOption(new MenuOption(CHAT_HELPER.getMessage(receiver, "menu-abort-quest"))
        {
            @Override
            public Prompt handleSelect()
            {
                pm.abortQuest(offlinePlayer, progress.getQuest());
                CHAT_HELPER.sendMessage(receiver, "quest-aborted-player",
                        progress.getQuest().getId(), offlinePlayer.getName());
                return END_OF_CONVERSATION;
            }
        });
        addOption(new MenuOption(CHAT_HELPER.getMessage(receiver, "menu-finish-quest"))
        {
            @Override
            public Prompt handleSelect()
            {
                Player player = offlinePlayer.getPlayer();
                if (player == null)
                {
                    CHAT_HELPER.sendMessage(receiver, "player-not-online");
                    return EditProgressPrompt.this;
                }
                else
                {
                    pm.finishQuest(player, progress);
                    if (player != receiver) {
                        CHAT_HELPER.sendMessage(receiver, "quest-finished-player",
                                progress.getQuest().getId(), player.getName());
                    }
                }
                return END_OF_CONVERSATION;
            }
        });
        addOption(new MenuOption(CHAT_HELPER.getMessage(receiver, "menu-fail-quest"))
        {
            @Override
            public Prompt handleSelect()
            {
                Player player = offlinePlayer.getPlayer();
                if (player == null)
                {
                    CHAT_HELPER.sendMessage(receiver, "player-not-online");
                    return EditProgressPrompt.this;
                }
                else
                {
                    pm.failQuest(player, progress);
                    if (player != receiver) {
                        CHAT_HELPER.sendMessage(receiver, "quest-failed-player",
                                progress.getQuest().getId(), player.getName());
                    }
                }
                return END_OF_CONVERSATION;
            }
        });
//        addOption(new MenuOption(CHAT_HELPER.getMessage(receiver, "menu-advance-stage"))
//        {
//            @Override
//            public Prompt handleSelect()
//            {
//                return EditProgressPrompt.this;
//            }
//        });
        addOption(new MenuOption(CHAT_HELPER.getMessage(receiver, "menu-update-quest"))
        {
            @Override
            public Prompt handleSelect()
            {
                Player player = offlinePlayer.getPlayer();
                if (player == null)
                {
                    CHAT_HELPER.sendMessage(receiver, "player-not-online");
                }
                else
                {
                    pm.updateQuestProgress(player, progress);
                    CHAT_HELPER.sendMessage(receiver, "quest-updated-player",
                            progress.getQuest().getId(), player.getName());
                }
                return EditProgressPrompt.this;
            }
        });
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-done"), END_OF_CONVERSATION);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        String name = offlinePlayer.getName();
        if (name == null) { // pretty unlikely
            name = offlinePlayer.getUniqueId().toString();
        }
        Quest quest = progress.getQuest();
        Stage stage = progress.getCurrentStage();
        List<Stage> stages = quest.getStages();
        int stageIdx = stages.indexOf(stage);

        CHAT_HELPER.sendMessage(receiver, "progress-editor-header", quest.getId(), name);
        CHAT_HELPER.sendMessage(receiver, "progress-editor-stage-header", stageIdx + 1, stages.size());
        printOptions(receiver);
        return CHAT_HELPER.getMessage(receiver, "prompt-edit-progress");
    }
}
