/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.editors.EditListPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditStagesPrompt extends EditListPrompt<Stage>
{
    private final Quest quest;

    public EditStagesPrompt(Quest quest, Prompt parentPrompt, CommandSender sender)
    {
        super(parentPrompt);
        this.quest = quest;
    }

    @Override
    protected void addOptionFor(int index, Stage stage, CommandSender sender)
    {
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-stage", index + 1), new EditStagePrompt(quest, index, this, sender));
    }

    @Override
    protected List<Stage> getObjects()
    {
        return quest.getStages();
    }

    @Override
    protected PromptSupplier getAddPrompt(final CommandSender sender)
    {
        return new PromptSupplier()
        {
            @Override
            public Prompt get()
            {
                int index = quest.getStages().size();
                Stage stage = new Stage(quest);
                quest.appendStage(stage);
                return new EditStagePrompt(quest, index, EditStagesPrompt.this, sender);
            }
        };

    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-edit-stages");
    }

    @Override
    protected String getMsgIdNew()
    {
        return "menu-new-stage";
    }
}
