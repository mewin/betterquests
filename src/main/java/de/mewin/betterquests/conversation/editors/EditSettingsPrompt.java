/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.editors;

import de.mewin.mewinbukkitlib.conversation.acceptors.BooleanAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.BooleanAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.LongStringAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.quest.extra.Settings;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EditSettingsPrompt extends MenuPrompt
{
    private final Settings settings;
    private final Prompt parentPrompt;
    private final List<KnownSetting> knownSettings;
    private final String settingMsgPrefix;

    public EditSettingsPrompt(Settings settings, Prompt parentPrompt, List<KnownSetting> knownSettings, String settingMsgPrefix)
    {
        this.settings = settings;
        this.parentPrompt = parentPrompt;
        this.knownSettings = knownSettings;
        this.settingMsgPrefix = settingMsgPrefix;
    }

    private BooleanAcceptorPrompt buildBoolPrompt(final String name)
    {
        return new BooleanAcceptorPrompt(this, "prompt-enter-bool", new BooleanAcceptor()
        {
            @Override
            public boolean setBoolean(boolean value)
            {
                settings.setBoolean(name, value);
                return true;
            }

            @Override
            public boolean getBoolean()
            {
                return settings.getBoolean(name);
            }
        });
    }

    private IntAcceptorPrompt buildIntPrompt(final String name)
    {
        return new IntAcceptorPrompt(this, "prompt-enter-int", new IntAcceptor()
        {
            @Override
            public boolean setInt(int value)
            {
                settings.setInt(name, value);
                return true;
            }

            @Override
            public int getInt()
            {
                return settings.getInt(name);
            }
        });
    }

    private DoubleAcceptorPrompt buildDoublePrompt(final String name)
    {
        return new DoubleAcceptorPrompt(this, "prompt-enter-double", new DoubleAcceptor()
        {
            @Override
            public boolean setDouble(double value)
            {
                settings.setDouble(name, value);
                return true;
            }

            @Override
            public double getDouble()
            {
                return settings.getDouble(name);
            }
        });
    }

    private StringAcceptorPrompt buildStringPrompt(final String name)
    {
        return new StringAcceptorPrompt(this, "prompt-enter-string", new StringAcceptor()
        {
            @Override
            public boolean setString(String value)
            {
                settings.setString(name, value);
                return true;
            }

            @Override
            public String getString()
            {
                return settings.getString(name);
            }
        });
    }

    private LongStringAcceptorPrompt buildLongStringPrompt(final String name)
    {
        return new LongStringAcceptorPrompt(this, "prompt-enter-longstring", "-", new StringAcceptor()
        {
            @Override
            public boolean setString(String value)
            {
                settings.setString(name, value);
                return true;
            }

            @Override
            public String getString()
            {
                return settings.getString(name);
            }
        });
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        for (KnownSetting setting : knownSettings)
        {
            Prompt prompt = null;
            Object value = null;


            switch (setting.type)
            {
                case BOOLEAN:
                    prompt = buildBoolPrompt(setting.name);
                    break;
                case DOUBLE:
                    prompt = buildDoublePrompt(setting.name);
                    break;
                case INTEGER:
                    prompt = buildIntPrompt(setting.name);
                    break;
                case STRING:
                    prompt = buildStringPrompt(setting.name);
                    break;
                case LONG_STRING:
                    prompt = buildLongStringPrompt(setting.name);
                    break;
                default:
                    continue;
            }

            if (!settings.contains(setting.name)) {
                value = CHAT_HELPER.getMessage(receiver, "setting-unset");
            }
            else
            {
                switch (setting.type)
                {
                    case BOOLEAN:
                        value = settings.getBoolean(setting.name);
                        break;
                    case DOUBLE:
                        value = settings.getDouble(setting.name);
                        break;
                    case INTEGER:
                        value = settings.getInt(setting.name);
                        break;
                    case STRING:
                        value = settings.getString(setting.name);
                        break;
                    case LONG_STRING:
                        value = settings.getString(setting.name).split("\n")[0];
                        break;
                }
            }

            String name = CHAT_HELPER.tryGetMessage(receiver, String.format("%s-%s",
                    settingMsgPrefix, setting.name));
            if (name == null) {
                name = setting.name;
            }
            String label = CHAT_HELPER.getMessage(receiver, "menu-settings-entry", name, value);
            addSimpleOption(label, prompt);
        }
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-done"), parentPrompt);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);
        CHAT_HELPER.sendMessage(receiver, "settings-hint-unset");
        return CHAT_HELPER.getMessage(receiver, "prompt-settings-editor");
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        if (!"".equals(input) && input.charAt(0) == 'x') {
            String[] parts = input.split(" ");
            if (parts.length > 1)
            {
                try
                {
                    int index = Integer.valueOf(parts[1]) - 1;
                    if (index >= 0 && index < knownSettings.size()) {
                        settings.remove(knownSettings.get(index).name);
                        reloadMenu();
                        return this;
                    }
                }
                catch(NumberFormatException ex)
                {
                    return this;
                }
            }
        }
        return super.acceptInput(context, input);
    }
}
