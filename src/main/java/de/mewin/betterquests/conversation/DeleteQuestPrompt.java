/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.QuestRegistry;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class DeleteQuestPrompt extends StringPrompt
{
    private final BetterQuestsPlugin plugin;
    private final Quest quest;

    public DeleteQuestPrompt(BetterQuestsPlugin plugin, Quest quest)
    {
        this.plugin = plugin;
        this.quest = quest;
    }

    public DeleteQuestPrompt(BetterQuestsPlugin plugin)
    {
        this(plugin, null);
    }

    private Quest getQuest(ConversationContext context)
    {
        if (this.quest != null) {
            return this.quest;
        }

        return (Quest) context.getSessionData("quest");
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        Conversable forWhom = context.getForWhom();
        Quest quest = getQuest(context);

        CHAT_HELPER.trySendMessage(forWhom, "warn-quest-delete", quest.getId());

        return CHAT_HELPER.getMessage(forWhom, "prompt-quest-delete", quest.getId());
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        if (input.equals(CHAT_HELPER.getMessage(context.getForWhom(), "input-quest-delete")))
        {
            Quest quest = getQuest(context);
            QuestRegistry questRegistry = plugin.getQuestRegistry();

            questRegistry.removeQuest(quest);

            CHAT_HELPER.sendMessage(receiver, "quest-deleted", quest.getId());
        }
        else {
            CHAT_HELPER.sendMessage(receiver, "quest-delete-cancelled", quest.getId());
        }

        return Prompt.END_OF_CONVERSATION;
    }

}
