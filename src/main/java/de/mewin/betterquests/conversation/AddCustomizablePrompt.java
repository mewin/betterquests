/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation;

import de.mewin.betterquests.quest.Customizable;
import de.mewin.betterquests.quest.factory.Factory;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class AddCustomizablePrompt<T, S extends Customizable<T>> extends MenuPrompt
{
    protected final T base;
    private final MenuPrompt parentPrompt;

    public AddCustomizablePrompt(T base, MenuPrompt parentPrompt, CommandSender sender)
    {
        this.base = base;
        this.parentPrompt = parentPrompt;
    }

    @Override
    protected void fillOptions(final CommandSender sender)
    {
        List<? extends Factory<T, S>> factories = getFactories();

        for (final Factory<T, S> factory : factories)
        {
            addComplexOption(factory.getName(sender), new PromptSupplier() {
                @Override
                public Prompt get()
                {
                    S newObject = factory.createNew(base);
                    return newObject.createPrompt(parentPrompt, base, sender);
                }
            });
        }

        addUpdatingOption(CHAT_HELPER.getMessage(sender, "menu-back"), parentPrompt);
    }

    protected abstract List<? extends Factory<T, S>> getFactories();
}
