/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.conversations.Prompt;

/**
 *
 * @author mewin
 */
public class QuestAcceptorPrompt extends StringAcceptorPrompt
{
    public QuestAcceptorPrompt(Prompt parentPrompt, String promptid, StringAcceptor acceptor)
    {
        super(parentPrompt, promptid, acceptor);
    }
}
