/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.addons.placeholders;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;

import java.util.ArrayList;
import me.clip.placeholderapi.external.EZPlaceholderHook;
import org.bukkit.entity.Player;

/**
 *
 * @author mewin
 */
public class BQPlaceholdersHook extends EZPlaceholderHook
{
    private final BetterQuestsPlugin plugin;
    public BQPlaceholdersHook(BetterQuestsPlugin plugin)
    {
        super(plugin, "bq");

        this.plugin = plugin;
    }

    private String getActiveQuestsNames(ProgressManager pm, Player player)
    {
        PlayerProgress playerProgress = pm.getPlayerProgress(player);

        ArrayList<String> titles = new ArrayList<>();
        for (QuestProgress progress : playerProgress.getQuestProgresses().values())
        {
            titles.add(progress.getQuest().getTitle());
        }

        return String.join("§r, ", titles);
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier)
    {
        if (player == null) {
            return "";
        }

        ProgressManager pm = plugin.getProgressManager();
        switch(identifier)
        {
            case "active":
                return String.valueOf(pm.getPlayerProgress(player).getQuestProgresses().size());
            case "finished":
                return String.valueOf(pm.getPlayerProgress(player).getFinishedQuests().size());
            case "active_names":
                return getActiveQuestsNames(pm, player);
        }

        return "";
    }
}
