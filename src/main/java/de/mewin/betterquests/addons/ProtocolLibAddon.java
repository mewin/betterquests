/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.addons;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import de.mewin.betterquests.BetterQuestsAddon;
import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.util.MetaKeys;
import de.mewin.betterquests.util.Util;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author mewin
 */
public class ProtocolLibAddon extends BetterQuestsAddon implements PacketListener
{
    private final BetterQuestsPlugin plugin;
    private final ProtocolManager protocolManager;

    public ProtocolLibAddon(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
        this.protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Override
    public void onEnable()
    {
        protocolManager.addPacketListener(this);
    }

    @Override
    public void onPacketReceiving(PacketEvent event)
    {
        PacketContainer packet = event.getPacket();
        Player player = event.getPlayer();

        if (event.getPacketType() == PacketType.Play.Client.SETTINGS)
        {
            String locale = packet.getStrings().readSafely(0);

            if (locale != null) {
                Util.setPlayerMeta(player, MetaKeys.LOCALE, locale);
            }
        }
    }

    @Override
    public Plugin getPlugin()
    {
        return plugin;
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist()
    {
        return ListeningWhitelist.newBuilder()
                .gamePhase(GamePhase.PLAYING)
                .types(PacketType.Play.Client.SETTINGS)
            .build();
    }

    @Override
    public void onPacketSending(PacketEvent pe) {}

    @Override
    public ListeningWhitelist getSendingWhitelist()
    {
        return ListeningWhitelist.EMPTY_WHITELIST;
    }
}
