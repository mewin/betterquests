/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.addons;

import de.mewin.betterquests.BetterQuestsAddon;
import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.addons.placeholders.BQPlaceholdersHook;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PlaceholderAPIAddon extends BetterQuestsAddon
{
    private final BetterQuestsPlugin plugin;

    public PlaceholderAPIAddon(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public void onEnable()
    {
        CHAT_HELPER.setHavePlaceholdersAPI(true); // TODO: not my job anymore
        new BQPlaceholdersHook(plugin).hook();
    }
}
