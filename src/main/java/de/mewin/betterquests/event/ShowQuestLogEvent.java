/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 *
 * @author mewin
 */
public class ShowQuestLogEvent extends PlayerEvent implements Cancellable
{
    private boolean cancelled;

    public ShowQuestLogEvent(Player who)
    {
        super(who);
        cancelled = false;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel)
    {
        cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}
