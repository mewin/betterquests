/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.event;

import de.mewin.betterquests.progress.QuestAdvertisement;
import de.mewin.betterquests.quest.Trigger;
import de.mewin.betterquests.quest.event.QuestEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 *
 * @author mewin
 */
public class AdvertiseQuestEvent extends QuestEvent implements Cancellable
{
    private final QuestAdvertisement advertisement;
    private final Player player;
    private final Trigger trigger;
    private boolean cancelled;

    public AdvertiseQuestEvent(QuestAdvertisement advertisement, Player player, Trigger trigger)
    {
        super(advertisement.getQuest());
        this.advertisement = advertisement;
        this.player = player;
        this.trigger = trigger;
        this.cancelled = false;
    }

    public QuestAdvertisement getAdvertisement()
    {
        return advertisement;
    }

    public Player getPlayer()
    {
        return player;
    }

    public Trigger getTrigger()
    {
        return trigger;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel)
    {
        cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}
