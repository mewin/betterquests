package de.mewin.betterquests.features;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.progress.PlayerProgress;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.progress.event.TrackedQuestChangedEvent;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.Quest;
import de.mewin.betterquests.quest.Stage;
import de.mewin.betterquests.quest.event.StageChangeEvent;
import de.mewin.betterquests.util.ConfigKeys;
import de.mewin.betterquests.util.MetaKeys;
import de.mewin.betterquests.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.List;

public class TrackQuestListener implements Listener, Runnable
{
    private final BetterQuestsPlugin plugin;

    public TrackQuestListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    private void updateCompassTarget(Player player, Stage stage)
    {
        if (!plugin.getConfig().getBoolean(ConfigKeys.COMPASS_TRACKING, true)) {
            return;
        }

        Location loc = null;
        if (stage != null)
        {
            for (Objective objective : stage.getObjectives())
            {
                loc = objective.getTargetLocation();
                if (loc == null) {
                    break;
                }
            }
        }

        if (loc == null) {
            loc = player.getBedSpawnLocation();
        }
        if (loc == null) {
            loc = player.getWorld().getSpawnLocation();
        }
        player.setCompassTarget(loc);
    }

    private Scoreboard getPlayerScoreboard(Player player)
    {
        Scoreboard scoreboard;
        MetadataValue meta = Util.getPlayerMeta(player, MetaKeys.SCOREBOARD);
        if (meta == null)
        {
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            Util.setPlayerMeta(player, MetaKeys.SCOREBOARD, scoreboard);
        }
        else {
            scoreboard = (Scoreboard) meta.value();
        }

        return scoreboard;
    }

    private void updateScorebard(Player player, Stage stage)
    {
        if (!plugin.getConfig().getBoolean(ConfigKeys.SCOREBOARD_TRACKING, false)) { // todo: default should be true when this sh*t finally works
            return;
        }

        Scoreboard scoreboard = getPlayerScoreboard(player);
        ProgressManager pm = plugin.getProgressManager();
        QuestProgress progress = pm.getQuestProgress(player, stage.getQuest());

        // todo: add plugin to manage multiple scoreboards
        player.setScoreboard(scoreboard);
        scoreboard.clearSlot(DisplaySlot.SIDEBAR);

        org.bukkit.scoreboard.Objective scoreboardObjective = scoreboard.getObjective(DisplaySlot.SIDEBAR);
        List<Objective> objectives = stage.getObjectives();

        if (scoreboardObjective == null)
        {
            String title = stage.getQuest().getTitle();
            if (title.length() > 16) {
                title = title.substring(0, 16);
            }
            scoreboardObjective = scoreboard.registerNewObjective(title, "dummy");
            scoreboardObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        }

        for (int i = 0; i < objectives.size(); ++i)
        {
            Objective objective = objectives.get(i);
            Score score = scoreboardObjective.getScore(objective.getDescription(player, progress.getObjectiveProgress(objective)));
            score.setScore(objectives.size() - i);
        }
    }

    private void resetPlayerScoreboard(Player player, Stage stage)
    {
        Scoreboard scoreboard = getPlayerScoreboard(player);
        scoreboard.clearSlot(DisplaySlot.SIDEBAR);
    }

    private void update(Player player, Stage stage)
    {
        updateCompassTarget(player, stage);
        updateScorebard(player, stage);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onTrackedQuestChanged(TrackedQuestChangedEvent event)
    {
        Player player = event.getPlayer();
        Quest quest = event.getNewTrackedQuest();
        ProgressManager pm = plugin.getProgressManager();

        if (quest == null) {
            update(player, null);
        }
        else
        {
            QuestProgress questProgress = pm.getQuestProgress(player, quest);
            update(player, questProgress == null ? null : questProgress.getCurrentStage());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuestStageChanged(StageChangeEvent event)
    {
        if (plugin.getConfig().getBoolean(ConfigKeys.SCOREBOARD_TRACKING, true)) {
            resetPlayerScoreboard(event.getPlayer(), event.getNewStage());
        }
        update(event.getPlayer(), event.getNewStage());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        ProgressManager pm = plugin.getProgressManager();
        PlayerProgress playerProgress = pm.getPlayerProgress(player);
        Quest quest = playerProgress.getTrackedQuest();

        if (quest != null)
        {
            QuestProgress questProgress = pm.getQuestProgress(player, quest);

            // only works if delayed
            Bukkit.getScheduler().runTaskLater(plugin, () ->
                update(player, questProgress == null ? null : questProgress.getCurrentStage()),
                20L);

        }
    }

    @Override
    public void run()
    {
        // update scoreboard
        ProgressManager pm = plugin.getProgressManager();

        for (Player player : Bukkit.getOnlinePlayers())
        {
            PlayerProgress playerProgress = pm.getPlayerProgress(player);
            Quest quest = playerProgress.getTrackedQuest();

            if (quest != null)
            {
                QuestProgress questProgress = pm.getQuestProgress(player, quest);
                updateScorebard(player, questProgress.getCurrentStage());
            }
            else
            {
                updateScorebard(player, null);
            }
        }
    }
}
