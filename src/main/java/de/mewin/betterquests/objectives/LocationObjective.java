/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.LocationAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import de.mewin.betterquests.objectives.conversation.LocationObjectivePrompt;
import de.mewin.betterquests.objectives.factories.LocationObjectiveFactory;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.world.LocationProxy;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class LocationObjective extends Objective implements DoubleAcceptor, LocationAcceptor,
                                                            EnumAcceptor<LocationObjective.Type>,
                                                            StringAcceptor
{
    private LocationProxy locationProxy;
    private String locationName;
    private double distanceSq;
    private Type type;

    public LocationObjective(ObjectivesHolder holder)
    {
        super(holder);
        locationProxy = new LocationProxy();
        locationName = "location";
        distanceSq = 0.0;
        type = Type.REACH;
    }

    public LocationObjective(ObjectivesHolder holder, LocationProxy location, String locationName,
                             double distanceSq, Type type)
    {
        super(holder);
        this.locationProxy = location;
        this.locationName = locationName;
        this.distanceSq = distanceSq;
        this.type = type;
    }

    public double getDistanceSq()
    {
        return distanceSq;
    }

    public void setDistanceSq(double distanceSq)
    {
        this.distanceSq = distanceSq;
    }

    public double getDistance()
    {
        return Math.sqrt(distanceSq);
    }

    public void setDistance(double distance)
    {
        this.distanceSq = distance * distance;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    @Override
    public Location getTargetLocation() {
        return getLocation();
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        if (type == Type.REACH) {
            return data != null && (data instanceof Boolean) && (Boolean) data;
        }

        Location playerLoc = player.getLocation();
        Location location = locationProxy.getLocation();

        if (!playerLoc.getWorld().equals(location.getWorld())) {
            return false;
        }

        return playerLoc.distanceSquared(location) <= distanceSq;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        switch (type)
        {
            case BE_AT:
                return CHAT_HELPER.getMessage(player, "objective-location-description-be-at",
                        CHAT_HELPER.formatPlayerString(locationName, player));
            case REACH:
                return CHAT_HELPER.getMessage(player, "objective-location-description-reach",
                        CHAT_HELPER.formatPlayerString(locationName, player));
        }
        return "";
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        Location location = locationProxy.getLocation();

        return CHAT_HELPER.getMessage(sender, "objective-location", type.name(),
                locationProxy.getX(), locationProxy.getY(), locationProxy.getZ(), location.getWorld().getName());
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new LocationObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return LocationObjectiveFactory.MY_ID;
    }

    @Override
    public double getDouble()
    {
        return Math.sqrt(distanceSq);
    }

    @Override
    public boolean setDouble(double value)
    {
        if (value < 0.0) {
            return false;
        }

        distanceSq = value * value;

        return true;
    }

    @Override
    public Location getLocation()
    {
        return locationProxy.getLocation();
    }

    @Override
    public boolean setLocation(Location location)
    {
        this.locationProxy = new LocationProxy(location);
        return true;
    }

    @Override
    public Type getEnum()
    {
        return type;
    }

    @Override
    public boolean setEnum(Type value)
    {
        type = value;
        return true;
    }

    @Override
    public boolean setString(String value)
    {
        if ("".equals(value)) {
            return false;
        }

        locationName = value;

        return true;
    }

    @Override
    public String getString()
    {
        return locationName;
    }

    public static enum Type
    {
        BE_AT,
        REACH
    }
}
