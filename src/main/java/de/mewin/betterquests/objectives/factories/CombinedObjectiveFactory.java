/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.io.YamlQuestRegistryBackend;
import de.mewin.betterquests.objectives.CombinedObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CombinedObjectiveFactory extends ObjectiveFactory
{

    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        CombinedObjective combinedObjective = new CombinedObjective(holder.getStage());
        holder.addObjective(combinedObjective);
        return combinedObjective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-combined-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        CombinedObjective.Type type = CombinedObjective.Type.OR;
        Object tmp;

        if ((tmp = yaml.get("combination")) != null)
        {
            try {
                type = CombinedObjective.Type.valueOf(tmp.toString());
            } catch(IllegalArgumentException ex) {}
        }

        CombinedObjective combinedObjective = new CombinedObjective(holder.getStage(), type);

        if ((tmp = yaml.get("objectives")) != null && tmp instanceof List)
        {
            for (Object obj : (List) tmp)
            {
                if (obj instanceof Map)
                {
                    Objective objective = YamlQuestRegistryBackend.objectiveFromYaml(combinedObjective, (Map<String, Object>) obj);

                    combinedObjective.addObjective(objective);
                }
            }
        }

        return combinedObjective;
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        CombinedObjective combinedObjective = (CombinedObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("combination", combinedObjective.getType().name());

        ArrayList list = new ArrayList();
        for (Objective obj : combinedObjective.getObjectives())
        {
            Map<String, Object> objYaml = YamlQuestRegistryBackend.objectiveToYaml(obj);
            if (objYaml != null) {
                list.add(objYaml);
            }
        }

        map.put("objectives", list);

        return map;
    }

    public static final String MY_ID = "combined";
}
