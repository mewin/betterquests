/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.objectives.SurvivalObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class SurvivalObjectiveFactory extends ObjectiveFactory
{

    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        SurvivalObjective objective = new SurvivalObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-survival-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        return new SurvivalObjective(holder);
    }

    @Override
    public Map<String, Object> toYaml(Objective object) throws IOException
    {
        return new HashMap<>();
    }

    public static final String MY_ID = "survival";
}
