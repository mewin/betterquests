/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.objectives.LocationObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;
import de.mewin.mewinbukkitlib.world.LocationProxy;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class LocationObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        LocationObjective objective = new LocationObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-location-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        LocationProxy location = new LocationProxy();
        String locationName = "location";
        double distanceSq = 4.0;
        LocationObjective.Type type = LocationObjective.Type.REACH;
        Object tmp;

        if ((tmp = yaml.get("location")) != null && (tmp instanceof Map)) {
            location = Serializer.deserializeLocation((Map<String, Object>) tmp);
        }

        if ((tmp = yaml.get("location-name")) != null) {
            locationName = tmp.toString();
        }

        if ((tmp = yaml.get("distance-sq")) != null && (tmp instanceof Number)) {
            distanceSq = ((Number) tmp).doubleValue();
        }

        if ((tmp = yaml.get("objective-type")) != null)
        {
            try {
                type = LocationObjective.Type.valueOf(tmp.toString());
            } catch(IllegalArgumentException ex) {}
        }

        return new LocationObjective(holder, location, locationName, distanceSq, type);
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        LocationObjective locationObjective = (LocationObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("location", Serializer.serializeLocation(locationObjective.getLocation()));
        map.put("location-name", locationObjective.getLocationName());
        map.put("distance-sq", locationObjective.getDistanceSq());
        map.put("objective-type", locationObjective.getType().name());

        return map;
    }

    public static final String MY_ID = "location";
}
