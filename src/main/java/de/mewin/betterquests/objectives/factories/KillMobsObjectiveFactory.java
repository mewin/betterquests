/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.objectives.KillMobsObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class KillMobsObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        KillMobsObjective objective = new KillMobsObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-kill-mobs-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        EntityType type = EntityType.ZOMBIE;
        int amount = 1;
        Object tmp;

        if ((tmp = yaml.get("entity-type")) != null)
        {
            try {
                type = EntityType.valueOf(tmp.toString());
            } catch(IllegalArgumentException ex) {}
        }

        if ((tmp = yaml.get("amount")) != null && (tmp instanceof Number)) {
            amount = ((Number) tmp).intValue();
        }

        return new KillMobsObjective(holder, type, amount);
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        KillMobsObjective killMobsObjective = (KillMobsObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("entity-type", killMobsObjective.getType().name());
        map.put("amount", killMobsObjective.getAmount());

        return map;
    }

    public static final String MY_ID = "kill-mobs";
}
