/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.objectives.ItemObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;
import de.mewin.betterquests.io.Serializer;
import de.mewin.betterquests.quest.ObjectivesHolder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemObjectiveFactory extends ObjectiveFactory
{
    public static final String MY_ID = "item-stack";

    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        ItemObjective objective = new ItemObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-item-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        Object itemsObj = yaml.get("items");
        if (!(itemsObj instanceof Map)) {
            return new ItemObjective(holder);
        }
        Map<String, Object> items = (Map<String, Object>) itemsObj;

        return new ItemObjective(holder, Serializer.deserializeItemStack(items));
    }

    @Override
    public Map<String, Object> toYaml(Objective object) throws IOException
    {
        HashMap<String, Object> map = new HashMap<>();
        ItemObjective objective = (ItemObjective) object;

        ItemStack items = objective.getItemStack();

        if (items != null) {
            map.put("items", Serializer.serializeItemStack(items));
        }

        return map;
    }

}
