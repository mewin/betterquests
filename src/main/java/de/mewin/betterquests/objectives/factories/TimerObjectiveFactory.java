/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.objectives.TimerObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TimerObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        TimerObjective objective = new TimerObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-timer-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder base, Map<String, Object> yaml) throws IOException
    {
        int seconds = 0;
        TimerObjective.Type type = TimerObjective.Type.SUCCESS;
        Object tmp;

        if ((tmp = yaml.get("seconds")) != null && (tmp instanceof Number)) {
            seconds = ((Number) tmp).intValue();
        }

        if ((tmp = yaml.get("type")) != null)
        {
            try {
                type = TimerObjective.Type.valueOf(tmp.toString());
            } catch(IllegalArgumentException ex) {}
        }

        return new TimerObjective(base, seconds, type);
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        TimerObjective timerObjective = (TimerObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("seconds", timerObjective.getSeconds());
        map.put("type", timerObjective.getType().name());

        return map;
    }

    @Override
    public Object progressFromYaml(Object yaml)
    {
        int remaining = 0;

        if (yaml != null && (yaml instanceof Number)) {
            remaining = ((Number) yaml).intValue();
        }

        return System.currentTimeMillis() + remaining;
    }

    @Override
    public Object progressToYaml(Object data)
    {
        int remaining = (int) ((Long) data - System.currentTimeMillis());

        return remaining;
    }

    public static final String MY_ID = "timer";
}
