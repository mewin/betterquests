/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.factories;

import de.mewin.betterquests.objectives.PasswordObjective;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.factory.ObjectiveFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PasswordObjectiveFactory extends ObjectiveFactory
{
    @Override
    public Objective createNew(ObjectivesHolder holder)
    {
        PasswordObjective objective = new PasswordObjective(holder);
        holder.addObjective(objective);
        return objective;
    }

    @Override
    public String getName(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-password-factory");
    }

    @Override
    public Objective fromYaml(ObjectivesHolder holder, Map<String, Object> yaml) throws IOException
    {
        String message = "";
        String password = "";
        Object tmp;

        if ((tmp = yaml.get("message")) != null) {
            message = tmp.toString();
        }

        if ((tmp = yaml.get("password")) != null) {
            password = tmp.toString();
        }

        return new PasswordObjective(holder, message, password);
    }

    @Override
    public Map<String, Object> toYaml(Objective objective) throws IOException
    {
        PasswordObjective passwordObjective = (PasswordObjective) objective;

        HashMap<String, Object> map = new HashMap<>();

        map.put("message", passwordObjective.getMessage());
        map.put("password", passwordObjective.getPassword());

        return map;
    }

    public static final String MY_ID = "password";
}
