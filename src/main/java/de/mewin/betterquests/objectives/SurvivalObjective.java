/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.betterquests.objectives.conversation.SurvivalObjectivePrompt;
import de.mewin.betterquests.objectives.factories.SurvivalObjectiveFactory;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class SurvivalObjective extends Objective
{
    public SurvivalObjective(ObjectivesHolder holder)
    {
        super(holder);
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        return true;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        return CHAT_HELPER.getMessage(player, "objective-survival-description");
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-survival");
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new SurvivalObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return SurvivalObjectiveFactory.MY_ID;
    }
}
