/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.betterquests.objectives.conversation.ItemObjectivePrompt;
import de.mewin.betterquests.objectives.factories.ItemObjectiveFactory;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemStackAcceptor;
import de.mewin.mewinbukkitlib.util.Items;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemObjective extends Objective implements ItemStackAcceptor
{
    private ItemStack items;

    public ItemObjective(ObjectivesHolder holder)
    {
        super(holder);
        items = new ItemStack(Material.AIR);
    }

    public ItemObjective(ObjectivesHolder holder, ItemStack items)
    {
        super(holder);
        this.items = items;
    }

    @Override
    public ItemStack getItemStack()
    {
        return items;
    }

    @Override
    public void setItemStack(ItemStack itemStack)
    {
        items = new ItemStack(itemStack);
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        return countItems(player) >= items.getAmount();
    }

    @Override
    public double getProgress(Player player, Object data)
    {
        int amount = countItems(player);

        return Math.min((double) amount / (double) items.getAmount(), 1.0);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-item");
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder holder, CommandSender sender)
    {
        return new ItemObjectivePrompt(this, holder, parentPrompt, sender);
    }

    @Override
    public String getFactoryId()
    {
        return ItemObjectiveFactory.MY_ID;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        int numItems = countItems(player);

        return CHAT_HELPER.getMessage(player, "objective-item-description", items.getType().toString(),
                                     numItems, items.getAmount());
    }

    @Override
    public void onStageEnd(Player player, Object data)
    {
        if (getSettings().getBoolean(TAKE_ITEMS, false))
        {
            boolean ignoreDamage = getSettings().getBoolean(IGNORE_META, false);
            Items.removeFromInventory(player.getInventory(), items, ignoreDamage);
        }
    }

    private int countItems(Player player)
    {
        int amount = 0;
        boolean ignoreDamage = getSettings().getBoolean(IGNORE_META, false);

        for (ItemStack stack : player.getInventory().getContents()) {
            if (Items.compareStacks(stack, items, ignoreDamage)) {
                amount += stack.getAmount();
            }
        }

        return amount;
    }

    public static final String IGNORE_META = "ignore-meta";
    public static final String TAKE_ITEMS = "take-items";
}
