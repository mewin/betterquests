/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.objectives.LocationObjective;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.mewinbukkitlib.util.Pair;
import java.util.HashSet;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author mewin
 */
public class LocationObjectiveListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public LocationObjectiveListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event)
    {
        Player player = event.getPlayer();
        ProgressManager progressManager = plugin.getProgressManager();
        List<Pair<QuestProgress, LocationObjective>> progresses =
                progressManager.findProgressByObjectiveType(player, LocationObjective.class);

        HashSet<QuestProgress> progressesToUpdate = new HashSet<>();

        for (Pair<QuestProgress, LocationObjective> pair : progresses)
        {
            QuestProgress progress = pair.getFirst();
            LocationObjective objective = pair.getSecond();

            if (objective.getType() == LocationObjective.Type.REACH)
            {
                Location playerLoc = event.getTo();
                Location loc = objective.getLocation();
                double distanceSq = objective.getDistanceSq();

                if (playerLoc.getWorld().equals(loc.getWorld()) && playerLoc.distanceSquared(loc) <= distanceSq) {
                    progress.setObjectiveProgress(objective, true);
                }
            }

            progressesToUpdate.add(progress);
        }

        for (QuestProgress progress : progressesToUpdate) {
            progressManager.updateQuestProgress(player, progress);
        }
    }
}
