/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.quest.event.StageChangeEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author mewin
 */
public class TimerObjectiveListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public TimerObjectiveListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {

    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event)
    {

    }

    @EventHandler
    public void onStageChange(StageChangeEvent event)
    {

    }
}
