/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.objectives.ItemObjective;
import de.mewin.betterquests.progress.ProgressManager;
import java.util.HashSet;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 *
 * @author mewin
 */
public class ItemObjectiveListener implements Listener
{
    private final BetterQuestsPlugin plugin;
    private final HashSet<Player> playersToUpdate;
    private boolean taskScheduled;

    public ItemObjectiveListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
        this.playersToUpdate = new HashSet<>();
        this.taskScheduled = false;
    }

    private synchronized void handleInventoryChange(Player player)
    {
        playersToUpdate.add(player);

        if (!taskScheduled) {
            taskScheduled = true;
            Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                @Override
                public void run()
                {
                    updateQuests();
                }
            }, 10);
        }
    }

    private synchronized void updateQuests()
    {
        ProgressManager progressManager = plugin.getProgressManager();

        for (Player player : playersToUpdate) {
            progressManager.updateQuestProgressByObjectiveType(player, ItemObjective.class);
        }
        playersToUpdate.clear();
        taskScheduled = false;
    }

    @EventHandler
    public void onBucketFill(PlayerBucketFillEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerEggThrow(PlayerEggThrowEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerItemBreak(PlayerItemBreakEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerItemConsume(PlayerItemConsumeEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerPickupArrow(PlayerPickupArrowEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event)
    {
        handleInventoryChange(event.getPlayer());
    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event)
    {
        HumanEntity player;
        if ((player = event.getWhoClicked()) instanceof Player) {
            handleInventoryChange((Player) player);
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event)
    {
        HumanEntity player;
        if ((player = event.getPlayer()) instanceof Player) {
            handleInventoryChange((Player) player);
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event)
    {
        HumanEntity player;
        if ((player = event.getWhoClicked()) instanceof Player) {
            handleInventoryChange((Player) player);
        }
    }
}
