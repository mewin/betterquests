/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.objectives.PasswordObjective;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.mewinbukkitlib.util.Pair;
import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author mewin
 */
public class PasswordObjectiveListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public PasswordObjectiveListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event)
    {
        Player player = event.getPlayer();
        ProgressManager progressManager = plugin.getProgressManager();
        List<Pair<QuestProgress, PasswordObjective>> progresses =
                progressManager.findProgressByObjectiveType(player, PasswordObjective.class);

        for (Pair<QuestProgress, PasswordObjective> pair : progresses)
        {
            QuestProgress progress = pair.getFirst();
            PasswordObjective objective = pair.getSecond();
            String msg = event.getMessage();
            String password = objective.getPassword();

            if (objective.getSettings().getBoolean(PasswordObjective.IGNORE_CASE, false))
            {
                msg = msg.toLowerCase();
                password = password.toLowerCase();
            }

            if (msg.equals(password))
            {
                progress.setObjectiveProgress(objective, true);
                progressManager.updateQuestProgress(player, progress);
                event.setCancelled(true);
            }
        }
    }
}
