/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.objectives.KillMobsObjective;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.mewinbukkitlib.util.Pair;
import java.util.List;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.projectiles.ProjectileSource;

/**
 *
 * @author mewin
 */
public class KillMobsObjectiveListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public KillMobsObjectiveListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    private void onMobKilledByPlayer(Entity entity, Player player)
    {
        ProgressManager progressManager = plugin.getProgressManager();
        List<Pair<QuestProgress, KillMobsObjective>> progresses = progressManager.findProgressByObjectiveType(player, KillMobsObjective.class);

        for (Pair<QuestProgress, KillMobsObjective> pair : progresses)
        {
            QuestProgress progress = pair.getFirst();
            KillMobsObjective objective = pair.getSecond();

            if (objective.getType() != entity.getType()) {
                continue;
            }

            int count = (Integer) progress.getObjectiveProgress(objective);
            progress.setObjectiveProgress(objective, count + 1);

            progressManager.updateQuestProgress(player, progress);
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event)
    {
        LivingEntity entity = event.getEntity();
        EntityDamageEvent lastDamageCause = entity.getLastDamageCause();

        if (lastDamageCause == null || lastDamageCause.isCancelled()
                || !(lastDamageCause instanceof EntityDamageByEntityEvent)) {
            return;
        }

        EntityDamageByEntityEvent dmgEvent = (EntityDamageByEntityEvent) lastDamageCause;
        Entity damager = dmgEvent.getDamager();

        if (damager instanceof Player) {
            onMobKilledByPlayer(entity, (Player) damager);
        }
        else if (damager instanceof Projectile)
        {
            Projectile projectile = (Projectile) damager;
            ProjectileSource shooter = projectile.getShooter();

            if (shooter instanceof Player) {
                onMobKilledByPlayer(entity, (Player) shooter);
            }
        }
        else if (damager instanceof TNTPrimed)
        {
            TNTPrimed tnt = (TNTPrimed) damager;
            Entity source = tnt.getSource();

            if (source instanceof Player) {
                onMobKilledByPlayer(entity, (Player) source);
            }
        }
    }
}
