/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.listeners;

import de.mewin.betterquests.BetterQuestsPlugin;
import de.mewin.betterquests.objectives.SurvivalObjective;
import de.mewin.betterquests.progress.ProgressManager;
import de.mewin.betterquests.progress.QuestProgress;
import de.mewin.betterquests.quest.Quest;
import de.mewin.mewinbukkitlib.util.Pair;
import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 *
 * @author mewin
 */
public class SurvivalObjectiveListener implements Listener
{
    private final BetterQuestsPlugin plugin;

    public SurvivalObjectiveListener(BetterQuestsPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        Player player = event.getEntity();
        ProgressManager progressManager = plugin.getProgressManager();
        List<Pair<QuestProgress, SurvivalObjective>> objectives = progressManager.findProgressByObjectiveType(player, SurvivalObjective.class);

        for (Pair<QuestProgress, SurvivalObjective> ele : objectives) {
            QuestProgress progress = ele.getFirst();

            progressManager.failQuest(player, progress);
        }
    }
}
