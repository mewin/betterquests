/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.betterquests.objectives.conversation.TimerObjectivePrompt;
import de.mewin.betterquests.objectives.factories.TimerObjectiveFactory;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TimerObjective extends Objective implements IntAcceptor, EnumAcceptor<TimerObjective.Type>
{
    private int seconds;
    private Type type;

    public TimerObjective(ObjectivesHolder holder)
    {
        super(holder);
        seconds = 0;
        type = Type.SUCCESS;
    }

    public TimerObjective(ObjectivesHolder holder, int seconds, Type type)
    {
        super(holder);
        this.seconds = seconds;
        this.type = type;
    }

    public int getSeconds()
    {
        return seconds;
    }

    public void setSeconds(int seconds)
    {
        this.seconds = seconds;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    public Object initProgress(Player player)
    {
        return System.currentTimeMillis() + (1000 * seconds);
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        if (type == Type.FAILURE) {
            return true;
        }

        return (Long) data <= System.currentTimeMillis();
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        long diff = System.currentTimeMillis() - (Long) data;
        return CHAT_HELPER.getMessage(player, "objective-timer-description", diff / 1000);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        if (type == Type.SUCCESS) {
            return CHAT_HELPER.getMessage(sender, "objective-timer-success", seconds);
        } else {
            return CHAT_HELPER.getMessage(sender, "objective-timer-fail", seconds);
        }
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new TimerObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return TimerObjectiveFactory.MY_ID;
    }

    @Override
    public int getInt()
    {
        return seconds;
    }

    @Override
    public boolean setInt(int value)
    {
        if (value < 0) {
            return false;
        }

        seconds = value;

        return true;
    }

    @Override
    public Type getEnum()
    {
        return type;
    }

    @Override
    public boolean setEnum(Type value)
    {
        type = value;
        return true;
    }

    public static enum Type
    {
        SUCCESS,
        FAILURE
    }
}
