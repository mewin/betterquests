/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.betterquests.objectives.conversation.KillMobsObjectivePrompt;
import de.mewin.betterquests.objectives.factories.KillMobsObjectiveFactory;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptor;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class KillMobsObjective extends Objective implements IntAcceptor, EnumAcceptor<EntityType>
{
    private EntityType type;
    private int amount;

    public KillMobsObjective(ObjectivesHolder holder)
    {
        super(holder);
        type = EntityType.ZOMBIE;
        amount = 1;
    }

    public KillMobsObjective(ObjectivesHolder holder, EntityType type, int count)
    {
        super(holder);
        this.type = type;
        this.amount = count;
    }

    public EntityType getType()
    {
        return type;
    }

    public void setType(EntityType type)
    {
        this.type = type;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    @Override
    public Object initProgress(Player player)
    {
        return 0;
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        return (Integer) data >= amount;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        return CHAT_HELPER.getMessage(player, "objective-kill-mobs-description", type.name(), (Integer) data, amount);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-kill-mobs", amount, type.name());
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new KillMobsObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return KillMobsObjectiveFactory.MY_ID;
    }

    @Override
    public boolean setInt(int value)
    {
        if (value < 1) {
            return false;
        }

        amount = value;

        return true;
    }

    @Override
    public int getInt()
    {
        return amount;
    }

    @Override
    public EntityType getEnum()
    {
        return type;
    }

    @Override
    public boolean setEnum(EntityType value)
    {
        type = value;
        return true;
    }

}
