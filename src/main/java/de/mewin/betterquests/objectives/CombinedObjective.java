/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.betterquests.objectives.conversation.CombinedObjectivePrompt;
import de.mewin.betterquests.objectives.factories.CombinedObjectiveFactory;
import de.mewin.betterquests.progress.ObjectiveProgressAcceptor;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.Stage;
import de.mewin.betterquests.quest.event.ObjectiveAddedEvent;
import de.mewin.betterquests.quest.event.ObjectiveRemovedEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CombinedObjective extends Objective implements ObjectivesHolder, ObjectiveProgressAcceptor, EnumAcceptor<CombinedObjective.Type>
{
    private final ArrayList<Objective> objectives;
    private Type type;

    public CombinedObjective(ObjectivesHolder holder)
    {
        super(holder);
        this.objectives = new ArrayList<>();
        this.type =  Type.OR;
    }

    public CombinedObjective(ObjectivesHolder holder, Type type)
    {
        super(holder);
        this.objectives = new ArrayList<>();
        this.type = type;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    @Override
    public Location getTargetLocation() {
        for (Objective objective : objectives)
        {
            Location loc = objective.getTargetLocation();
            if (loc != null) {
                return loc;
            }
        }
        return null;
    }

    @Override
    public Object initProgress(Player player)
    {
        HashMap<String, Object> map = new HashMap<>();

        for (Objective objective : objectives) {
            map.put(objective.getUuid().toString(), objective.initProgress(player));
        }

        return map;
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        Map<String, Object> map = (Map<String, Object>) data;

        switch (type)
        {
            case AND:
                for (Objective objective : objectives) {
                    Object objData = map.get(objective.getUuid().toString());
                    if (!objective.testObjective(player, objData)) {
                        return false;
                    }
                }
                return true;
            case OR:
                for (Objective objective : objectives)
                {
                    Object objData = map.get(objective.getUuid().toString());
                    if (objective.testObjective(player, objData)) {
                        return true;
                    }
                }
                return false;
        }
        return true;
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        Map<String, Object> map = (Map<String, Object>) data;
        String connector = "";

        switch(type)
        {
            case AND:
                connector = CHAT_HELPER.getMessage(player, "objective-combined-and");
                break;
            case OR:
                connector = CHAT_HELPER.getMessage(player, "objective-combined-or");
                break;
        }

        ArrayList<String> descriptions = new ArrayList<>();

        for (Objective objective : objectives)
        {
            Object objData = map.get(objective.getUuid().toString());
            descriptions.add(objective.getDescription(player, objData));
        }

        return String.join(" " + connector + " ", descriptions);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-combined", type.name(), objectives.size());
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder holder, CommandSender sender)
    {
        return new CombinedObjectivePrompt(this, holder, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return CombinedObjectiveFactory.MY_ID;
    }

    @Override
    public void addObjective(Objective objective)
    {
        objectives.add(objective);
        Bukkit.getPluginManager().callEvent(new ObjectiveAddedEvent(getStage().getQuest(), getStage(), objective));
    }

    @Override
    public void removeObjective(Objective objective)
    {
        if (objectives.remove(objective))
        {
            objective.onRemove();
            Bukkit.getPluginManager().callEvent(new ObjectiveRemovedEvent(getStage().getQuest(), getStage(), objective));
        }
    }

    @Override
    public void onRemove()
    {
        for (Objective objective : objectives)
        {
            objective.onRemove();
            Bukkit.getPluginManager().callEvent(new ObjectiveRemovedEvent(getStage().getQuest(), getStage(), objective));
        }
    }

    @Override
    public void onStageEnd(Player player, Object data)
    {
        Map<String, Object> map = (Map<String, Object>) data;
        for (Objective objective : objectives)
        {
            Object objData = map.get(objective.getUuid().toString());
            if (objective.testObjective(player, objData)) {
                objective.onStageEnd(player, objData);
            }
        }
    }

    @Override
    public List<Objective> getObjectives()
    {
        return Collections.unmodifiableList(objectives);
    }

    @Override
    public Stage getStage()
    {
        return getHolder().getStage();
    }

    @Override
    public Object getObjectiveProgress(Objective objective, Object data)
    {
        Map<String, Object> map = (Map<String, Object>) data;

        Object res;

        for (Objective subObjective : objectives)
        {
            if (subObjective instanceof ObjectiveProgressAcceptor
                    && (res = ((ObjectiveProgressAcceptor) subObjective)
                            .getObjectiveProgress(objective, map.get(subObjective.getUuid().toString()))) != null) {
                return res;
            }
        }

        if (!objectives.contains(objective)) {
            return null;
        }

        return map.get(objective.getUuid().toString());
    }

    @Override
    public boolean setObjectiveProgress(Objective objective, Object progress, Object data)
    {
        Map<String, Object> map = (Map<String, Object>) data;

        for (Objective subObjective : objectives)
        {
            if (subObjective instanceof ObjectiveProgressAcceptor
                    && ((ObjectiveProgressAcceptor) subObjective).setObjectiveProgress(objective, progress, map.get(subObjective.getUuid().toString()))) {
                return true;
            }
        }

        if (!objectives.contains(objective)) {
            return false;
        }

        map.put(objective.getUuid().toString(), progress);

        return true;
    }

    @Override
    public Type getEnum()
    {
        return type;
    }

    @Override
    public boolean setEnum(Type value)
    {
        type = value;
        return true;
    }

    public static enum Type
    {
        AND,
        OR
    }
}
