/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives;

import de.mewin.betterquests.objectives.conversation.PasswordObjectivePrompt;
import de.mewin.betterquests.objectives.factories.PasswordObjectiveFactory;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PasswordObjective extends Objective
{

    private String message;
    private String password;

    public PasswordObjective(ObjectivesHolder holder)
    {
        super(holder);
        message = "";
        password = "";
    }

    public PasswordObjective(ObjectivesHolder holder, String message, String password)
    {
        super(holder);
        this.message = message;
        this.password = password;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public StringAcceptor getPasswordAcceptor()
    {
        return new StringAcceptor()
        {
            @Override
            public String getString()
            {
                return password;
            }

            @Override
            public boolean setString(String value)
            {
                password = value;
                return true;
            }
        };
    }

    public StringAcceptor getMessageAcceptor()
    {
        return new StringAcceptor()
        {
            @Override
            public String getString()
            {
                return message;
            }

            @Override
            public boolean setString(String value)
            {
                message = value;
                return true;
            }
        };
    }

    @Override
    public boolean testObjective(Player player, Object data)
    {
        return (data != null && (data instanceof Boolean) && (Boolean) data);
    }

    @Override
    public String getDescription(Player player, Object data)
    {
        return CHAT_HELPER.formatPlayerString(message, player);
    }

    @Override
    public String getMenuLabel(CommandSender sender)
    {
        return CHAT_HELPER.getMessage(sender, "objective-password", message, password);
    }

    @Override
    public Prompt createPrompt(MenuPrompt parentPrompt, ObjectivesHolder base, CommandSender sender)
    {
        return new PasswordObjectivePrompt(this, base, parentPrompt);
    }

    @Override
    public String getFactoryId()
    {
        return PasswordObjectiveFactory.MY_ID;
    }

    public static final String IGNORE_CASE = "ignore-case";
}
