/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.objectives.TimerObjective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class TimerObjectivePrompt extends ObjectivePrompt
{
    private final TimerObjective timerObjective;

    public TimerObjectivePrompt(TimerObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);
        timerObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-timer-type", timerObjective.getType().name()),
                new EnumAcceptorPrompt(this, "prompt-timer-type", timerObjective, TimerObjective.Type.class));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-timer-delay", timerObjective.getSeconds()),
                new IntAcceptorPrompt(this, "prompt-timer-delay", timerObjective));

        super.fillOptions(receiver);
    }
}
