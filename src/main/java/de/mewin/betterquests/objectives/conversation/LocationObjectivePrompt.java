/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.mewinbukkitlib.conversation.acceptors.DoubleAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.LocationAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import de.mewin.betterquests.objectives.LocationObjective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class LocationObjectivePrompt extends ObjectivePrompt
{
    private final LocationObjective locationObjective;

    public LocationObjectivePrompt(LocationObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);
        this.locationObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        Location loc = locationObjective.getLocation();

        String msg = CHAT_HELPER.getMessage(receiver, "menu-location", loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());
        addSimpleOption(msg, new LocationAcceptorPrompt(this, "prompt-edit-location", locationObjective));

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-location-name", locationObjective.getLocationName()),
                new StringAcceptorPrompt(this, "prompt-location-name", locationObjective));

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-distance", locationObjective.getDistance()),
                new DoubleAcceptorPrompt(this, "prompt-edit-distance", locationObjective));

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-location-objective-type", locationObjective.getType().name()),
                new EnumAcceptorPrompt(this, "prompt-location-objective-type", locationObjective, LocationObjective.Type.class));

        super.fillOptions(receiver);
    }
}
