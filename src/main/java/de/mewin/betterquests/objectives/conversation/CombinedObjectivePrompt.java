/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.conversation.editors.EditObjectivesPrompt;
import de.mewin.betterquests.objectives.CombinedObjective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EnumAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class CombinedObjectivePrompt extends ObjectivePrompt
{
    private final CombinedObjective combinedObjective;

    public CombinedObjectivePrompt(CombinedObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);
        this.combinedObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-combination-type", combinedObjective.getType().name()),
                new EnumAcceptorPrompt(this, "prompt-combination-type", combinedObjective, CombinedObjective.Type.class));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-edit-objectives"),
                new EditObjectivesPrompt(combinedObjective, this, receiver));

        super.fillOptions(receiver);
    }
}
