/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;

/**
 *
 * @author mewin
 */
public class SurvivalObjectivePrompt extends ObjectivePrompt
{
    public SurvivalObjectivePrompt(Objective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);
    }
}
