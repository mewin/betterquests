/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.objectives.PasswordObjective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;

import java.util.ArrayList;
import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.LongStringAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.StringAcceptorPrompt;
import org.bukkit.command.CommandSender;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class PasswordObjectivePrompt extends ObjectivePrompt
{
    private final PasswordObjective passwordObjective;

    public PasswordObjectivePrompt(PasswordObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);
        this.passwordObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-message", passwordObjective.getMessage()),
                new LongStringAcceptorPrompt(this, "prompt-message", "-", passwordObjective.getMessageAcceptor()));
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-password", passwordObjective.getPassword()),
                new StringAcceptorPrompt(this, "prompt-password", passwordObjective.getPasswordAcceptor()));

        super.fillOptions(receiver);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());

        settings.add(new KnownSetting(PasswordObjective.IGNORE_CASE, DataType.BOOLEAN));

        return settings;
    }
}
