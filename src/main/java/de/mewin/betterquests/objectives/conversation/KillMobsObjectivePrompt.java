/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.objectives.KillMobsObjective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.EntityTypeAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.IntAcceptorPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class KillMobsObjectivePrompt extends ObjectivePrompt
{
    private final KillMobsObjective killMobsObjective;

    public KillMobsObjectivePrompt(KillMobsObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt);
        this.killMobsObjective = objective;
    }

    @Override
    protected void fillOptions(CommandSender receiver)
    {
        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-mob-type", killMobsObjective.getType().name()),
                new EntityTypeAcceptorPrompt(this, "prompt-mob-type", killMobsObjective, EntityType.class));

        addSimpleOption(CHAT_HELPER.getMessage(receiver, "menu-mob-amount", killMobsObjective.getAmount()),
                new IntAcceptorPrompt(this, "prompt-mob-amount", killMobsObjective));

        super.fillOptions(receiver);
    }
}
