/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.objectives.ItemObjective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.extra.DataType;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemAmountAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemDamageAcceptorPrompt;
import de.mewin.mewinbukkitlib.conversation.acceptors.ItemMaterialAcceptorPrompt;
import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.ShowItemEvent;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemObjectivePrompt extends ObjectivePrompt
{
    private final ItemObjective itemObjective;

    public ItemObjectivePrompt(ItemObjective objective, ObjectivesHolder holder, MenuPrompt parentPrompt, CommandSender sender)
    {
        super(objective, holder, parentPrompt);

        this.itemObjective = objective;
    }

    @Override
    protected void fillOptions(final CommandSender sender)
    {
        ItemStack items = itemObjective.getItemStack();

        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-material", items.getType().toString()),
                        new ItemMaterialAcceptorPrompt(itemObjective, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-amount", items.getAmount()),
                        new ItemAmountAcceptorPrompt(itemObjective, this));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-item-meta", items.getDurability()),
                        new ItemDamageAcceptorPrompt(itemObjective, this));

        if (sender instanceof Player)
        {
            addComplexOption(CHAT_HELPER.getMessage(sender, "menu-item-fromhand"),
            new PromptSupplier()
            {
                @Override
                public Prompt get()
                {
                    Player player = (Player) sender;
                    ItemStack stack = player.getInventory().getItemInMainHand();
                    if (stack == null) { // just in case
                        stack = new ItemStack(Material.AIR, 0);
                    }
                    itemObjective.setItemStack(stack);
                    ItemObjectivePrompt.this.reloadMenu();
                    return ItemObjectivePrompt.this;
                }
            });
        }

        super.fillOptions(sender);
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        Conversable forWhom = context.getForWhom();

        if (forWhom instanceof Player)
        {
            Player player = (Player) forWhom;

            ItemStack stack = itemObjective.getItemStack();

            JSONText[] texts = new JSONText.MultiBuilder()
                .setText(CHAT_HELPER.getMessage(player, "objective-item-current-prefix"))
                .next()
                .setText(stack.getType().name())
                .setHoverEvent(new ShowItemEvent(stack))
                .build();

            CHAT_HELPER.sendJSONMessage(player, texts);
        }

        return super.getPromptText(context);
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        ArrayList<KnownSetting> settings = new ArrayList<>(super.getKnownSettings());
        settings.add(new KnownSetting(ItemObjective.IGNORE_META, DataType.BOOLEAN));
        settings.add(new KnownSetting(ItemObjective.TAKE_ITEMS, DataType.BOOLEAN));
        return settings;
    }
}
