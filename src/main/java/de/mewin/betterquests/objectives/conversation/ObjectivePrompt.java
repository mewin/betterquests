/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.betterquests.objectives.conversation;

import de.mewin.betterquests.conversation.editors.CustomizableEditorPrompt;
import de.mewin.betterquests.quest.Objective;
import de.mewin.betterquests.quest.ObjectivesHolder;
import de.mewin.betterquests.quest.extra.KnownSetting;
import de.mewin.betterquests.quest.extra.ObjectiveSettings;

import java.util.List;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class ObjectivePrompt extends CustomizableEditorPrompt<ObjectivesHolder, Objective>
{
    public ObjectivePrompt(Objective objective, ObjectivesHolder holder, MenuPrompt parentPrompt)
    {
        super(objective, holder, parentPrompt, "advanced-objective");
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();

        printOptions(receiver);

        return CHAT_HELPER.getMessage(receiver, "prompt-edit-objective");
    }

    @Override
    protected void doDelete()
    {
        base.removeObjective(object);
    }

    @Override
    protected String getString(CustomizableEditorPrompt.Strings id)
    {
        switch(id)
        {
            case MENU_DELETE:
                return "menu-delete-objective";
            default:
                return "<missing string>";
        }
    }

    @Override
    protected List<KnownSetting> getKnownSettings()
    {
        return ObjectiveSettings.getKnownSettingsList();
    }
}
