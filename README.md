# BetterQuests

Questing plugin for [Bukkit](https://bukkit.org/) and [Spigot](https://www.spigotmc.org/) servers. Aims to provide support for fully customizable quests while being simple to use for admins and players. This is also kept completely modular to allow easy modification and extension of the source code.

## Getting Started

Just clone the repository and compile using maven (`mvn package` to create jar files).

### Prerequisites

You will need to install maven and any JDK > 8.

## Deployment

To install this to a Bukkit/Spigot server simply copy the created jar file (target/BetterQuests-x.x-SNAPSHOT.jar) to the plugins folder. Make sure you also have [MewinBukkitLib](https://babulo.eu/jenkins/job/Minecraft/job/MewinBukkitLib/) installed on your server. You probably also want [BetterQuestsAddons](https://babulo.eu/jenkins/job/Minecraft/job/BetterQuestsAddons/) which provides integration for several third party plugins.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

You can create issues [on GitLab](https://gitlab.com/mewin/betterquests/issues).

## Versioning

For the versions available, see the [tags on this repository](https://git.babulo.eu/mewin/betterquests/tags). 

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://git.babulo.eu/mewin)

See also the list of [contributors](https://git.babulo.eu/mewin/project/graphs/master) who participated in this project.

## License

This project is published under the terms of the GNU General Public License v3.0. See the LICENSE file for more information.

## Acknowledgments

* the Bukkit and Spigot devs
* the team of [Avalunia](https://avalunia.de) who provided great ideas and feedback
